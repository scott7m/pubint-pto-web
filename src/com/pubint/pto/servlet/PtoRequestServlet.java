package com.pubint.pto.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pubint.pto.dataObjects.PtoRequestStatus;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.sessionInterfaces.PtoRequestLocal;

public class PtoRequestServlet extends PuppetServlet {
	static final long serialVersionUID = 3L;

	private static Logger log = Logger.getLogger("requestServlet");

	private XPathFactory xpf = XPathFactory.newInstance();
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private TransformerFactory tpf = TransformerFactory.newInstance();

	@EJB
	private PtoRequestLocal requestBean;

	@EJB
	private PtoEmployeeLocal employeeBean;

	public PtoRequestServlet() {
		super();

		log.setLevel(Level.TRACE);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		writer.write("Invalid Get");

		writer.flush();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		HttpSession session = request.getSession();

		String email = (String) session.getAttribute("userEMail");

		System.out.println("Currently logged in as: " + email);

		response.setContentType("text/xml");

		PrintWriter writer = response.getWriter();

		Document document = getXML(request, dbf);
		NodeList commands = getCommands(document);
		showXML(document, tpf);

		try {
			String action = "";

			for (Node commandNode = commands.item(0); commandNode != null;) {
				Node nextCommand = commandNode.getNextSibling();

				Element command = (Element) commandNode.cloneNode(true);

				command.appendChild(createElement(document, "sourceIP", request.getRemoteHost()));

				long currentEmployeeID = 0;

				try {
					currentEmployeeID = (Long) session.getAttribute("userID");
					command.appendChild(createElement(document, "currentEmployeeID", currentEmployeeID));
				} catch (Exception e) {
					System.out.println("Exception trying to put the current user (userID) from the session into the command document");
					System.out.println("Message: " + e.getMessage());

					e.printStackTrace();
				}

				command.appendChild(createElement(document, "email", email));

				XPath xpath = xpf.newXPath();

				action = xpath.evaluate("//action", command);

				System.out.println("Action:" + action);

				if (action.equalsIgnoreCase("sendRequest")) {
					writer.write(requestBean.sendRequest(command, xpath));
				} else if (action.equalsIgnoreCase("processRequest")) {
					writer.write(requestBean.processRequest(command, xpath));
				} else if (action.equalsIgnoreCase("sendXML")) {
					writer.write(requestBean.sendXML(command, xpath));
				} else if (action.equalsIgnoreCase("calculateDates")) {
					writer.write(requestBean.calculateDates(command, xpath));
				} else if (action.equalsIgnoreCase("calculateEndDate")) {
					writer.write(requestBean.calculateEndDate(command, xpath));
				} else if (action.equalsIgnoreCase("calculateStartDate")) {
					writer.write(requestBean.calculateStartDate(command, xpath));
				} else if (action.equalsIgnoreCase("calculateEndTime")) {
					writer.write(requestBean.calculateEndTime(command, xpath));
				} else if (action.equalsIgnoreCase("validateDate")) {
					writer.write(requestBean.validateDate(command, xpath));
				} else if (action.equalsIgnoreCase("calculateNumDays")) {
					writer.write(requestBean.calculateNumDays(command, xpath));
				} else if (action.equalsIgnoreCase("getEmployeeRequestHistory")) {
					writer.write(requestBean.getEmployeeHistory(command, xpath));
				} else if (action.equalsIgnoreCase("getPendingRequests")) {
					writer.write(requestBean.getPendingRequests(currentEmployeeID));
				} else if (action.equalsIgnoreCase("statusSelectList")) {
					writer.write(PtoRequestStatus.getSelectList());
				} else if (action.equalsIgnoreCase("departmentalAttendance")) {
					writer.write(requestBean.getDepartmentalAttendance(command, xpath));
				} else {
					System.out.println("Unsupported function");
				}

				commandNode = nextCommand;
			}

			writer.flush();
		} catch (Exception e) {

		}
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doDelete(request, response);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPut(request, response);
	}
}
