package com.pubint.pto.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pubint.pto.timers.DroneInterface;
import com.pubint.pto.timers.TimerData;

public class ManageTimers extends PuppetServlet {
	static final long serialVersionUID = 3L;

	@EJB(beanName = "SendDailyAttendance")
	private DroneInterface timerControl;

	public ManageTimers() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		HttpSession session = request.getSession();

		String userID = ("" + session.getAttribute("userID")).trim();

		String enableTimers = request.getParameter("enableTimers");

		@SuppressWarnings("unused") String disableTimers = request.getParameter("disableTimers");

		PrintWriter writer = response.getWriter();

		String output = "";

		output =
			"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
				"<html>" +
				"<head>" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=US-ASCII\" />" +
				"<script language=\"JavaScript\" type=\"text/javascript\">" +
				"window.close();" +
				"</script>" +
				"</head>" +
				"<body>";

		try {
			if (enableTimers.equals("true")) {
				timerControl.setSchedule(new TimerData(userID, "Enable timers..."));
			}
		} catch (Exception e) {
		}

		output +=
			"</body>" +
				"</html>";

		writer.write(output);

		writer.flush();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doDelete(request, response);
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPut(request, response);
	}
}