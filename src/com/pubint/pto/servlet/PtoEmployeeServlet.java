package com.pubint.pto.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;

public class PtoEmployeeServlet extends PuppetServlet {
	static final long serialVersionUID = 3L;

	private XPathFactory xpf = XPathFactory.newInstance();
	private DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	private TransformerFactory tpf = TransformerFactory.newInstance();

	@EJB
	private PtoEmployeeLocal employeeBean;

	public PtoEmployeeServlet() {
		super();
		System.out.println("In the Employee Servlet constructor.");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		writer.write("PTO Employee Servlet Response OK");

		writer.flush();
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In the Employee Servlet post.");

		super.doPost(request, response);

		HttpSession session = request.getSession();

		String email = (String) session.getAttribute("userEMail");

		System.out.println("Currently logged in as: " + email);

		response.setContentType("text/xml");

		PrintWriter writer = response.getWriter();

		Document document = getXML(request, dbf);
		NodeList commands = getCommands(document);
		showXML(document, tpf);

		try {
			String action = "";

			System.out.println("Trying the for loop.");

			for (Node commandNode = commands.item(0); commandNode != null;) {
				System.out.println("In the for loop!");
				Node nextCommand = commandNode.getNextSibling();

				System.out.println("Creating Element!");
				Element command = (Element) commandNode.cloneNode(true);

				System.out.println("Appending Element!");

				command.appendChild(createElement(document, "sourceIP", request.getRemoteHost()));

				long currentEmployeeID = 0;

				boolean loggedIn = true;

				try {
					currentEmployeeID = (Long) session.getAttribute("userID");
					command.appendChild(createElement(document, "currentEmployeeID", currentEmployeeID));
				} catch (Exception e) {
					System.out.println("Exception trying to put the current user (userID) from the session into the command document");
					System.out.println("Message: " + e.getMessage());

					loggedIn = false;

					e.printStackTrace();
				}

				System.out.println("Creating XPath!");
				XPath xpath = xpf.newXPath();

				action = xpath.evaluate("//action", command);

				System.out.println("Action:" + action);

				if (action.equalsIgnoreCase("selectList")) {
					boolean asReception = getBoolean(command, "asReceptionist", xpath);

					String xml;

					if (asReception) {
						xml = employeeBean.receptionistList(currentEmployeeID);
					} else {
						xml = employeeBean.selectList(currentEmployeeID);
					}

					writer.write(xml);
				} else if (action.equalsIgnoreCase("checkEscalation")) {
					System.out.println("In the code to check privilege escalation...");

					if (session.getAttribute("escalated") == null) {
						System.out.println("Not yet escalated...testing...");

						String escalation = employeeBean.escalationPrivileges(command, xpath);

						if (notEmptyString(escalation)) {
							session.setAttribute("escalated", escalation);

							System.out.println("Setting the session variable to remember that I'm escalated...");
						}
					} else {
						System.out.println("Already escalated...returning: " + session.getAttribute("escalated"));
					}

					String escalationXML = (String) session.getAttribute("escalated");

					if (notEmptyString(escalationXML)) {
						System.out.println("Return escalation!");
					} else {
						escalationXML = "";
					}

					writer.write(escalationXML);
				} else if (("sendXML").equalsIgnoreCase(action)) {
					writer.write(employeeBean.sendXML(command, xpath));
				} else if (action.equalsIgnoreCase("changePassword")) {
					writer.write(employeeBean.changePassword(command, xpath));
				} else if (action.equalsIgnoreCase("resetPassword")) {
					writer.write(employeeBean.resetPassword(command, xpath));
				} else if (action.equalsIgnoreCase("update")) {
					writer.write(employeeBean.update(command, xpath));
				} else if (action.equalsIgnoreCase("remove")) {
					writer.write(employeeBean.remove(command, xpath));
				} else if (action.equalsIgnoreCase("typeSelectList")) {
					writer.write(employeeBean.typeSelectList());
				} else if (action.equalsIgnoreCase("eventTypeSelectList")) {
					writer.write(employeeBean.eventTypeSelectList());
				} else if (action.equalsIgnoreCase("supervisorSelectList")) {
					writer.write(employeeBean.supervisorSelectList());
				} else if (action.equalsIgnoreCase("getTotalPTO")) {
					writer.write(String.valueOf(employeeBean.getTotalPTO(this.getLong(command, "employeeID", xpath))));
				} else if (action.equalsIgnoreCase("addEvent")) {
					writer.write(employeeBean.addEvent(command, xpath));
				} else if (action.equalsIgnoreCase("deleteEvent")) {
					writer.write(employeeBean.deleteEvent(command, xpath));
				} else if (action.equalsIgnoreCase("activeEmployeeList")) {
					writer.write(employeeBean.activeEmployeeXML());
				} else if (action.equalsIgnoreCase("sendDailyAttendance")) {
					employeeBean.sendDailyAttendance();
				} else if (action.equalsIgnoreCase("rollover2015PTO")) {
					employeeBean.rollover2015PTO();
				} else if (action.equalsIgnoreCase("newSystemEmail")) {
					employeeBean.sendNewSystemEmails();
				} else if (action.equalsIgnoreCase("populateHistory")) {
					java.sql.Date today = new java.sql.Date(new java.util.Date().getTime());
					employeeBean.populateGrantedHistory(today); // this.getDate(command,
																// "date",
																// xpath)
				} else if (action.equalsIgnoreCase("userLoggedIn")) {
					if (! loggedIn) {
						System.out.println("User is not logged in!!!");
						writer.write("false");
					} else {
						System.out.println("User IS logged in!!");
						writer.write("true");
					}
				} else {
					System.out.println("Action: " + action);
					System.out.println("Unsupported function");
				}

				commandNode = nextCommand;
			}

			writer.flush();
		} catch (Exception e) {

		}
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doDelete(request, response);
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPut(request, response);
	}
}
