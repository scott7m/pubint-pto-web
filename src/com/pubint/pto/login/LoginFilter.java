package com.pubint.pto.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.util.UtilHttp;

public class LoginFilter implements Filter {
	private FilterConfig filterConfig = null;

	@EJB
	private PtoEmployeeLocal userLogin;

	public LoginFilter() {
		super();
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {
		this.filterConfig = null;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (filterConfig == null) {
			return;
		}

		boolean authenticated = false;

		if (request instanceof HttpServletRequest) {
			HttpServletRequest req = (HttpServletRequest) request;

			if (req.getRequestURL().indexOf("LoginServlet") > 0) {
				authenticated = true;
			} else if (req.getRequestURI().toUpperCase().contains("MANAGETIMERS")) {
				System.out.println("Allow crontab...");

				authenticated = true;
			} else {
				HttpSession session = req.getSession();

				if (session.isNew()) {
					authenticated = false;
				} else {
					Long userID = (Long) session.getAttribute("userID");

					@SuppressWarnings("unused") String ipAddress = request.getRemoteAddr();

					boolean userIsJay = false; // (ipAddress.indexOf("172.16.48.234")
												// == 0);

					if ((userID == null) ||
						(userID == 0L)) {
						if (userIsJay) {
							PtoEmployee employee = userLogin.getEmployee("jmchugh@pubint.com", "eidolon");

							session.setAttribute("userID", employee.getId());
							session.setAttribute("userEMail", employee.getEmail());
							session.setAttribute("userFirstName", employee.getFirstName());
							session.setAttribute("userLastName", employee.getLastName());
							session.setAttribute("userFullName", employee.getFullName());
							session.setAttribute(
								"userLine",
								"" +
									employee.getFirstName() + " " +
									employee.getLastName() + " (" +
									employee.getId() + ")"
								);

							session.setAttribute("systemName", LoginServlet.systemName);

							System.out.println("userID: " + session.getAttribute("userID"));
							System.out.println("userEMail: " + session.getAttribute("userEMail"));

							authenticated = true;
						} else {
							authenticated = false;
						}
					} else if (request.getParameter("logout") != null) {
						try {
							session.invalidate();
						} catch (Exception e) {
							System.out.println("Failed to logout properly!!!!");
							System.out.println("Exception: " + e.getMessage());

							e.printStackTrace();
						}

						authenticated = false;
					} else {
						authenticated = true;
					}
				}
			}

			if (authenticated) {
				chain.doFilter(request, response);
			} else {
				response.setContentType("text/html");

				try {
					PrintWriter out = response.getWriter();

					out.println("<!DOCTYPE html>\n");
					out.println("<html class=\"no-js\">\n");
					out.println("\t<head>");
					out.println("\t\t<meta charset=\"utf-8\">");
					out.println("\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">");
					out.println("\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
					out.println("\t\t<title>PIL PTO Login</title>");
					out.println("\t</head>");
					out.println("\t<body>");
					out.println("\t\t<form action=\"" + UtilHttp.getFullRootUrl(req) + "/LoginServlet\" method=\"post\">");
					out.println("\t\t\t<table border=\"1\">");
					out.println("\t\t\t\t<tr>");
					out.println("\t\t\t\t\t<td>Login Name</td>");
					out.println("\t\t\t\t\t<td><input type=\"text\" name=\"userCode\" value=\"\" width=\"30\" /></td>");
					out.println("\t\t\t\t</tr>");
					out.println("\t\t\t\t<tr>");
					out.println("\t\t\t\t\t<td>Password</td>");
					out.println("\t\t\t\t\t<td><input type=\"password\" name=\"userPassword\" value=\"\" width=\"30\" /></td>");
					out.println("\t\t\t\t</tr>");
					out.println("\t\t\t</table>");
					out.println("\t\t\t<br />");

					System.out.println("Stashing the URL: " + req.getRequestURL());
					System.out.println("Stashing the query: " + req.getQueryString());

					out.println("\t\t\t<input type=\"hidden\" name=\"originalURL\" value=\"" + req.getRequestURL() + "\" />");
					out.println("\t\t\t<input type=\"hidden\" name=\"originalQuery\" value=\"" + req.getQueryString() + "\" />");
					out.println("\t\t\t<input type=\"submit\" name=\"submit\" value=\"Process Login\" />");
					out.println("\t\t</form>");
					out.println("\t\t<hr />");
					out.println("\t</body>");
					out.println("</html>");
				} catch (IOException ioe) {

				}
			}
		}
	}
}