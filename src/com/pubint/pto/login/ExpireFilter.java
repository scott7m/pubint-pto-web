package com.pubint.pto.login;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class ExpireFilter implements Filter {
	private FilterConfig filterConfig = null;

	public ExpireFilter() {
		super();
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {
		this.filterConfig = null;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (filterConfig == null) {
			return;
		}

		try {
			if (response instanceof HttpServletResponse) {
				final HttpServletResponse resp = (HttpServletResponse) response;

				resp.addHeader("Cache-Control", "no-cache");
			}
		} catch (Exception e) {
			// swallow
		}

		chain.doFilter(request, response);
	}
}