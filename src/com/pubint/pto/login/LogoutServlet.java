package com.pubint.pto.login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pubint.pto.servlet.PuppetServlet;
import com.pubint.pto.util.UtilHttp;

public class LogoutServlet extends PuppetServlet {
	private static final long serialVersionUID = 2L;

	public LogoutServlet() {
		super();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		System.out.println("Removing login credentials...");

		HttpSession session = request.getSession();

		session.removeAttribute("userID");
		session.removeAttribute("userEMail");
		session.removeAttribute("userFirstName");
		session.removeAttribute("userLastName");
		session.removeAttribute("userFullName");

		try {
			session.removeAttribute("escalated");
		} catch (Exception e) {
			// swallow
		}

		response.sendRedirect(UtilHttp.getFullRootUrl(request));
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}