package com.pubint.pto.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Authority {
	public Authority() {
	}

	public boolean login(HttpServletRequest request, HttpServletResponse response) {
		StringBuffer fullURL = request.getRequestURL();

		int tempPos = fullURL.indexOf(":80");
		tempPos = fullURL.indexOf("/", tempPos + 6);

		String serverName = fullURL.substring(0, tempPos);
		String loginURL = serverName + "/login.jsp";

		HttpSession session = request.getSession();
		boolean returnFlag = false;

		if (session.isNew()) {
			try {
				response.sendRedirect(loginURL);
			} catch (Exception ex) {

			}
		} else {
			if ((session.getAttribute("userID") == null) || (session.getAttribute("userID") == "")) {
				try {
					response.sendRedirect(loginURL);
				} catch (Exception ex) {

				}
			} else if (request.getParameter("logout") != null) {
				try {
					session.invalidate();
					response.sendRedirect(loginURL);
				} catch (Exception ex) {

				}
			} else {
				returnFlag = true;
			}
		}

		return returnFlag;
	}
}