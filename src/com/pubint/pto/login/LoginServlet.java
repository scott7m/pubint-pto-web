package com.pubint.pto.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.servlet.PuppetServlet;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;
import com.pubint.pto.util.UtilHttp;

public class LoginServlet extends PuppetServlet {
	static final long serialVersionUID = 3L;

	static final String systemName = "Publications International - PTO Tracking";

	@EJB
	private PtoEmployeeLocal userLogin;

	public LoginServlet() {
		super();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			super.doPost(request, response);

			String ipAddress = request.getRemoteAddr();

			System.out.println("Checking to see if this is a local user...");
			System.out.println("User IP: " + ipAddress);

			@SuppressWarnings("unused") boolean userIsLocal = false ||
				(ipAddress.indexOf("172.16.") == 0) ||			// PIL
				(ipAddress.indexOf("172.19.") == 0) ||			// JRS
				(ipAddress.indexOf("10.10.") == 0) ||			// International
				(ipAddress.indexOf("192.168.") == 0) ||			// VPN
				(ipAddress.indexOf("0:0:0:0:0:0:0:1") == 0) ||	// localhost
				(ipAddress.indexOf("127.0.") == 0);				// localhost

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			HttpSession session = request.getSession();

			String userCode = request.getParameter("userCode");
			String userPassword = request.getParameter("userPassword");

			PtoEmployee employee = userLogin.getEmployee(userCode, userPassword);

			if (employee.getTerminationDate() != null) {
				employee = null;
			}

			System.out.println("Got the employee: " + employee);
			System.out.println("Saving values to the session...");

			if (employee != null) {
				session.setAttribute("userID", employee.getId());
				session.setAttribute("userEMail", employee.getEmail());
				session.setAttribute("userFirstName", employee.getFirstName());
				session.setAttribute("userLastName", employee.getLastName());
				session.setAttribute("userFullName", employee.getFullName());
				session.setAttribute(
					"userLine",
					"" +
						employee.getFirstName() + " " +
						employee.getLastName() + " (" +
						employee.getId() + ")"
					);

				session.setAttribute("systemName", systemName);

				System.out.println("userID: " + session.getAttribute("userID"));
				System.out.println("userEMail: " + session.getAttribute("userEMail"));

				try {
					String queryString = request.getParameter("originalQuery");
					String fullURL = request.getParameter("originalURL");

					if ((queryString != null) && (! queryString.equals(""))) {
						fullURL += "?" + queryString;
					}

					System.out.println("Forwarding to URL: " + fullURL);

					response.sendRedirect(fullURL);
				} catch (Exception ex) {
					System.out.println("Unable to forward to the appropriate URL: " + ex.getMessage());
				}
			} else {
				response.sendRedirect(UtilHttp.getFullRootUrl(request));
			}

			out.close();
		} catch (Exception e) {
			System.out.println("Exception logging in: " + e.getMessage());

			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
}