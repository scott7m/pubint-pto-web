package com.pubint.pto.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.servlet.PuppetServlet;
import com.pubint.pto.sessionInterfaces.PtoRequestLocal;

public class PtoRequestPrint extends PuppetServlet {
	private static final long serialVersionUID = 3L;

	@EJB
	private PtoRequestLocal requestBean;

	Font normalFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
	Font boldFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
	Font bigFont = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL);
	Font bigBoldFont = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD);
	Font headerFont = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD);

	private class MakePDF extends PdfPageEventHelper {

		public MakePDF(HttpSession session) {
		}

		@Override
		public void onEndPage(PdfWriter writer, Document document) {
			DateFormat localFormat = DateFormat.getDateInstance();
			DateFormat localTime = DateFormat.getTimeInstance();

			try {
				Rectangle page = document.getPageSize();

				// Define the basic page heading and add to the document
				PdfPTable header = new PdfPTable(3);

				PdfPCell emptyCell = new PdfPCell();
				emptyCell.setBorder(Rectangle.NO_BORDER);

				// row one
				PdfPCell dateCell = new PdfPCell(new Paragraph("Date: " + localFormat.format(new Date())));
				dateCell.setBorder(Rectangle.NO_BORDER);

				header.addCell(dateCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// title row
				PdfPCell timeCell = new PdfPCell(new Paragraph("Time: " + localTime.format(new Date())));
				timeCell.setBorder(Rectangle.NO_BORDER);

				Phrase titlePhrase = new Phrase("View PTO Request" + "", headerFont);
				PdfPCell title = new PdfPCell(titlePhrase);
				title.setBorder(Rectangle.NO_BORDER);
				title.setHorizontalAlignment(Element.ALIGN_CENTER);

				header.addCell(timeCell);
				header.addCell(title);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				header.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
				header.writeSelectedRows(0, - 1, document.leftMargin(),
					page.getHeight() - document.topMargin() + header.getTotalHeight(),
					writer.getDirectContent());

				// The content from the document ends up here
				// between the header and footer

				// Define the basic page footer and add to the document
				PdfPTable foot = new PdfPTable(3);

				PdfPCell pageCount = new PdfPCell(new Paragraph("Page: " + writer.getPageNumber()));
				pageCount.setBorder(Rectangle.NO_BORDER);
				pageCount.setHorizontalAlignment(Element.ALIGN_CENTER);

				foot.addCell(emptyCell);
				foot.addCell(pageCount);
				foot.addCell(emptyCell);

				foot.setTotalWidth(page.getWidth() - document.leftMargin()
					- document.rightMargin());
				foot.writeSelectedRows(0, - 1, document.leftMargin(),
					document.bottomMargin(), writer.getDirectContent());
			} catch (Exception e) {
				throw new ExceptionConverter(e);
			}
		}
	}

	public PtoRequestPrint() {
		super();
	}

	protected void makePdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		HttpSession session = request.getSession();

		@SuppressWarnings("unused") Phrase workPhrase = new Phrase();

		@SuppressWarnings("unused") String work = "";

		int tableCols = 4;

		try {
			Document document = new Document(PageSize.LETTER, 20, 20, 70, 70);
			Rectangle page = document.getPageSize();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			PdfWriter writer = PdfWriter.getInstance(document, baos);
			writer.setPageEvent(new MakePDF(session));

			document.open();

			// set up an empty cell for filler
			PdfPCell emptyCell = new PdfPCell();
			emptyCell.setBorder(Rectangle.NO_BORDER);

			// set up a blank line cell for filler
			PdfPCell blankLine = new PdfPCell();
			blankLine.setBorder(Rectangle.NO_BORDER);
			blankLine.setColspan(tableCols);

			// set up the body as a five column table
			PdfPTable requestTable = new PdfPTable(tableCols);

			SimpleDateFormat tf = new SimpleDateFormat("h:mm a");
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

			PtoRequest ptoRequest = null;
			long reqID = 0;

			try {
				reqID = Long.parseLong(request.getParameter("requestID"));

				ptoRequest = requestBean.getRequest(reqID);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			if (ptoRequest != null) {
				System.out.println("Got a request: " + ptoRequest.toString());
			} else {
				System.err.println("Unable to find request based on ID: " + reqID);
			}

			PdfPCell workCell = new PdfPCell();
			workCell.setColspan(1);

			// Employee Name
			workCell.setPhrase(new Phrase("Employee Name:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(ptoRequest.getEmployeeName(), normalFont));
			workCell.setColspan(3);
			requestTable.addCell(workCell);

			// Request Type
			workCell.setPhrase(new Phrase("Request Type:", boldFont));
			workCell.setColspan(1);
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(ptoRequest.getRequestTypeShortDescription(), normalFont));
			workCell.setColspan(3);
			requestTable.addCell(workCell);

			workCell.setColspan(1);

			// Starting
			workCell.setPhrase(new Phrase("Starting Date:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(sdf2.format(ptoRequest.getStartDate()), normalFont));
			requestTable.addCell(workCell);

			workCell.setPhrase(new Phrase("Time:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(tf.format(ptoRequest.getStartTime()), normalFont));
			requestTable.addCell(workCell);

			// Ending
			workCell.setPhrase(new Phrase("Ending Date:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(sdf2.format(ptoRequest.getEndDate()), normalFont));
			requestTable.addCell(workCell);

			workCell.setPhrase(new Phrase("Time:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(tf.format(ptoRequest.getEndTime()), normalFont));
			requestTable.addCell(workCell);

			double numDays = ptoRequest.getDaysTaken();

			// Number Days
			workCell.setPhrase(new Phrase("Number of Days:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(String.valueOf(numDays), normalFont));
			requestTable.addCell(workCell);

			// Status
			workCell.setPhrase(new Phrase("Status:", boldFont));
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(ptoRequest.getRequestStatus(), normalFont));
			requestTable.addCell(workCell);

			// Email
			workCell.setPhrase(new Phrase("Comment:", boldFont));
			workCell.setColspan(1);
			requestTable.addCell(workCell);
			workCell.setPhrase(new Phrase(ptoRequest.getComment(), normalFont));
			workCell.setColspan(3);
			requestTable.addCell(workCell);

			requestTable.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
			document.add(requestTable);

			document.close();

			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			// setting the content type
			response.setContentType("application/pdf");

			// the contentlength is needed for MSIE!!!
			response.setContentLength(baos.size());

			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
		} catch (DocumentException de) {
			de.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}
}
