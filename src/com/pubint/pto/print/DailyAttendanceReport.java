package com.pubint.pto.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfBorderDictionary;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.pubint.pto.entity.PtoRequest;
import com.pubint.pto.entityList.PtoRequestList;
import com.pubint.pto.servlet.PuppetServlet;
import com.pubint.pto.sessionInterfaces.PtoRequestLocal;

public class DailyAttendanceReport extends PuppetServlet {
	private static final long serialVersionUID = 3L;

	@EJB
	private PtoRequestLocal requestBean;

	private class MakePDF extends PdfPageEventHelper {

		public MakePDF(HttpSession session) {
		}

		@Override
		public void onEndPage(PdfWriter writer, Document document) {
			DateFormat localFormat = DateFormat.getDateInstance();
			DateFormat localTime = DateFormat.getTimeInstance();

			try {
				Rectangle page = document.getPageSize();

				// Define the basic page heading and add to the document
				PdfPTable header = new PdfPTable(3);

				PdfPCell emptyCell = new PdfPCell();
				emptyCell.setBorder(Rectangle.NO_BORDER);

				// row one
				PdfPCell dateCell = new PdfPCell(new Paragraph("Date: " + localFormat.format(new Date())));
				dateCell.setBorder(Rectangle.NO_BORDER);

				header.addCell(dateCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// title row
				PdfPCell timeCell = new PdfPCell(new Paragraph("Time: " + localTime.format(new Date())));
				timeCell.setBorder(Rectangle.NO_BORDER);

				Phrase titlePhrase = new Phrase("Daily Attendance Report" + "",
					FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD));
				PdfPCell title = new PdfPCell(titlePhrase);
				title.setBorder(Rectangle.NO_BORDER);
				title.setHorizontalAlignment(Element.ALIGN_CENTER);

				header.addCell(timeCell);
				header.addCell(title);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				header.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
				header.writeSelectedRows(0, - 1, document.leftMargin(),
					page.getHeight() - document.topMargin() + header.getTotalHeight(),
					writer.getDirectContent());

				// The content from the document ends up here
				// between the header and footer

				// Define the basic page footer and add to the document
				PdfPTable foot = new PdfPTable(3);

				PdfPCell pageCount = new PdfPCell(new Paragraph("Page: " + writer.getPageNumber()));
				pageCount.setBorder(Rectangle.NO_BORDER);
				pageCount.setHorizontalAlignment(Element.ALIGN_CENTER);

				foot.addCell(emptyCell);
				foot.addCell(pageCount);
				foot.addCell(emptyCell);

				foot.setTotalWidth(page.getWidth() - document.leftMargin()
					- document.rightMargin());
				foot.writeSelectedRows(0, - 1, document.leftMargin(),
					document.bottomMargin(), writer.getDirectContent());
			} catch (Exception e) {
				throw new ExceptionConverter(e);
			}
		}
	}

	public DailyAttendanceReport() {
		super();
	}

	protected void makePdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		HttpSession session = request.getSession();

		Phrase workPhrase = new Phrase();
		String work = "";
		int tableCols = 17;

		try {
			Document document = new Document(PageSize.LETTER, 10, 10, 70, 70);
			Rectangle page = document.getPageSize();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			PdfWriter writer = PdfWriter.getInstance(document, baos);
			writer.setPageEvent(new MakePDF(session));

			document.open();

			// set up an empty cell for filler
			PdfPCell emptyCell = new PdfPCell();
			emptyCell.setBorder(Rectangle.NO_BORDER);

			// set up a blank line cell for filler
			PdfPCell blankLine = new PdfPCell();
			blankLine.setBorder(Rectangle.NO_BORDER);
			blankLine.setColspan(tableCols);

			// set up the body as a five column table
			PdfPTable requestTable = new PdfPTable(tableCols);

			// category header
			PdfPCell headerRow = null;

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy");
			java.sql.Date reportDate = null;

			try {
				reportDate = new java.sql.Date(sdf.parse(request.getParameter("reportDate")).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}

			headerRow = new PdfPCell(new Paragraph("Date: " + sdf2.format(reportDate), FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD)));
			headerRow.setBorder(Rectangle.NO_BORDER);
			headerRow.setHorizontalAlignment(Element.ALIGN_LEFT);
			headerRow.setColspan(tableCols);
			requestTable.addCell(headerRow);
			requestTable.addCell(blankLine);

			PtoRequestList requests = requestBean.getAbsenceList(reportDate);

			headerRow = new PdfPCell(new Paragraph("Name", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(Element.ALIGN_LEFT);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);
			requestTable.addCell(emptyCell);

			headerRow = new PdfPCell(new Paragraph("Dates away", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(Element.ALIGN_CENTER);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);
			requestTable.addCell(emptyCell);

			headerRow = new PdfPCell(new Paragraph("Number Days", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
			headerRow.setBorder(PdfBorderDictionary.STYLE_BEVELED);
			headerRow.setHorizontalAlignment(Element.ALIGN_RIGHT);
			headerRow.setColspan(5);
			requestTable.addCell(headerRow);

			for (PtoRequest ptoRequest : requests.getRequests()) {
				work = ptoRequest.getEmployee().getFullName() + " - " + ptoRequest.getEmployee().getDepartment().getDepartmentName();
				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				PdfPCell workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
				requestTable.addCell(emptyCell);

				SimpleDateFormat df = new SimpleDateFormat("M-d");
				SimpleDateFormat tf = new SimpleDateFormat("h:mm a");

				work = "" +
					df.format(ptoRequest.getStartDate()) +
					" " + tf.format(ptoRequest.getStartTime()) +
					" - " +
					df.format(ptoRequest.getEndDate()) +
					" " + tf.format(ptoRequest.getEndTime());

				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
				requestTable.addCell(emptyCell);

				work = String.valueOf(ptoRequest.getDaysTaken());

				work = work + " - " + ptoRequest.getRequestType().getDescription();

				workPhrase = new Phrase(work, FontFactory.getFont(FontFactory.HELVETICA, 9));

				workCell = new PdfPCell(workPhrase);
				workCell.setBorder(Rectangle.NO_BORDER);
				workCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				workCell.setColspan(5);

				requestTable.addCell(workCell);
			}

			requestTable.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
			document.add(requestTable);

			document.close();

			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			// setting the content type
			response.setContentType("application/pdf");

			// the contentlength is needed for MSIE!!!
			response.setContentLength(baos.size());

			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
		} catch (DocumentException de) {
			de.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}
}
