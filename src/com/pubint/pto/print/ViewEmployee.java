package com.pubint.pto.print;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.pubint.pto.entity.PtoEmployee;
import com.pubint.pto.servlet.PuppetServlet;
import com.pubint.pto.sessionInterfaces.PtoEmployeeLocal;

public class ViewEmployee extends PuppetServlet {
	private static final long serialVersionUID = 3L;

	@EJB
	private PtoEmployeeLocal employeeBean;

	private class MakePDF extends PdfPageEventHelper {

		public MakePDF(HttpSession session) {
		}

		@Override
		public void onEndPage(PdfWriter writer, Document document) {
			DateFormat localFormat = DateFormat.getDateInstance();
			DateFormat localTime = DateFormat.getTimeInstance();

			try {
				Rectangle page = document.getPageSize();

				// Define the basic page heading and add to the document
				PdfPTable header = new PdfPTable(3);

				PdfPCell emptyCell = new PdfPCell();
				emptyCell.setBorder(Rectangle.NO_BORDER);

				// row one
				PdfPCell dateCell = new PdfPCell(new Paragraph("Date: " + localFormat.format(new Date())));
				dateCell.setBorder(Rectangle.NO_BORDER);

				header.addCell(dateCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// title row
				PdfPCell timeCell = new PdfPCell(new Paragraph("Time: " + localTime.format(new Date())));
				timeCell.setBorder(Rectangle.NO_BORDER);

				Phrase titlePhrase = new Phrase("View Employee Record" + "",
					FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD));
				PdfPCell title = new PdfPCell(titlePhrase);
				title.setBorder(Rectangle.NO_BORDER);
				title.setHorizontalAlignment(Element.ALIGN_CENTER);

				header.addCell(timeCell);
				header.addCell(title);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				// blank row
				header.addCell(emptyCell);
				header.addCell(emptyCell);
				header.addCell(emptyCell);

				header.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
				header.writeSelectedRows(0, - 1, document.leftMargin(),
					page.getHeight() - document.topMargin() + header.getTotalHeight(),
					writer.getDirectContent());

				// The content from the document ends up here
				// between the header and footer

				// Define the basic page footer and add to the document
				PdfPTable foot = new PdfPTable(3);

				PdfPCell pageCount = new PdfPCell(new Paragraph("Page: " + writer.getPageNumber()));
				pageCount.setBorder(Rectangle.NO_BORDER);
				pageCount.setHorizontalAlignment(Element.ALIGN_CENTER);

				foot.addCell(emptyCell);
				foot.addCell(pageCount);
				foot.addCell(emptyCell);

				foot.setTotalWidth(page.getWidth() - document.leftMargin()
					- document.rightMargin());
				foot.writeSelectedRows(0, - 1, document.leftMargin(),
					document.bottomMargin(), writer.getDirectContent());
			} catch (Exception e) {
				throw new ExceptionConverter(e);
			}
		}
	}

	public ViewEmployee() {
		super();
	}

	protected void makePdf(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		super.doPost(request, response);

		HttpSession session = request.getSession();

		@SuppressWarnings("unused") Phrase workPhrase = new Phrase();

		String work = "";

		int tableCols = 4;

		Font boldFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
		Font normalFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);

		try {
			Document document = new Document(PageSize.LETTER, 20, 20, 70, 70);
			Rectangle page = document.getPageSize();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			PdfWriter writer = PdfWriter.getInstance(document, baos);
			writer.setPageEvent(new MakePDF(session));

			document.open();

			// set up an empty cell for filler
			PdfPCell emptyCell = new PdfPCell();
			emptyCell.setBorder(Rectangle.NO_BORDER);

			// set up a blank line cell for filler
			PdfPCell blankLine = new PdfPCell();
			blankLine.setBorder(Rectangle.NO_BORDER);
			blankLine.setColspan(tableCols);

			// set up the body as a five column table
			PdfPTable employeeTable = new PdfPTable(tableCols);

			SimpleDateFormat tf = new SimpleDateFormat("h:mm a");

			PtoEmployee emp = null;
			long empID = 0;

			try {
				empID = Long.parseLong(request.getParameter("empID"));

				emp = employeeBean.getEmployee(empID);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			if (emp != null) {
				System.out.println("Got an employee: " + emp.getFullName());
			} else {
				System.err.println("Unable to find employee based on ID: " + empID);
			}

			// headerRow = new PdfPCell(new Paragraph("Date: " +
			// sdf2.format(reportDate),
			// FontFactory.getFont(FontFactory.HELVETICA, 14, Font.BOLD)));
			// headerRow.setBorder(Rectangle.NO_BORDER);
			// headerRow.setHorizontalAlignment(Element.ALIGN_LEFT);
			// headerRow.setColspan(tableCols);
			// requestTable.addCell(headerRow);
			// requestTable.addCell(blankLine);
			//
			// PtoRequestList requests = requestBean.getAbsenceList(reportDate);

			PdfPCell workCell = new PdfPCell();
			workCell.setColspan(2);

			// ID
			// workCell.setPhrase(new Phrase("Employee ID:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(String.valueOf(emp.getId()),
			// normalFont));
			// employeeTable.addCell(workCell);

			// First Name
			workCell.setPhrase(new Phrase("First Name:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getFirstName(), normalFont));
			employeeTable.addCell(workCell);

			// Middle Initial
			workCell.setPhrase(new Phrase("Middle Initial:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getMiddleInitial(), normalFont));
			employeeTable.addCell(workCell);

			// Last Name
			workCell.setPhrase(new Phrase("Last Name:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getLastName(), normalFont));
			employeeTable.addCell(workCell);

			// Location
			workCell.setPhrase(new Phrase("Office Location:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getLocation().getName(), normalFont));
			employeeTable.addCell(workCell);

			// Department
			workCell.setPhrase(new Phrase("Department:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getDepartment().getDepartmentName(), normalFont));
			employeeTable.addCell(workCell);

			// Title
			workCell.setPhrase(new Phrase("Title:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getTitle(), normalFont));
			employeeTable.addCell(workCell);

			// Email
			workCell.setPhrase(new Phrase("Email Address:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getEmail(), normalFont));
			employeeTable.addCell(workCell);

			// Extension
			workCell.setPhrase(new Phrase("Extension:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getExtension(), normalFont));
			employeeTable.addCell(workCell);

			System.out.println("This employee's Direct Report: " + emp.getDirectReport());

			// Direct Report
			workCell.setPhrase(new Phrase("Direct Report:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getDirectReport() == null ? "" : emp.getDirectReport().getFullName(), normalFont));
			employeeTable.addCell(workCell);

			// PTO Report
			workCell.setPhrase(new Phrase("PTO Report:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getPtoReport() == null ? "" : emp.getPtoReport().getFullName(), normalFont));
			employeeTable.addCell(workCell);

			// Backup PTO Report
			workCell.setPhrase(new Phrase("Backup PTO Report:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getBackupPtoReport() == null ? "" : emp.getBackupPtoReport().getFullName(), normalFont));
			employeeTable.addCell(workCell);

			// Type
			workCell.setPhrase(new Phrase("Type:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.getEmployeeType() == null ? "" : emp.getEmployeeType().getDescription(), normalFont));
			employeeTable.addCell(workCell);

			// Hourly
			workCell.setPhrase(new Phrase("Hourly:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(emp.isHourly() ? "Yes" : "No", normalFont));
			employeeTable.addCell(workCell);

			// Start Time
			workCell.setPhrase(new Phrase("Start Time:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(tf.format(emp.getStartTime()), normalFont));
			employeeTable.addCell(workCell);

			// End Time
			workCell.setPhrase(new Phrase("End Time:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(tf.format(emp.getEndTime()), normalFont));
			employeeTable.addCell(workCell);

			work = "";

			try {
				work = emp.getDepartment().getDepartmentName();
			} catch (NullPointerException e) {
			}

			// Department
			workCell.setPhrase(new Phrase("Department:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(work, normalFont));
			employeeTable.addCell(workCell);

			work = "";

			try {
				work = emp.getDepartment().getDepartmentHead().getFullName();
			} catch (NullPointerException e) {
			}

			// Department Head
			workCell.setPhrase(new Phrase("Department Head:", boldFont));
			employeeTable.addCell(workCell);
			workCell.setPhrase(new Phrase(work, normalFont));
			employeeTable.addCell(workCell);

			// Hire Date
			// workCell.setPhrase(new Phrase("Hire Date:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(sdf2.format(emp.getHireDate()),
			// normalFont));
			// employeeTable.addCell(workCell);

			// Start Date
			// workCell.setPhrase(new Phrase("Start Date:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(sdf2.format(emp.getStartDate()),
			// normalFont));
			// employeeTable.addCell(workCell);

			// Termination Date
			// workCell.setPhrase(new Phrase("Termination Date:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(emp.getTerminationDate() == null ?
			// "" : sdf2.format(emp.getTerminationDate()), normalFont));
			// employeeTable.addCell(workCell);

			// Rollover Days
			// workCell.setPhrase(new Phrase("Rollover Days:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new
			// Phrase(String.valueOf(emp.getRolloverDays()), normalFont));
			// employeeTable.addCell(workCell);

			// Comp Time Granted
			// workCell.setPhrase(new Phrase("Comp-Time Granted:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new
			// Phrase(String.valueOf(emp.getCompTimeGranted()), normalFont));
			// employeeTable.addCell(workCell);

			// Comp Time Expires
			// workCell.setPhrase(new Phrase("Comp-Time Expires:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(emp.getCompTimeExpireDate() == null
			// ? "" : sdf2.format(emp.getCompTimeExpireDate()), normalFont));
			// employeeTable.addCell(workCell);

			// Paid Time Adjustment
			// workCell.setPhrase(new Phrase("PTO Adjustment:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new
			// Phrase(String.valueOf(emp.getPtoAdjustment()), normalFont));
			// employeeTable.addCell(workCell);

			// Paid Time Adjustment Expires
			// workCell.setPhrase(new Phrase("PTO Adjust Expires:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(emp.getPtoAdjustmentExpireDate() ==
			// null ? "" : sdf2.format(emp.getPtoAdjustmentExpireDate()),
			// normalFont));
			// employeeTable.addCell(workCell);

			// Review Date
			// workCell.setPhrase(new Phrase("Review Date:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(emp.getReviewDate() == null ? "" :
			// sdf2.format(emp.getReviewDate()), normalFont));
			// employeeTable.addCell(workCell);

			// Is Super
			// workCell.setPhrase(new Phrase("Is Supervisor:", boldFont));
			// employeeTable.addCell(workCell);
			// workCell.setPhrase(new Phrase(emp.isSupervisor() ? "Yes" : "No",
			// normalFont));
			// employeeTable.addCell(workCell);
			
			employeeTable.addCell(blankLine);
			employeeTable.addCell(blankLine);
			employeeTable.addCell(blankLine);
			
			try {
				String path = "/mnt/archives/headshots/" + emp.getFullName() + ".jpg";
				Image img = Image.getInstance(path);
				
				workCell.setPhrase(new Phrase("Photo:", boldFont));
				employeeTable.addCell(workCell);

				img.setWidthPercentage(50);
				PdfPCell imageCell = new PdfPCell();
				imageCell.setColspan(2);
				imageCell.addElement(img);
				employeeTable.addCell(imageCell);
			} catch (Exception e) {
				System.err.println("Photo not found for: " + emp.getFullName());
			}

			employeeTable.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
			document.add(employeeTable);

			document.close();

			// setting some response headers
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			// setting the content type
			response.setContentType("application/pdf");

			// the contentlength is needed for MSIE!!!
			response.setContentLength(baos.size());

			// write ByteArrayOutputStream to the ServletOutputStream
			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();
		} catch (DocumentException de) {
			de.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
		HttpServletResponse response) throws ServletException, IOException {
		makePdf(request, response);
	}
}
