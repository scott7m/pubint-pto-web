package com.pubint.pto.listeners;

import javax.ejb.EJB;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.pubint.pto.timers.DroneInterface;
import com.pubint.pto.timers.TimerData;

public class AttendanceReportListener implements ServletContextListener {
	ServletContext context;

	@EJB(beanName = "SendDailyAttendance")
	private DroneInterface timerControl;

	public void contextInitialized(ServletContextEvent contextEvent) {
		System.out.println("Context Created...Enable twice daily attendance report emails...");

		context = contextEvent.getServletContext();

		timerControl.setSchedule(new TimerData("System", "Auto-Enable timers..."));
	}

	public void contextDestroyed(ServletContextEvent contextEvent) {
		context = contextEvent.getServletContext();

		System.out.println("Context being destroyed...Disable twice daily attendance report emails...");
	}
}
