package com.pubint.pto.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class Menu extends SimpleTagSupport {
	public Menu() {
		super();
	}

	public void doTag() throws JspException {
		PageContext pageContext = (PageContext) getJspContext();

		JspWriter out = pageContext.getOut();

		String output = "" +
			"\t\t<div id=\"header\" class=\"asRow\">\n" +
			"\t\t\t<img src=\"./images/pil.png\" alt=\"Publications International, LTD\" class=\"logo\" onclick=\"checkEscalation()\"/>\n" +
			"\t\t\t<nav class=\"top-bar\" data-topbar data-options=\"is_hover: false\">\n" +
			"\t\t\t\t<ul class=\"title-area\">\n" +
			"\t\t\t\t\t<!-- Title Area -->\n" +
			"\t\t\t\t\t<li class=\"name\"> </li>\n" +
			"\n" +
			"\t\t\t\t\t<!-- Remove the class \"menu-icon\" to get rid of menu icon. Take out \"Menu\" to just have icon alone -->\n" +
			"\t\t\t\t\t<li class=\"toggle-topbar menu-icon\"><a href=\"\">Menu</a></li>\n" +
			"\t\t\t\t</ul>\n" +
			"\n" +
			"\t\t\t\t<section class=\"top-bar-section\">\n" +
			"\t\t\t\t\t<ul class=\"left\">\n" +
			"\t\t\t\t\t\t<li class=\"has-dropdown\"><a href=\"/\">Publications International PTO System</a>\n" +
			"\t\t\t\t\t\t\t<ul class=\"dropdown\">\n" +
			"\t\t\t\t\t\t\t\t<li><label>Main Menu</label></li>\n" +
			"\t\t\t\t\t\t\t\t<li class=\"has-dropdown\"><a href=\"#\">Requests</a>						\n" +
			"\t\t\t\t\t\t\t\t\t<ul class=\"dropdown\">\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><label>Request Menu</label></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/requests.jsp\">Request PTO</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t</ul>\n" +
			"\t\t\t\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t\t\t\t<li class=\"has-dropdown\"><a href=\"#\">Administration</a>\n" +
			"\t\t\t\t\t\t\t\t\t<ul class=\"dropdown\">\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><label>Administration Menu</label></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/departments.jsp\" class=\"hidden administration\">Department Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/employees.jsp\">Employee View/Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/calendars.jsp\" class=\"hidden administration\">Calendar Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/locations.jsp\" class=\"hidden administration\">Location Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/timeTypes.jsp\" class=\"hidden administration\">Time Type Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/requestTypes.jsp\" class=\"hidden administration\">Request Type Maintenance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/resetPassword.jsp\" class=\"hidden administration\">Reset Password</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./pto/changePassword.jsp\">Change Password</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t</ul>\n" +
			"\t\t\t\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t\t\t\t<li class=\"has-dropdown\"><a href=\"#\">Reports</a>\n" +
			"\t\t\t\t\t\t\t\t\t<ul class=\"dropdown\">\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><label>Reports Menu</label></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/employeeList.jsp\">Employee List</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/dailyAttendance.jsp\">Daily Attendance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/departmentalDaily.jsp\">Daily Departmental Attendance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/departmentalWeekly.jsp\">Weekly Departmental Attendance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/departmentalMonthly.jsp\">Monthly Departmental Attendance</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/requestException.jsp\" class=\"hidden administration\">Unpaid PTO (Exception) Report</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/unusedPtoReport.jsp\" class=\"hidden administration\">Unused PTO Report</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./reports/terminationsByDept.jsp\" class=\"hidden administration\">Terminated Employee Listing</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t\t<li><a href=\"./SummerViolations\" class=\"hidden administration\" target=\"_blank\">Summer Hours Violations</a></li>\n" +
			"\t\t\t\t\t\t\t\t\t</ul>\n" +
			"\t\t\t\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t\t\t</ul>\n" +
			"\t\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t</ul>\n" +
			"\n" +
			"\t\t\t\t\t<ul class=\"right\">\n" +
			"\t\t\t\t\t\t<li class=\"has-form\">\n" +
			"\t\t\t\t\t\t\t<a href=\"/Logout\" class=\"button\">Logout</a>\n" +
			"\t\t\t\t\t\t</li>\n" +
			"\t\t\t\t\t</ul>\n" +
			"\t\t\t\t</section>\n" +
			"\t\t\t</nav>\n" +
			"\t\t</div>\n";

		try {
			out.println(output);
		} catch (Exception e) {
			System.out.println("Exception building the tag text: " + e.getMessage());

			e.printStackTrace();
		}
	}

	protected void finalize() throws Throwable {
		super.finalize();
	}
}