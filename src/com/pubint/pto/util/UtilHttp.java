package com.pubint.pto.util;

import javax.servlet.http.HttpServletRequest;

public class UtilHttp {

	public UtilHttp() {
		super();
	}

	public static StringBuffer getServerRootUrl(HttpServletRequest request) {
		StringBuffer requestUrl = new StringBuffer();
		requestUrl.append(request.getScheme());
		requestUrl.append("://" + request.getServerName());

		if (request.getServerPort() != 80 && request.getServerPort() != 443)
			requestUrl.append(":" + request.getServerPort());

		return requestUrl;
	}

	public static StringBuffer getSecureServerRootUrl(HttpServletRequest request) {
		StringBuffer requestUrl = new StringBuffer();
		requestUrl.append(request.getScheme());
		requestUrl.append("://" + request.getServerName());
		requestUrl.append(":8443");

		System.out.println(requestUrl);

		return requestUrl;
	}

	public static String getApplicationName(HttpServletRequest request) {
		String appName = "";

		appName = request.getContextPath();

		return appName;
	}

	public static String getFullRootUrl(HttpServletRequest request) {
		StringBuffer retValue = getServerRootUrl(request);

		if (! getApplicationName(request).equals("")) {
			retValue.append("/" + getApplicationName(request));
		}

		return retValue.toString();
	}

}
