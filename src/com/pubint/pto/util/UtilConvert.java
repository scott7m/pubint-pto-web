package com.pubint.pto.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UtilConvert {

	public UtilConvert() {
		super();
		
		System.out.println("Initializing a UtilConvert object...");
	}

	public static long toLong(String stringLong) {
		System.out.println("Trying to convert string toLong: " + stringLong);
		
		long tempLong = 0;

		try {
			tempLong = (stringLong.equals("")) ? 0 : Long.parseLong(stringLong);
		} catch (final NullPointerException npe) {
			System.out.println("Null Pointer Exception: " + npe.getMessage());
		} catch (final NumberFormatException nfex) {
			System.out.println("Number Format Exception: " + nfex.getMessage());
		}

		return tempLong;
	}

	public static int toInt(String stringInt) {
		System.out.println("Trying to convert string toInt: " + stringInt);
		
		int tempInt = 0;

		try {
			tempInt = (stringInt.equals("")) ? 0 : Integer.parseInt(stringInt);
		} catch (final NullPointerException npe) {
			System.out.println("Null Pointer Exception: " + npe.getMessage());
		} catch (final NumberFormatException nfex) {
			System.out.println("Number Format Exception: " + nfex.getMessage());
		}

		return tempInt;
	}

	public static double toDouble(String stringDouble) {
		double tempDouble = 0;

		try {
			tempDouble = (stringDouble.equals("")) ? 0 : Double.parseDouble(stringDouble);
		} catch (final NullPointerException ex) {
			tempDouble = 0;
		} catch (final NumberFormatException nfex) {
			tempDouble = 0;
		}

		return tempDouble;
	}

	public static String toString(String someString) {
		return toString(someString, "");
	}

	public static String toString(String someString, String defaultValue) {
		System.out.println("Trying to convert a string to a string (default: " + defaultValue + "): " + someString);
		
		return (someString == null) ? defaultValue.trim() : someString.trim();
	}

	public static String to_Element(String elementName, String elementValue) {
		String returnValue = "";

		if (! elementName.equals(""))
			if (! elementValue.equals("")) {
				String work = "";

				work = elementValue.replaceAll("&", "&amp;");
				work = work.replaceAll("<", "&lt;");
				work = work.replaceAll(">", "&gt;");

				returnValue += "<" + elementName + ">";
				returnValue += work;
				returnValue += "</" + elementName + ">";
			}
		return returnValue;
	}

	public static String to_Element(String elementName) {
		String returnValue = "";

		if (! elementName.equals("")) {
			returnValue += "<" + elementName + "> </" + elementName + ">";
		}

		return returnValue;
	}

	public static String to_Element(String elementName, long elementData) {
		return to_Element(elementName, String.valueOf(elementData));
	}

	public static String toEscaped(String replaceString) {
		String work = "";

		try {		
			work = replaceString.replaceAll("\\\\", "\\\\\\\\");
			work = work.replaceAll("'", "\\\\'");
			work = work.replaceAll("\"", "\\\\\"");
			
		} catch (NullPointerException npe) {
			// go ahead and return the blank string
		}

		return work;
	}

	public static String decode(String work) {
		String result = "";

		try {
			result = URLDecoder.decode(work, "UTF-8");
		} catch (UnsupportedEncodingException uee) {
		} catch (NullPointerException npe) {
		}

		return result;
	}

	public static void emptyResponse(HttpServletRequest request, HttpServletResponse response) {
		String result = "";

		result += "<?xml version='1.0' encoding='utf-8' ?>\n";
		result += "<response type='object' />\n";

		try {
			response.setContentType("text/xml");
			PrintWriter out = response.getWriter();

			out.println(result);
		} catch (IOException ex) {
		}
	}
	
	public static StringReader createReader(String param) {
		String fixed = param.replace("&", "&amp;");
		
		return new StringReader(fixed);
	}
}
