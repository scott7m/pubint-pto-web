<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
String systemName ="PIL PTO Tracking";
String pageName = "Maintain Locations";
%>
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />
<%
	// put any parameter grabs here
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/pto/locations.js" language="JavaScript" type="text/javascript"></script>
	</head>
	
	<body class="tundra">
<pto:pageMenu />

		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="locationForm">
					<div class="row">
						<div class="small-8 columns">
							<label>Location</label>
							<select id="locSelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Location Name</label>
							<input type="text" id="locName" size=30 maxLength=100 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Address 1</label>
							<input type="text" id="locAdd1" size=40 maxLength=60 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Address 2</label>
							<input type="text" id="locAdd2" size=40 maxLength=60 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Address 3</label>
							<input type="text" id="locAdd3" size=40 maxLength=60 />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns">
							<label>City</label>
							<input type="text" id="locCity" size=20 maxLength=30 />
						</div>
						<div class="small-2 columns">
							<label>State</label>
							<input type="text" id="locState" size=10 maxLength=30 />
						</div>
						<div class="small-4 columns">
							<label>Postal Code</label>
							<input type="text" id="locZip" size=10 maxLength=30 />
						</div>
					</div>
					<div class="row">
						<div class="small-7 columns">
							<label>Country</label>
							<input type="text" id="locCountry" size=30 maxLength=50 />
						</div>
						<div class="small-5 columns">
							<label>Phone</label>
							<input type="text" id="locPhone" size=20 maxLength=30 />
						</div>
					</div>
					<div class="row">
						<div class="small-4 columns">
							<label>Calendar</label>
							<select id="calSelect"></select>
						</div>
					</div>
				</form>
				<div>
					<input type="button" name="updateButton" id="updateButton" value="Submit"  />
					<input type="button" name="deleteButton" id="deleteButton" value="Remove" />
					<input type="button" name="closeButton" id="closeButton" value="Close Window" cssClass="closeButton" class="closeButton"/>
				</div>
			</div>
		</div>
	</body>
</html>
