<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
String systemName ="PIL PTO Tracking";
String pageName = "Maintain Time Types";
%>
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />
<%
	// put any parameter grabs here
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/pto/timeTypes.js" language="JavaScript" type="text/javascript"></script>
	</head> 
	
	<body class="tundra">
<pto:pageMenu />
		
		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="timeType">
					<div class="row">
						<div class="small-12 columns">
							<label>Time Selection</label>
							<select id="timeSelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Time Type</label>
							<select id="typeCategorySelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Description</label>
							<input type="text" id="description" length="30" maxLength="45" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Location</label>
							<select id="locSelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns">
							<label>Years Break 1</label>
							<input type="text" id="yrsBrk1"  length="15" maxLength="15" />
						</div>
						<div class="small-6 columns">
							<label>Maximum</label>
							<input type="text" id="yrsBrk1Max" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns">
							<label>Years Break 2</label>
							<input type="text" id="yrsBrk2" length="15" maxLength="15" />
						</div>
						<div class="small-6 columns">
							<label>Maximum</label>
							<input type="text" id="yrsBrk2Max" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns">
							<label>Years Break 3</label>
							<input type="text" id="yrsBrk3" length="15" maxLength="15" />
						</div>
						<div class="small-6 columns">
							<label>Maximum</label>
							<input type="text" id="yrsBrk3Max" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Maximum Base PTO</label>
							<input type="text" id="maximum" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Maximum Rollover</label>
							<input type="text" id="maximumRollover" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 1 Month</label>
							<input type="text" id="brk1Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk1Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 2 Month</label>
							<input type="text" id="brk2Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk2Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 3 Month</label>
							<input type="text" id="brk3Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk3Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 4 Month</label>
							<input type="text" id="brk4Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk4Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 5 Month</label>
							<input type="text" id="brk5Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk5Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 6 Month</label>
							<input type="text" id="brk6Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk6Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 7 Month</label>
							<input type="text" id="brk7Date"  class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk7Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 8 Month</label>
							<input type="text" id="brk8Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk8Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 9 Month</label>
							<input type="text" id="brk9Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk9Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 10 Month</label>
							<input type="text" id="brk10Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk10Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 11 Month</label>
							<input type="text" id="brk11Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk11Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Break 12 Month</label>
							<input type="text" id="brk12Date" class="isMMDate" />
						</div>
						<div class="small-6 columns">
							<label>Quantity</label>
							<input type="text" id="brk12Qty" length="15" maxLength="15" />
						</div>
					</div>
					<div>
						<input type="button" id="updateButton" value="Update"  />
						<input type="button" id="deleteButton" value="Remove" />
						<input type="button" id="closeButton" value="Close Window" cssClass="closeButton" class="closeButton"/>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>
