<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
	String systemName = "PIL PTO Tracking";
	String pageName = "Maintain Notifications";
%>
<c:set var="pageName" value="Maintain Notifications" />
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />
<%
	// put any parameter grabs here
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/pto/notifications.js" language="JavaScript" type="text/javascript"></script>
		
	</head>
	
	<body class="tundra">
<pto:pageMenu />

		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="notificationForm">
					<div class="row">
						<div class="small-12 columns">
							<label>Notification</label>
							<select id="notifySelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Title</label>
							<input type="text" id="title" length="50" maxLength="100" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Template</label>
							<textarea id="template" cols="50" rows="10"></textarea>
						</div>
					</div>
					<div>
						<input type="button" id="updateButton" value="Update" />
						<input type="button" id="closeButton" value="Close Window" class="closeButton" />
					</div>
					<br />
					<hr />
				<div id="addDetail" class="hidden asRow">
					<div class="row">
						<div class="small-12 columns">
							<label>Recipient</label>
							<select id='employeeID' ></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Department</label>
							<select id='deptSelect' ></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<input type="button" id="addRecipientButton" value="Add Recipient" />
						</div>
					</div>
				</div>
				</form>
			</div>
			<div class="rightColumn">
				<form id="notifyDetailForm">
					<div id="detailDisplay"></div>
				</form>
			</div>
		</div>
	</body>
</html>
