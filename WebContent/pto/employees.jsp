<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
String systemName ="PIL PTO Tracking";
String pageName = "Maintain Employees";
%>
<c:set var="pageName" value="Maintain Employees" />
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />
<%
	// put any parameter grabs here
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/pto/employees.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/pto/requestHistory.js" language="JavaScript" type="text/javascript"></script>	
	</head>
	
	<body class="tundra">
<pto:pageMenu />

		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="employee">
					<div class="row">
						<div class="small-8 columns">
							<label>Employee</label>
							<select id="employeeID"></select>
						</div>
						<div id="employeePhoto" class="small-4 columns">
						</div>
					</div>
					<div class="row">
						<div class="small-5 columns">
							<label>First Name</label>
							<input type="text" id="firstName" size=30 maxLength=100 />
						</div>
						<div class="small-3 columns">
							<label>Middle Initial</label>
							<input type="text" id="midInitial" size=1 maxLength=1 />
						</div>
						<div class="small-4 columns">
							<label>Last Name</label>
							<input type="text" id="lastName" size=30 maxLength=100 />
						</div>
					</div>
					<div class="row">
						<div class="small-4 columns">
							<label>Office Location</label>
							<select id="locSelect"></select>
						</div>
						<div class="small-5 columns">
							<label>Department</label>
							<select id="deptSelect"></select>
						</div>
						<div class="small-3 column">
							<label>Include Inactive</label>
							<input type="checkbox" id="includeInactive" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Title</label>
							<input type="text" id="empTitle" size=30 maxLength=100 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Email Address</label>
							<input type="text" id="email" size=30 maxLength=100 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Extension/Phone</label>
							<input type="text" id="extension" size=15 maxLength=19 />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Direct Report</label>
							<select id="directReport" name="directReport"></select>
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-12 columns">
							<label>PTO Report</label>
							<select id="ptoReport" name="ptoReport"></select>
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-12 columns">
							<label>PTO Backup Report</label>
							<select id="ptoBackup" name="ptoBackup"></select>
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-8 columns">
							<label>Employee Type</label>
							<select id="empTypeSelect" name="empTypeSelect"></select>
						</div>
						<div class="small-4 columns">
							<label>Hourly?</label>
							<input type="checkbox" id="hourly" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Daily Start Time</label>
							<input dojoType="dijit.form.TimeTextBox" 
								id="startTime" name="startTime" 
								lang="en-us" value="T08:00:00" 
								constraints="{formatLength:'short', timePattern: 'h:mm a',
									clickableIncrement: 'T00:30:00',
									visibleIncrement: 'T00:30:00',
									visibleRange: 'T05:00:00'}" />
						</div>
						<div class="small-6 columns dijitCell">
							<label>Ending Time</label>
							<input dojoType="dijit.form.TimeTextBox" 
								id="endTime" name="endTime" 
								lang="en-us" value="T08:00:00" 
								constraints="{formatLength:'short', timePattern: 'h:mm a',
									clickableIncrement: 'T00:30:00',
									visibleIncrement: 'T00:30:00',
									visibleRange: 'T05:00:00'}" />
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-6 columns dijitCell">
							<label>Hire Date</label>
							<input type="text" id="hireDate" name="hireDate" class="isDate" />
						</div>
						<div class="small-6 columns dijitCell">
							<label>Start Date</label>
							<input type="text" id="startDate" name="startDate" class="isDate" />
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-8 columns dijitCell">
							<label>Termination Date</label>
							<input type="text" id="termDate" name="termDate" class="isDate" />
						</div>
						<div class="small-4 columns dijitCell">
							<label></label>
							<input type="button" id="terminateButton" value="Terminate" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns">
							<label>Rollover Days</label>
							<input type="text" id="rollover" name="rollover" size=5 maxLength=3 />
						</div>
						<div class="small-6 columns hide hrOnly">
							<label>Bonus One to Three's</label>
							<input type="text" id="bonusOneToThree" name="bonusOneToThree" size=5 maxLength=2 />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Comp Time Granted</label>
							<input type="text" id="compTime" size=5 maxLength=3 />
						</div>
						<div class="small-6 columns dijitCell">
							<label>Expiration</label>
							<input id="compTimeExpiration" type="text" name="compTimeExpiration" class="isDate" disabled="true"/>
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label>Paid Time Adjustment</label>
							<input type="text" id="timeAdjust" size=5 maxLength=3 />
						</div>
						<div class="small-6 columns dijitCell">
							<label>Expiration</label>
							<input id="timeAdjustExpiration" type="text" name="timeAdjustExpiration" class="isDate" disabled="true"/>
						</div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-8 columns dijitCell">
							<label>Review Date</label>
							<input type="text" id="reviewDate" name="reviewDate" class="isDate" />
						</div>
                        <div class="small-4 columns">
                            <button id='reviewToday' type='button'>Today</button>
                        </div>
					</div>
					<div class="row hide hrOnly">
						<div class="small-4 columns">
							<label>Summer Hours Participant?</label>
							<input type="checkbox" id="summerHours" />
						</div>
						<div class="small-3 columns">
							<label>Supervisor?</label>
							<input type="checkbox" id="supervisor" />
						</div>
						<div class="small-5 columns">
							<label>Request as Others?</label>
							<input type="checkbox" id="isReception" />
						</div>
					</div>
					<div class="asRow">
						<input type="button" id="updateButton" value="Submit" disabled="true"/>
						<!-- <input type="button" id="deleteButton" value="Remove" disabled="true"/> -->
						<input type="button" id="closeButton" value="Close Window" class="closeButton" />
					</div>
				</form>
			</div>
				
			<div class="rightColumn">
				<form id="eventAdd" class="hide hrOnly">
					<fieldset>
						<legend>Event History</legend>
						<div class="row">
							<div class="small-8 columns">
								<label>Event Type</label>
								<select id="eventTypeSelect" name="eventTypeSelect"></select>
							</div>
							<div class="small-4 columns">
								<label></label>
								<span id="eventID"></span>
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns dijitCell">
								<label>Event Date</label>
								<input type="text" id="eventDate" name="eventDate" class="isDate" />
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<label>Department</label>
								<select id="xferDeptSelect" name="xferDeptSelect"></select>
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<label>Grantor</label>
								<select id="eventGrantorSelect" name="eventGrantorSelect"></select>
							</div>
						</div>
						<div class="row">
							<div class="small-12 columns">
								<label>Comment</label>
								<textarea rows="5" cols="30" id="eventComment" name="eventComment"></textarea>
							</div>
						</div>
						<div class="row">
							<input type="button" id="addEventButton" value="Add Event"  />
							<input type="button" id="deleteEventButton" value="Delete Event" disabled="true" />
							<input type="button" id="clearEventButton" value="Clear Entry"  />
						</div>
					</fieldset>
				</form>
				
				<div id="eventHistory" class="panel hide hrOnly"></div>
				<div class="row hide hrOnly">
					<label>Request History Year</label>
					<select id="historyYearSelect"></select>
				</div>
				<div id="requestHistory" class="fullBreakdown"></div>
			</div>
		</div>
	</body>
</html>
