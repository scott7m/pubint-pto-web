<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
String systemName ="PIL PTO Tracking";
String pageName = "Request Time Off";
%>
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />

<%--Grabbing the current employee ID and name from the session --%>
<c:set var="eid" value="<%=session.getAttribute(\"userID\") %>" />
<c:set var="ename" value="<%=session.getAttribute(\"userFullName\") %>" />
<%
	// put any parameter grabs here
	String requestID = (request.getParameter("requestID") != null ? request.getParameter("requestID") : "");
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<link href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/pto/requests.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/pto/requestHistory.js" language="JavaScript" type="text/javascript"></script>
	</head>
	
	<body class="tundra">
<pto:pageMenu />

		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="request">
					<input type="hidden" name="requestID" id="requestID" value="<%=requestID %>" />
					<input type="hidden" name="statusID" id="statusID" value="" />
					<div class="row">
						<div class="small-12 columns">
							<label>Employee Name</label>
							<select id='employeeID' value="${eid}" disabled><option value="${eid}">${ename}</option></select>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label>Request Type
							<span id="reqInstructions"><b>(Select a request type)</b></span>
							</label>
							<select id="requestSelect" name="requestSelect"></select>
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label for="startDate">Starting Date</label>
							<input id="startDate" type="text" name="startDate" class="isDowDate" />
						</div>
						<div id='startTimeContainer' class="small-6 columns dijitCell">
							<label for="startTime">Starting Time</label>
							<input type="text" id="startTime" name="startTime" />
						</div>
					</div>
					<div class="row">
						<div class="small-6 columns dijitCell">
							<label for="endDate">Returning Date</label>
							<input id="endDate" type="text" name="endDate" class="isDowDate" />
						</div>
						<div id='endTimeContainer' class="small-6 columns dijitCell">
							<label for="endTime">Ending Time</label>
							<input type="text" id="endTime" name="endTime" />
						</div>
					</div>
					<div class="row">
						<div class="small-7 columns">
							<label for="numDays">Number of Days Requested</label>
							<input type="text" id="numDays" name="numDays" size=10 maxLength=5 disabled="true" />
						</div>
						<div class="small-5 columns">
							<label for="status">Status</label>
							<div class="informationCell"><span id="requestStatus"></span></div>
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns">
							<label for="requestComment">Comment</label>
							<textarea id="requestComment" name="requestComment" cols="50" rows="5"></textarea>
						</div>
					</div>
					<div>
						<input type="button" id="sendButton" value="Send Request" class='actionButton' />
						<input type="button" id="closeButton" value="Close Window" cssClass="closeButton" class="closeButton actionButton" />
						<input type="button" id="approveButton" value="Approve Request" class="hidden actionButton" pilProcessType="approve"/>
						<input type="button" id="rejectButton" value="Reject Request" class="hidden actionButton" pilProcessType="reject"/>
						<input type="button" id="cancelButton" value="Cancel Request" class="hidden actionButton" pilProcessType="cancel"/>
						<input type="button" id="printButton" value="Print Request" class="hidden actionButton" pilProcessType="print"/>
					</div>
					<div id="requestStatusDetail" class="row hide hrOnly"></div>
				</form>
			</div>
			<div class="rightColumn">
				<div class="row hide hrOnly">
					<label>Request History Year</label>
					<select id="historyYearSelect"></select>
				</div>
				<div id="requestHistory"></div>
			</div>
		</div>
	</body>
</html>
