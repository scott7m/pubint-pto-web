/**
 * departmentalDaily.js
 */

function clearResults() {
	$('#results').empty();
}

function generateReport() {
	clearResults();

	var selectedDate = dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
		selector: 'date'
	});

	var startingDate = strToDate(selectedDate);

	while (startingDate.getDay() > 0) {
		startingDate = offsetDate(startingDate, -1);
	}

	var shiftedDate = offsetDate(startingDate, 7);

	pilQueueCommand({
		action: "departmentalAttendance",
		startDate: fixDate(startingDate),
		endDate: fixDate(shiftedDate)
	});

	pilPushQueue("./PtoRequest", showAbsences);
}

function formattedDate(date, time) {
	var dateParts = date.split('-');
	var timeParts = time.split(':');

	return dateParts[1] + "/" + dateParts[2] + "@" + timeParts[0] + ":" + timeParts[1];
}

function formattedDates(requestXML) {
	var startDate = getNodeValue(requestXML, "startDate");
	var endDate = getNodeValue(requestXML, "endDate");

	var startTime = getNodeValue(requestXML, "startTime");
	var endTime = getNodeValue(requestXML, "endTime");

	return formattedDate(startDate, startTime) + " - " + formattedDate(endDate, endTime);
}

function amPmOneToThree(requestXML, relatedDay) {
	var startTimeHour = (getNodeValue(requestXML, "startTime").split(':'))[0];
	var endTimeHour = (getNodeValue(requestXML, "endTime").split(':'))[0];

	if (startTimeHour < 12) {
		if (endTimeHour < 12) {
			return "AM";
		} else {
			return "Midday";
		}
	} else {
		return "PM";
	}

}

function amPmDaily(requestXML, relatedDate) {
	var relatedNoon = relatedDate;

	relatedNoon.setHours(12); // noon

	var startDate = strToDate(getNodeValue(requestXML, "startDate"));
	var startTime = getNodeValue(requestXML, "startTime").split(':');

	startDate.setHours(startTime[0], startTime[1]);

	var endDate = strToDate(getNodeValue(requestXML, "endDate"));
	var endTime = getNodeValue(requestXML, "endTime").split(':');

	endDate.setHours(endTime[0], endTime[1]);

	if (startDate < relatedNoon) {
		if (endDate < relatedNoon) {
			return "AM";
		} else {
			return "All Day";
		}
	} else {
		return "PM";
	}
}

function amPm(requestXML, relatedDay) {
	if (getNodeAttribute(requestXML, "oneToThree") == 'true') {
		return amPmOneToThree(requestXML, relatedDay);
	} else {
		return amPmDaily(requestXML, relatedDay);
	}
}

function showDOWHeader() {
	var week = baseDIV.cloneNode(false);

	week.className = "row calendarRow";

	var calendar = $("#results");

	calendar[0].appendChild(week);

	var daysOfTheWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];

	daysOfTheWeek.forEach(function(dayName) {
		var dayOfWeek = baseDIV.cloneNode(false);

		dayOfWeek.id = "header-" + dayName;
		dayOfWeek.className = "small-2 column calendarDay";

		dayOfWeek.appendChild(createH5(dayName));

		week.appendChild(dayOfWeek);
	});

	var dayOfWeek = baseDIV.cloneNode(false);

	dayOfWeek.id = "header-Saturday";
	dayOfWeek.className = "small-2 columns";

	week.appendChild(dayOfWeek);
}

function showAbsences(type, response, event) {
	showDOWHeader();

	var xmlDoc = response.xhr.responseXML;

	// set a variable to the container that will hold the calendar
	var calendar = $("#results");

	var startingDate = dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
		selector: 'date'
	});

	// change to a JavaScript date object...
	startingDate = strToDate(startingDate);

	// shift starting date to the first Sunday preceding...
	startingDate = offsetDate(startingDate, -startingDate.getDay());

	var week = null;
	var dayOfWeek = null;

	var weekNumber = 0;

	var dates = xmlDoc.getElementsByTagName("dates");

	var workDay = startingDate;

	for (var i = 0; i < dates.length; i++) {
		var day = dates[i];

		var requestDate = strToDate(getNodeValue(day, "date"));

		// make empty blocks until we get to the starting date...
		while (workDay < requestDate) {
			// start a new week for Sunday and add it to the calendar
			if (workDay.getDay() == 0) {
				weekNumber++;

				week = baseDIV.cloneNode(false);
				week.id = "week-" + weekNumber;

				week.className = "row calendarRow";

				calendar[0].appendChild(week);
			}

			dayOfWeek = baseDIV.cloneNode(false);
			dayOfWeek.id = "week-" + weekNumber + ".day-" + workDay.getDay();

			dayOfWeek.className = "small-2 columns calendarDay";

			if (workDay.getDay() > 0) {
				if (workDay.getDay() < 6) {
					dayOfWeek.appendChild(createH5(workDay.getDate()));
				}

				week.appendChild(dayOfWeek);
			}

			workDay = offsetDate(workDay, 1);
		}

		if (week == null) {
			weekNumber = 1;

			week = baseDIV.cloneNode(false);
			week.id = "week-" + weekNumber;

			week.className = "row calendarRow";

			calendar[0].appendChild(week);
		}

		dayOfWeek = baseDIV.cloneNode(false);
		dayOfWeek.id = week.id + ".day-" + requestDate.getDay();

		dayOfWeek.className = "small-2 columns calendarDay";

		if (workDay.getDay() > 0) {
			if (workDay.getDay() < 6) {
				dayOfWeek.appendChild(createH5(workDay.getDate()));
			}

			week.appendChild(dayOfWeek);
		}

		if ( (workDay.getDay() == 0) || (workDay.getDay() == 6)) {
			workDay = offsetDate(workDay, 1);

			continue;
		}

		var requests = day.getElementsByTagName("request");

		var employee;
		var requestType;
		var requestDates;
		var requestTimeframe;
		var duration;

		var showAbsence;

		for (var j = 0; j < requests.length; j++) {
			request = requests[j];

			employee = getNodeValue(request, "employeeName");
			requestType = getNodeValue(request, "requestType");
			requestDates = formattedDates(request);
			requestTimeframe = amPm(request, requestDate);

			duration = getNodeValue(request, "daysTaken");

			showAbsence = document.createTextNode(employee + " (" + requestType + " " + requestTimeframe + ")");

			dayOfWeek.appendChild(showAbsence);
			dayOfWeek.appendChild(baseBR.cloneNode(false));
		}

		workDay = offsetDate(workDay, 1);
	}

	if (week == null) {
		return false;
	}

	for (var i = week.children.length; i < 7; i++) {
		dayOfWeek = baseDIV.cloneNode(false);
		dayOfWeek.id = week.id + ".day-" + i;

		dayOfWeek.className = "small-2 columns";

		week.appendChild(dayOfWeek);
	}

}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	var today = new Date();

	dijit.byId('reportDate').set("value", fixDate(today));

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);
