/**
 * JavaScript library for employeeList page (employeeList.jsp)
 */

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	pilQueueCommand({
		action: "activeEmployeeList"
	});

	pilPushQueue("./PtoEmployee", displayEmployeeList);
}

dojo.addOnLoad(init);
