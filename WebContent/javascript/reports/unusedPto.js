/**
 * unusedPto.js
 */

function generateReport() {
	var link = "/UnusedPtoReport?includeTerm="+dojo.byId('includeTerm').checked+
	"&reportDate=" + dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
		selector: 'date'
	});

	window.open(link, 'unusedPto');
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);