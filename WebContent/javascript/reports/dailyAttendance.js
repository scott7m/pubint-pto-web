/**
 * dailyAttendance.js
 */

function generateReport() {
	var link = "" + "/DailyAttendanceReport?reportDate="
			+ dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
				selector: 'date'
			});

	window.open(link, 'dailyAtt');
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	var today = new Date();

	dijit.byId('reportDate').set("value", fixDate(today));

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);