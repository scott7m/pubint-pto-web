/**
 * termsByDept.js
 */

function generateReport() {
	var link = "/TerminationsReport?fromDate=" + dojo.date.stamp.toISOString(dijit.byId('fromDate').getValue(), {
		selector: 'date'
	}) + "&toDate=" + dojo.date.stamp.toISOString(dijit.byId('toDate').getValue(), {
		selector: 'date'
	});

	window.open(link, 'unusedPto');
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);