/**
 * 
 */

function generateReport() {
	var link = "/RequestExceptionReport?startDate=" + dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
		selector: 'date'
	}) + "&endDate=" + dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
		selector: 'date'
	});

	window.open(link, 'reqException');
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);