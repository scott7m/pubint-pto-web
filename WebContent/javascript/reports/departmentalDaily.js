/**
 * departmentalDaily.js
 */

function clearResults() {
	$('#staff').empty();
}

function generateReport() {
	clearResults();

	var selectedDate = dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
		selector: 'date'
	});

	pilQueueCommand({
		action: "departmentalAttendance",
		startDate: selectedDate,
		endDate: selectedDate
	});

	pilPushQueue("./PtoRequest", showAbsences);
}

function formattedDate(date, time) {
	var dateParts = date.split('-');
	var timeParts = time.split(':');

	return dateParts[1] + "/" + dateParts[2] + "@" + timeParts[0] + ":" + timeParts[1];
}

function formattedDates(requestXML) {
	var startDate = getNodeValue(requestXML, "startDate");
	var endDate = getNodeValue(requestXML, "endDate");

	var startTime = getNodeValue(requestXML, "startTime");
	var endTime = getNodeValue(requestXML, "endTime");

	return formattedDate(startDate, startTime) + " - " + formattedDate(endDate, endTime);
}

function showAbsences(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var requests = xmlDoc.getElementsByTagName("request");

	var request;

	var row;
	var cell;

	var employee;
	var requestType;
	var dates;
	var duration;

	for (var i = 0; i < requests.length; i++) {
		request = requests[i];

		employee = getNodeValue(request, "employeeName");
		requestType = getNodeValue(request, "requestType");
		dates = formattedDates(request);
		duration = getNodeValue(request, "daysTaken");

		row = baseTR.cloneNode(false);

		cell = baseTD.cloneNode(false);
		cell.appendChild(document.createTextNode(employee));
		row.appendChild(cell);

		cell = baseTD.cloneNode(false);
		cell.appendChild(document.createTextNode(requestType));
		row.appendChild(cell);

		cell = baseTD.cloneNode(false);
		cell.appendChild(document.createTextNode(dates));
		row.appendChild(cell);

		cell = baseTD.cloneNode(false);
		cell.appendChild(document.createTextNode(duration));
		row.appendChild(cell);

		$("#staff").append(row);
	}
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	var today = new Date();

	dijit.byId('reportDate').set("value", fixDate(today));

	dojo.connect(dojo.byId('generateButton'), 'onclick', generateReport);
}

dojo.addOnLoad(init);