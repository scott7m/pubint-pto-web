// employees.js - Employee maintenance screen for PTO Tracking
function loadPage() {
	clearEntry();
	clearEvent();

	loadEmployeeDeptSelect();
	loadReportSelects();
	loadLocSelect();
	loadEmpTypeSelect();
	loadEventTypeSelect();
	loadEmpSelect();
}

function clearEntry() {
	var today = new Date();

	dojo.byId('firstName').value = "";
	dojo.byId('midInitial').value = "";
	dojo.byId('lastName').value = "";
	dojo.byId('locSelect').value = "0";
	dojo.byId('deptSelect').value = "0";
	dojo.byId('empTitle').value = "";
	dojo.byId('email').value = "";
	dojo.byId('extension').value = "";
	dojo.byId('directReport').value = "0";
	dojo.byId('ptoReport').value = "0";
	dojo.byId('ptoBackup').value = "0";
	dojo.byId('empTypeSelect').value = "0";
	dojo.byId('hourly').checked = false;
	dijit.byId('hireDate').set("value", fixDate(today));
	dijit.byId('startDate').set("value", fixDate(today));
	dijit.byId('termDate').set("value", "");
	dijit.byId('reviewDate').set("value", "");
	dojo.byId('supervisor').checked = false;
	dojo.byId('summerHours').checked = false;
	dojo.byId('isReception').checked = false;
	dojo.byId('rollover').value = "";
	dojo.byId('compTime').value = "";
	dijit.byId('compTimeExpiration').set("value", "");
	dojo.byId('timeAdjust').value = "";
	dijit.byId('timeAdjustExpiration').set("value", "");

	dijit.byId('startTime').set("value", "T08:30:00");
	dijit.byId('endTime').set("value", "T17:00:00");

	$("#employeePhoto").empty();
}

function disableAllFields() {
	dojo.byId('firstName').disabled = true;
	dojo.byId('midInitial').disabled = true;
	dojo.byId('lastName').disabled = true;
	dojo.byId('locSelect').disabled = true;
	dojo.byId('deptSelect').disabled = true;
	dojo.byId('empTitle').disabled = true;
	dojo.byId('email').disabled = true;
	dojo.byId('extension').disabled = true;
	dojo.byId('directReport').disabled = true;
	dojo.byId('ptoReport').disabled = true;
	dojo.byId('ptoBackup').disabled = true;
	dojo.byId('empTypeSelect').disabled = true;
	dojo.byId('hourly').disabled = true;
	dijit.byId('hireDate').set("readOnly", true);
	dijit.byId('startDate').set("readOnly", true);
	dijit.byId('termDate').set("readOnly", true);
	dojo.byId('terminateButton').disabled = true;
	dijit.byId('reviewDate').set("readOnly", true);
	dojo.byId('supervisor').disabled = true;
	dojo.byId('summerHours').disabled = true;
	dojo.byId('isReception').disabled = true;
	dojo.byId('rollover').disabled = true;
	dojo.byId('compTime').disabled = true;
	dojo.byId('compTimeExpiration').disabled = true;
	dojo.byId('timeAdjust').disabled = true;
	dojo.byId('timeAdjustExpiration').disabled = true;
	dojo.byId('bonusOneToThree').disabled = true;
	dijit.byId('startTime').set("readOnly", true);
	dijit.byId('endTime').set("readOnly", true);

	dojo.byId('eventTypeSelect').disabled = true;
	dojo.byId('eventDate').disabled = true;
	dojo.byId('xferDeptSelect').disabled = true;
	dojo.byId('eventGrantorSelect').disabled = true;
	dojo.byId('eventComment').readOnly = true;

	dojo.byId('addEventButton').disabled = true;
	dojo.byId('clearEventButton').disabled = true;
}

function loadEmpSelect(asReceptionist) {
	pilQueueCommand({
		action: "selectList",
		asReceptionist: (asReceptionist == true ? true : false)
	});

	if (asReceptionist) {
		pilPushQueue("./PtoEmployee", fillEmpSelectAsReceptionist);
	} else {
		pilPushQueue("./PtoEmployee", fillEmpSelect);
	}
}

function fillEmpSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var empSelect = dojo.byId("employeeID");

	pilLoadSelectBox(empSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");

	empSelect.value = empSelect.childNodes[0].value;

	loadEmployee();
}

function fillEmpSelectAsReceptionist(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var empSelect = dojo.byId("employeeID");

	pilLoadSelectBox(empSelect, "employee", "id", "fullName", xmlDoc, false, "Add New");

	empSelect.value = empSelect.childNodes[0].value;

	loadEmployee();
}

function loadEmpTypeSelect() {
	pilQueueCommand({
		action: "typeSelectList"
	});

	pilPushQueue("./PtoEmployee", fillEmpTypeSelect);
}

function fillEmpTypeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var empTypeSelect = dojo.byId("empTypeSelect");
	var selectedType = empTypeSelect.value;

	if (selectedType == "") {
		selectedType = "0";
	}

	pilLoadSelectBox(empTypeSelect, "empType", "id", "description", xmlDoc, false, "None Selected");

	empTypeSelect.value = selectedType;
}

function loadReportSelects() {
	pilQueueCommand({
		action: "supervisorSelectList"
	});

	pilPushQueue("./PtoEmployee", fillReportSelects);
}

function fillReportSelects(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var dirRepSelect = dojo.byId('directReport');
	var ptoRepSelect = dojo.byId('ptoReport');
	var ptoBackSelect = dojo.byId('ptoBackup');
	var eventGrantorSelect = dojo.byId('eventGrantorSelect');

	var selectedDirRep = dirRepSelect.value;

	if (selectedDirRep == "") {
		selectedDirRep = "0";
	}

	var selectedPtoRep = ptoRepSelect.value;

	if (selectedPtoRep == "") {
		selectedPtoRep = "0";
	}

	var selectedPtoBack = ptoBackSelect.value;

	if (selectedPtoBack == "") {
		selectedPtoBack = "0";
	}

	var selectedGrantor = eventGrantorSelect.value;

	if (selectedGrantor == "") {
		selectedGrantor = "0";
	}

	pilLoadSelectBox(dirRepSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");
	pilLoadSelectBox(ptoRepSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");
	pilLoadSelectBox(ptoBackSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");
	pilLoadSelectBox(eventGrantorSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");

	dirRepSelect.value = selectedDirRep;
	ptoRepSelect.value = selectedPtoRep;
	ptoBackSelect.value = selectedPtoBack;
	eventGrantorSelect.value = selectedGrantor;
}

function loadEmployeeDeptSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pushQueue("./PtoDepartment", fillEmployeeDeptSelect);
}

function fillEmployeeDeptSelect(xmlDoc) {
	fillDeptSelect(xmlDoc);
	fillXFerDeptSelect(xmlDoc);
}

function loadEventTypeSelect() {
	pilQueueCommand({
		action: "eventTypeSelectList"
	});

	pilPushQueue("./PtoEmployee", fillEventTypeSelect);
}

function fillEventTypeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var eventTypeSelect = dojo.byId("eventTypeSelect");
	var selectedType = eventTypeSelect.value;

	if (selectedType == "") {
		selectedType = "0";
	}

	pilLoadSelectBox(eventTypeSelect, "empEventType", "id", "eventName", xmlDoc, false, "None Selected");

	eventTypeSelect.value = selectedType;
}

function loadLocSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoLocation", fillLocSelect);
}

function fillLocSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var locSelect = dojo.byId("locSelect");
	var selectedLoc = locSelect.value;

	if (selectedLoc == "") {
		selectedLoc = "0";
	}

	pilLoadSelectBox(locSelect, "location", "id", "name", xmlDoc, false, "None Selected");

	locSelect.value = selectedLoc;
}

function loadEmployee() {
	clearEntry();

	pilQueueCommand({
		action: "sendXML",
		employeeID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoEmployee", displayEmp);
}

function displayEmp(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();
	clearEvent();

	var employee = xmlDoc.getElementsByTagName("employee")[0];

	var employeeID = getNodeAttribute(employee, "id");

	var departmentID = getNodeValue(employee, "departmentID");
	var locationID = getNodeValue(employee, "locationID");
	var typeID = getNodeValue(employee, "employeeTypeID");

	dojo.byId('firstName').value = getNodeValue(employee, "firstName");
	dojo.byId('midInitial').value = getNodeValue(employee, "middleInitial");
	dojo.byId('lastName').value = getNodeValue(employee, "lastName");

	var path = "/archives/headshots/" + getNodeValue(employee, "fullName") + ".jpg";

	$("#employeePhoto").empty().append($("<img>").attr('src', path));

	dojo.byId('deptSelect').value = departmentID;
	dojo.byId('locSelect').value = locationID;
	dojo.byId('empTypeSelect').value = typeID;

	dojo.byId('empTitle').value = getNodeValue(employee, "title");
	dojo.byId('email').value = getNodeValue(employee, "email");
	dojo.byId('extension').value = getNodeValue(employee, "extension");
	dojo.byId('hourly').checked = (getNodeValue(employee, "hourly") == "true");
	dojo.byId('supervisor').checked = (getNodeValue(employee, "supervisor") == "true");
	dojo.byId('summerHours').checked = (getNodeValue(employee, "summerHours") == "true");
	dojo.byId('isReception').checked = (getNodeValue(employee, "receptionRole") == "true");

	dijit.byId('startTime').set("value", "T" + getNodeValue(employee, "startTime"));
	dijit.byId('endTime').set("value", "T" + getNodeValue(employee, "endTime"));

	if (getNodeValue(employee, "hireDate") != "") {
		dijit.byId('hireDate').set("value", getNodeValue(employee, "hireDate"));
	}

	if (getNodeValue(employee, "startDate") != "") {
		dijit.byId('startDate').set("value", getNodeValue(employee, "startDate"));
	}

	if (getNodeValue(employee, "terminationDate") != "") {
		// dijit.byId('termDate').disabled = false;
		dijit.byId('termDate').set("value", getNodeValue(employee, "terminationDate"));
		// dojo.byId('terminateButton').disabled = true;
	} else {
		// dijit.byId('termDate').disabled = true;
		// dojo.byId('terminateButton').disabled = false;
	}

	if (getNodeValue(employee, "reviewDate") != "") {
		dijit.byId('reviewDate').set("value", getNodeValue(employee, "reviewDate"));
	}

	dojo.byId('rollover').value = getNodeValue(employee, "rolloverDays");

	if (getNodeValue(employee, "compTimeGranted") != "0") {
		dijit.byId('compTimeExpiration').disabled = false;
		dojo.byId('compTime').value = getNodeValue(employee, "compTimeGranted");
	}

	if (getNodeValue(employee, "ptoAdjustment") != "0") {
		dijit.byId('timeAdjustExpiration').disabled = false;
		dojo.byId('timeAdjust').value = getNodeValue(employee, "ptoAdjustment");
	}

	dojo.byId('bonusOneToThree').value = getNodeValue(employee, "bonusOneToThree");

	dijit.byId('compTimeExpiration').set("value", getNodeValue(employee, "compTimeExpireDate"));
	dijit.byId('timeAdjustExpiration').set("value", getNodeValue(employee, "ptoAdjustmentExpireDate"));

	dojo.byId('directReport').value = getNodeValue(employee, "directReportID");
	dojo.byId('ptoReport').value = getNodeValue(employee, "ptoReportID");
	dojo.byId('ptoBackup').value = getNodeValue(employee, "ptoBackupID");

	displayEmployeeEventHistory(xmlDoc);

	loadEmployeeHistory(employeeID);
}

function displayEmployeeEventHistory(xmlDoc) {
	var empHistory = xmlDoc.getElementsByTagName("history");

	var eventDiv = dojo.byId('eventHistory');

	pilKillChildren(eventDiv);

	var table = baseTABLE.cloneNode(false);

	eventDiv.appendChild(table);

	var headerRow = baseTR.cloneNode(false);

	var headCell = baseTH.cloneNode(false);
	var headText = document.createTextNode("Event Date");

	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	workCell = baseTH.cloneNode(false);
	headerRow.appendChild(workCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Event Type");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	workCell = baseTH.cloneNode(false);
	headerRow.appendChild(workCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Department");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	workCell = baseTH.cloneNode(false);
	headerRow.appendChild(workCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Grantor");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	workCell = baseTH.cloneNode(false);
	headerRow.appendChild(workCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Comment");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	table.appendChild(headerRow);

	var workCell;
	var workText;
	var workRow;

	for (var i = 0; i < empHistory.length; i++) {
		var dept = getChildrenByTagName(empHistory[i], "department");
		var eventType = getChildrenByTagName(empHistory[i], "eventType");

		workRow = baseTR.cloneNode(false);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(empHistory[i], "eventDate"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);

		var eventName = getNodeValue(eventType[0], "eventName");
		var workA = baseA.cloneNode(false);

		workA.onclick = loadEvent;
		workA.pilID = getNodeAttribute(empHistory[i], "id");
		workA.pilDeptID = getNodeAttribute(dept[0], "id");
		workA.pilEventTypeID = getNodeAttribute(eventType[0], "id");
		workA.pilEventDate = getNodeValue(empHistory[i], "eventDate");
		workA.pilGrantorID = getNodeValue(empHistory[i], "grantorID");
		workA.pilComment = getNodeValue(empHistory[i], "comment");
		workA.innerHTML = eventName;
		workCell.appendChild(workA);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(dept[0], "departmentName"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workRow.appendChild(workCell);

		var grantorName = "";
		var grantorID = getNodeValue(empHistory[i], "grantorID");
		var grantorSelect = dojo.byId('eventGrantorSelect');

		for (var j = 0; j < grantorSelect.options.length; j++) {
			if (grantorSelect[j].value == grantorID) {
				grantorName = grantorSelect[j].text;
			}
		}

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(grantorName); // getNodeValue(empHistory[i],
		// "grantorName"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);

		var workTA = baseTEXTAREA.cloneNode(false);

		workTA.rows = "1";
		workTA.cols = "14";
		workTA.innerHTML = getNodeValue(empHistory[i], "comment");
		workCell.appendChild(workTA);
		workRow.appendChild(workCell);

		table.appendChild(workRow);
	}
}

function update() {
	var termDate = dijit.byId('termDate').get("displayedValue");
	var reviewDate = dijit.byId('reviewDate').get("displayedValue");

	if (termDate == "1970-01-01") {
		termDate = null;
	}

	if (reviewDate == "1970-01-01") {
		reviewDate = null;
	}

	pilQueueCommand({
		action: "update",
		empID: dojo.byId('employeeID').value,
		firstName: dojo.byId('firstName').value,
		midInitial: dojo.byId('midInitial').value,
		lastName: dojo.byId('lastName').value,
		locID: dojo.byId('locSelect').value,
		deptID: dojo.byId('deptSelect').value,
		title: dojo.byId('empTitle').value,
		empEmail: dojo.byId('email').value,
		extension: dojo.byId('extension').value,
		directReport: dojo.byId('directReport').value,
		ptoReport: dojo.byId('ptoReport').value,
		ptoBackup: dojo.byId('ptoBackup').value,
		typeID: dojo.byId('empTypeSelect').value,
		hourly: dojo.byId('hourly').checked,
		startTime: dojo.byId('startTime').value,
		endTime: dojo.byId('endTime').value,
		hireDate: dijit.byId('hireDate').get("displayedValue"),
		startDate: dijit.byId('startDate').get("displayedValue"),
		termDate: termDate,
		reviewDate: reviewDate,
		supervisor: dojo.byId('supervisor').checked,
		summerHours: dojo.byId('summerHours').checked,
		isReception: dojo.byId('isReception').checked,
		rollover: dojo.byId('rollover').value,
		compTime: dojo.byId('compTime').value,
		compTimeExpire: dijit.byId('compTimeExpiration').get("displayedValue"),
		ptoAdjustment: dojo.byId('timeAdjust').value,
		ptoAdjExpire: dijit.byId('timeAdjustExpiration').get("displayedValue"),
		bonusOneToThree: dojo.byId('bonusOneToThree').value
	});

	pilPushQueue("./PtoEmployee", addNewEmployee);
}

function addNewEmployee(type, response, event) {
	var employeeSelect = dojo.byId("employeeID");

	var xmlDoc = response.xhr.responseXML;

	if (xmlDoc == null) {
		alert("Update failed.");
	} else {
		alert("Update successful.");

		var employee = xmlDoc.getElementsByTagName("employee");

		var empID = getNodeAttribute(employee, "id");
		var empName = getNodeValue(employee, "fullName");

		if (dojo.byId('supervisor').checked) {
			loadReportSelects();
		}

		if (employeeSelect.value == 0) {
			var opt = baseOPTION.cloneNode(false);
			opt.value = empID;
			opt.text = empName + "(" + empID + ")";

			employeeSelect.appendChild(opt);
			employeeSelect.value = empID;
		} else {
			loadEmpSelect();
		}
	}

	displayEmp(type, response, event);
}

function remove() {
	pilQueueCommand({
		action: "remove",
		empID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoEmployee", backFromRemove);
}

function backFromRemove(type, response, event) {
	var answer = response.xhr.responseText;

	if (answer == "done") {
		dojo.byId('employeeID').value = "0";
		clearEntry();
		clearEvent();
		loadEmpSelect();
	} else {
		alert("Remove failed!");
	}
}

function terminate() {
	dijit.byId('termDate').set("readOnly", false);

	var today = new Date();
	dijit.byId('termDate').set("value", fixDate(today));
}

function enableCompTime() {
	if (dojo.byId('compTime').value > 0) {
		dijit.byId('compTimeExpiration').disabled = false;
	} else {
		dijit.byId('compTimeExpiration').disabled = true;
	}
}

function enableTimeAdjust() {
	if (dojo.byId('timeAdjust').value > 0) {
		dijit.byId('timeAdjustExpiration').disabled = false;
	} else {
		dijit.byId('timeAdjustExpiration').disabled = true;
	}
}

function checkEmail() {
	pilQueueCommand({
		action: "checkEmail",
		empEmail: dojo.byId('email').value,
		employeeID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoEmployee", verifyEmail);
}

function verifyEmail(type, response, event) {
	var servletReply = response.xhr.responseText;

	if (servletReply != "good") {
		alert("The email address entered is already in use by " + servletReply + ".");
		dojo.byId('email').value = "";
		dojo.byId('email').focus();
	}
}

function addEvent() {
	dojo.byId('addEventButton').disabled = true;
	// dojo.byId('deleteEventButton').disabled = true;
	dojo.byId('clearEventButton').disabled = true;

	pilQueueCommand({
		action: "addEvent",
		employeeID: dojo.byId('employeeID').value,
		eventID: dojo.byId('eventID').pilEventID,
		deptID: dojo.byId('xferDeptSelect').value,
		grantor: dojo.byId('eventGrantorSelect').value,
		date: dijit.byId('eventDate').get("displayedValue"),
		comment: dojo.byId('eventComment').value,
		eventType: dojo.byId('eventTypeSelect').value
	});

	pilPushQueue("./PtoEmployee", returnFromAddEvent);
}

function returnFromAddEvent(type, response, event) {
	var retVal = response.xhr.responseText;

	if (retVal == "fail") {
		alert("Failed adding event.");
	} else {
		alert("Event added successfully.");
		loadEmployee();
	}

	dojo.byId('addEventButton').disabled = false;
	dojo.byId('clearEventButton').disabled = false;
}

function loadEvent(event) {
	dojo.byId('eventID').innerHTML = "ID: " + event.target.pilID;
	dojo.byId('eventID').pilEventID = event.target.pilID;
	dojo.byId('eventTypeSelect').value = event.target.pilEventTypeID;
	dijit.byId('eventDate').set("value", event.target.pilEventDate);
	dojo.byId('xferDeptSelect').value = event.target.pilDeptID;
	dojo.byId('eventGrantorSelect').value = event.target.pilGrantorID;
	dojo.byId('eventComment').value = event.target.pilComment;

	if (isHR()) {
		dojo.byId('addEventButton').value = "Update";
		dojo.byId('deleteEventButton').disabled = false;
	}
}

function clearEvent() {
	pilKillChildren(dojo.byId('eventID'));
	dojo.byId('xferDeptSelect').value = "0";
	dojo.byId('eventGrantorSelect').value = "0";
	dojo.byId('eventTypeSelect').value = "0";
	dojo.byId('eventDate').value = "0";
	dojo.byId('eventComment').value = "";

	dojo.byId('addEventButton').value = "Add Event";
	dojo.byId('deleteEventButton').disabled = true;

}

function deleteEvent() {
	pilQueueCommand({
		action: "deleteEvent",
		eventID: dojo.byId('eventID').pilEventID
	});

	pilPushQueue("./PtoEmployee", returnFromDeleteEvent);
}

function returnFromDeleteEvent(type, response, event) {
	var servReply = response.xhr.responseText;

	if (servReply == "success") {
		alert("Event deleted.");

		loadEmployee();
	} else {
		alert("Delete failed!");
	}
}

function escalate() {
	escalateMenu();

	if ( !isHR()) {
		return false;
	}

	loadEmpSelect(true);

	dojo.byId('firstName').disabled = false;
	dojo.byId('midInitial').disabled = false;
	dojo.byId('lastName').disabled = false;
	dojo.byId('locSelect').disabled = false;
	dojo.byId('deptSelect').disabled = false;
	dojo.byId('empTitle').disabled = false;
	dojo.byId('email').disabled = false;
	dojo.byId('extension').disabled = false;
	dojo.byId('directReport').disabled = false;
	dojo.byId('ptoReport').disabled = false;
	dojo.byId('ptoBackup').disabled = false;
	dojo.byId('empTypeSelect').disabled = false;
	dojo.byId('hourly').disabled = false;
	dijit.byId('hireDate').set("readOnly", false);
	dijit.byId('startDate').set("readOnly", false);
	dojo.byId('terminateButton').disabled = false;
	dijit.byId('reviewDate').set("readOnly", false);
	dojo.byId('supervisor').disabled = false;
	dojo.byId('summerHours').disabled = false;
	dojo.byId('isReception').disabled = false;
	dojo.byId('rollover').disabled = false;
	dojo.byId('compTime').disabled = false;
	dojo.byId('compTimeExpiration').disabled = false;
	dojo.byId('timeAdjust').disabled = false;
	dojo.byId('timeAdjustExpiration').disabled = false;
	dojo.byId('bonusOneToThree').disabled = false;
	dijit.byId('startTime').set("readOnly", false);
	dijit.byId('endTime').set("readOnly", false);

	dojo.byId('eventTypeSelect').disabled = false;
	dojo.byId('eventDate').disabled = false;
	dojo.byId('xferDeptSelect').disabled = false;
	dojo.byId('eventGrantorSelect').disabled = false;
	dojo.byId('eventComment').readOnly = false;

	dojo.byId('addEventButton').disabled = false;
	dojo.byId('clearEventButton').disabled = false;

	dojo.byId('updateButton').disabled = false;
	//dojo.byId('deleteButton').disabled = false;

	$('.hrOnly').each(function() {
		popClass(this, "hide");
	});
}

function reviewToday() {
	dijit.byId('reviewDate').set("value", fixDate(new Date()));
}

function init() {
	ptoSetup();

	disableAllFields();

	// reload page when the employee select box value changes
	dojo.connect(dojo.byId('employeeID'), 'onchange', 'loadEmployee');

	// connect buttons to functions
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
//	dojo.connect(dojo.byId('deleteButton'), 'onclick', 'remove');
	dojo.connect(dojo.byId('terminateButton'), 'onclick', 'terminate');
	dojo.connect(dojo.byId('addEventButton'), 'onclick', 'addEvent');
	dojo.connect(dojo.byId('clearEventButton'), 'onclick', 'clearEvent');
	dojo.connect(dojo.byId('deleteEventButton'), 'onclick', 'deleteEvent');

	dojo.connect(dojo.byId('compTime'), 'onchange', 'enableCompTime');
	dojo.connect(dojo.byId('timeAdjust'), 'onchange', 'enableTimeAdjust');

	$('#reviewToday').click(reviewToday);

	$('#includeInactive').change(toggleInactiveDepartments);

	loadPage();
}

dojo.addOnLoad(init);
