/**
 * changePass.js
 */

function checkNewPass1() {
	var newPass = dojo.byId('newPass1').value;

	if (newPass.length < 6) {
		alert("Password must be at least 6 characters.");
		dojo.byId('newPass1').focus();
		return false;
	}
}

function checkNewPass2() {
	var newPass1 = dojo.byId('newPass1').value;
	var newPass2 = dojo.byId('newPass2').value;

	if (newPass2 != newPass1) {
		alert("Passwords must match.");

		dojo.byId('newPass2').focus();

		return false;
	}

	if (newPass2.length < 6) {
		alert("Password must be at least 6 characters.");

		dojo.byId('newPass1').focus();

		return false;
	}
}

function update() {
	dojo.byId('updateButton').disabled = true;
	dojo.byId('closeButton').disabled = true;

	pilQueueCommand({
		action: "changePassword",
		currPass: dojo.byId('currPass').value,
		newPass1: dojo.byId('newPass1').value,
		newPass2: dojo.byId('newPass2').value
	});

	pilPushQueue("./PtoEmployee", backFromChanging);
}

function backFromChanging(type, response, event) {
	var serverReply = response.xhr.responseText;

	if (serverReply == "good") {
		alert("Password successfully changed.");

		closeWindow();
	} else {
		alert(serverReply);

		dojo.byId('updateButton').disabled = false;
		dojo.byId('closeButton').disabled = false;
	}
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	dojo.connect(dojo.byId('newPass1'), 'onblur', 'checkNewPass1');
	dojo.connect(dojo.byId('newPass2'), 'onblur', 'checkNewPass2');

	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
}

dojo.addOnLoad(init);