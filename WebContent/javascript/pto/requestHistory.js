/**
 * requestHistory.js
 * 
 * Displays the passed employee's history of all requests, separated by
 * timeTypeCategory.
 * 
 * Associated JSP file MUST HAVE a div with id="requestHistory".
 */

function loadEmployeeHistory(empID) {
	if ($('#historyYearSelect')[0].length == 0) {
		createYearSelect();
	}

	pilQueueCommand({
		action: "getEmployeeRequestHistory",
		employeeID: empID,
		year: $('#historyYearSelect')[0].value
	});

	pilPushQueue("./PtoRequest", fillEmployeeHistory);
}

function createHistoryPanel(panelTitle, panelID, tableBodyID) {
	var panel = baseDIV.cloneNode(false);

	panel.id = panelID;
	panel.className = "panel";

	panel.appendChild(createH5(panelTitle));

	var table = baseTABLE.cloneNode(false);

	panel.appendChild(table);

	var thead = baseTHEAD.cloneNode(false);

	table.appendChild(thead);

	var headerRow = baseTR.cloneNode(false);

	thead.appendChild(headerRow);

	var th;

	// start date
	th = baseTH.cloneNode(false);
	th.width = "15%";
	th.appendChild(document.createTextNode("Start Date"));

	headerRow.appendChild(th);

	// end date
	th = baseTH.cloneNode(false);
	th.width = "15%";
	th.appendChild(document.createTextNode("End Date"));

	headerRow.appendChild(th);

	// time type
	th = baseTH.cloneNode(false);
	th.width = "30%";
	th.appendChild(document.createTextNode("Type of Time Away"));

	headerRow.appendChild(th);

	// status
	th = baseTH.cloneNode(false);
	th.width = "30%";
	th.appendChild(document.createTextNode("Status"));

	headerRow.appendChild(th);

	// days
	th = baseTH.cloneNode(false);
	th.width = "10%";
	th.appendChild(document.createTextNode("Days"));

	headerRow.appendChild(th);

	var tbody = baseTBODY.cloneNode(false);
	tbody.id = tableBodyID;

	table.appendChild(tbody);

	return panel;
}

function fillEmployeeHistory(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var fullDetails = ($(".fullBreakdown").length > 0);

	var requestHistoryDiv = dojo.byId('requestHistory');

	pilKillChildren(requestHistoryDiv);

	var requestList = xmlDoc.getElementsByTagName("requests");
	var summary = requestList[0].getElementsByTagName("summary");

	// var compTimeGranted = getNodeValue(summary, "compTimeGranted");
	// var compTimeTaken = getNodeValue(summary, "compTimeTaken");
	// var compTimeRemaining = getNodeValue(summary, "compTimeRemaining");

	// add regular PTO panel
	requestHistoryDiv.appendChild(createHistoryPanel("Paid Time Off", "ptoPanel", "ptoTable"));

	var rolloverDays = getNodeValue(summary, "rolloverDays");
	var ptoAdjustment = getNodeValue(summary, "ptoAdjustment");
	var ptoDays = getNodeValue(summary, "totalPTO");
	var daysRequested = getNodeValue(requestList, "daysRequested");
	var daysRemaining = getNodeValue(requestList, "daysRemaining");

	var ptoTotals = createParagraph("", (fullDetails ? 'Rollover Days: ' + rolloverDays : ''),
			(fullDetails ? 'PTO Adjustment: ' + ptoAdjustment : ''), 'Total PTO Available: ' + ptoDays,
			'Total PTO Taken: ' + daysRequested, 'PTO Days Remaining: ' + daysRemaining);

	dojo.byId('ptoPanel').appendChild(ptoTotals);

	// add 1 to 3 panel
	requestHistoryDiv.appendChild(createHistoryPanel("One to Three's", "oneToThreePanel", "oneToThreeTable"));

	var oneTo3Granted = getNodeValue(summary, "totalOneToThree");
	var oneTo3Taken = getNodeValue(requestList, "oneToThreeRequested");
	var oneTo3Remaining = getNodeValue(requestList, "oneToThreeRemaining");
	var bonusOneToThree = getNodeValue(requestList, "bonusOneToThree");

	var oneToThreeTotals = createParagraph( (fullDetails && (bonusOneToThree > 0) ? 'Bonus 1-3: ' + bonusOneToThree
			: ''), 'Total 1-3 Available: ' + oneTo3Granted, 'Total 1-3 Taken: ' + oneTo3Taken, '1-3 Remaining: '
			+ oneTo3Remaining);

	dojo.byId('oneToThreePanel').appendChild(oneToThreeTotals);

	// add non-chargeable panel
	requestHistoryDiv.appendChild(createHistoryPanel("Nonchargeable Days", "nonchargeablePanel", "nonchargeableTable"));

	var nonChargeableTaken = getNodeValue(requestList, "nonChargeableTaken");

	var ncTotals = createParagraph("Total Nonchargeable Days Taken: " + nonChargeableTaken);

	dojo.byId("nonchargeablePanel").appendChild(ncTotals);

	// add unpaid panel
	requestHistoryDiv.appendChild(createHistoryPanel("Unpaid Time Off", "unpaidPanel", "unpaidTable"));

	var unpaidTimeTaken = getNodeValue(requestList, "unpaidTimeTaken");

	dojo.byId("unpaidPanel").appendChild(createParagraph("Total Unpaid Taken: " + unpaidTimeTaken));

	// add unpaid 1 to 3 panel
	requestHistoryDiv.appendChild(createHistoryPanel("Unpaid One to Three's", "unpaid1to3Panel", "unpaid1to3Table"));

	var unpaidOneTo3Taken = getNodeValue(requestList, "unpaidOneTo3Taken");

	dojo.byId("unpaid1to3Panel").appendChild(createParagraph("Total Unpaid 1-3 Taken: " + unpaidOneTo3Taken));

	// process requests
	var unpaidPTOTable = dojo.byId('unpaidTable');
	var unpaidOneToThreeTable = dojo.byId('unpaid1to3Table');
	var nonChargeTable = dojo.byId('nonchargeableTable');
	var detailTable = dojo.byId('ptoTable');
	var oneTo3Table = dojo.byId('oneToThreeTable');

	var requests = xmlDoc.getElementsByTagName("request");

	var workCell;
	var workText;
	var workRow;

	for (var i = 0; i < requests.length; i++) {
		var requestType = requests[i].getElementsByTagName("requestType");
		var timeType = requestType[0].getElementsByTagName("timeType");
		var timeTypeCategory = timeType[0].getElementsByTagName("timeTypeCategory");

		var catID = getNodeAttribute(timeTypeCategory[0], "id");

		workRow = baseTR.cloneNode(false);

		var ampm;

		if (parseInt(getNodeValue(requests[i], "startTime").substring(0, 2)) < 12) {
			ampm = " AM";
		} else {
			ampm = " PM";
		}

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "startDate").substring(5, 10) + ampm);
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		if (parseInt(getNodeValue(requests[i], "endTime").substring(0, 2)) < 12) {
			ampm = " AM";
		} else {
			ampm = " PM";
		}

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "endDate").substring(5, 10) + ampm);
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requestType[0], "description"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);

		var workA = baseA.cloneNode(false);
		var requestStatus = getNodeValue(requests[i], "requestStatus");

		workA.href = "/pto/requests.jsp?requestID=" + getNodeAttribute(requests[i], "id");
		workA.innerHTML = requestStatus;
		workCell.appendChild(workA);

		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "daysTaken"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		switch (catID) {
			case '1':
				detailTable.appendChild(workRow);

				break;
			case '2':
				unpaidPTOTable.appendChild(workRow);

				break;
			case '3':
				oneTo3Table.appendChild(workRow);

				break;
			case '4':
				unpaidOneToThreeTable.appendChild(workRow);

				break;
			case '5':
				// compTimeTable.appendChild(workRow);
				nonChargeTable.appendChild(workRow);

				break;
			case '6':
				nonChargeTable.appendChild(workRow);

				break;
		}
	}
}

function createYearSelect() {
	var yearSelect = dojo.byId('historyYearSelect');

	var currentYear = new Date().getFullYear();

	for (var i = 2000; i <= currentYear; i++) {
		var option = baseOPTION.cloneNode(false);
		option.value = i;
		option.label = i;

		yearSelect.appendChild(option);
	}

	yearSelect.value = currentYear;

	dojo.connect(dojo.byId('yearHistorySelect'), 'onchange', 'yearSelectChanged');
}

function yearSelectChanged() {
	var empID = dojo.byId('employeeID').value;

	loadEmployeeHistory(empID);
}
