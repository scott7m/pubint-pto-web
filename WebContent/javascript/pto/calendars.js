// calendars.js

function loadCalSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoCalendar", fillCalSelect);
}

function fillCalSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var calendarSelect = dojo.byId("calSelect");
	var selectedCalendar = calendarSelect.value;

	if (selectedCalendar == "") {
		selectedCalendar = "0";
	}

	pilLoadSelectBox(calendarSelect, "calendar", "id", "calendarName", xmlDoc, false, "Add New");

	calendarSelect.value = selectedCalendar;

	var yearSelect = dojo.byId('yearSelect');

	var opt = baseOPTION.cloneNode(false);
	opt.value = "0";
	opt.text = "Add New";

	yearSelect.appendChild(opt);
}

function clearEntry() {
	dojo.byId('calName').value = "";
	dojo.byId('yearSelect').value = "0";

	clearSouthOfName();
	pilKillChildren(dojo.byId('detailDisplay'));
}

function clearSouthOfName() {
	dojo.byId('yearEntry').value = "";
	dojo.byId('holidayName').value = "";
	dojo.byId('holidayDate').value = "";
	dojo.byId('summerHours').checked = false;

	if (dojo.byId('yearSelect').value == 0) {
		$("#addDetail").hide();

		dojo.byId('yearEntry').disabled = false;
		document.getElementById("yearEntry").focus();
	} else {
		$("#addDetail").show();

		dojo.byId('yearEntry').disabled = true;
	}
}

function loadYears() {
	clearEntry();

	pilQueueCommand({
		action: "sendXML",
		calID: dojo.byId('calSelect').value
	});

	pilPushQueue("./PtoCalendar", fillYearSelect);
}

function fillYearSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var calendar = xmlDoc.getElementsByTagName("calendar");
	dojo.byId('calName').value = getNodeValue(calendar, "calendarName");

	var yearSelect = dojo.byId("yearSelect");
	pilKillChildren(yearSelect);
	var selectedYear = yearSelect.value;

	if (selectedYear == "") {
		selectedYear = "0";
	}

	pilLoadSelectBox(yearSelect, "year", "id", "calendarYear", xmlDoc, false, "Add New");

	yearSelect.value = selectedYear;
}

function loadCalendar() {
	clearSouthOfName();

	pilQueueCommand({
		action: "sendDetails",
		yearID: dojo.byId('yearSelect').value
	});

	pilPushQueue("./PtoCalendar", displayCalendarDetails);
}

function displayCalendarDetails(type, response, event) {
	var detailSection = dojo.byId('detailDisplay');

	pilKillChildren(detailSection);

	var xmlDoc = response.xhr.responseXML;

	var details = xmlDoc.getElementsByTagName("details");

	var detailTable = baseTABLE.cloneNode(false);
	var detailHead = baseTHEAD.cloneNode(false);

	detailHead.border = "1";
	detailTable.appendChild(detailHead);

	workCell = baseTH.cloneNode(false);
	workCell.width = "45%";
	workData = document.createTextNode('Holiday Name');
	workCell.appendChild(workData);
	detailHead.appendChild(workCell);

	workCell = baseTH.cloneNode(false);
	workCell.width = "25%";
	workData = document.createTextNode('Holiday Date');
	workCell.appendChild(workData);
	detailHead.appendChild(workCell);

	workCell = baseTH.cloneNode(false);
	workCell.width = "15%";
	workData = document.createTextNode('Summer Hours');
	workCell.appendChild(workData);
	detailHead.appendChild(workCell);

	workCell = baseTH.cloneNode(false);
	workCell.width = "15%";
	workData = document.createTextNode('');
	workCell.appendChild(workData);
	detailHead.appendChild(workCell);

	var detailBody = baseTBODY.cloneNode(false);

	detailTable.appendChild(detailBody);

	for (var i = 0; i < details.length; i++) {
		var holID = getNodeAttribute(details[i], "id");
		var holName = getNodeValue(details[i], "holidayName");
		var holDate = getNodeValue(details[i], "holidayDate");

		workRow = baseTR.cloneNode(false);

		workCell = baseTD.cloneNode(false);
		workData = document.createTextNode(holName);
		workCell.appendChild(workData);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workData = document.createTextNode(holDate);
		workCell.appendChild(workData);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);

		var workBox = baseINPUT.cloneNode(false);

		workBox.type = "checkbox";
		workBox.checked = (getNodeValue(details[i], "summerHours") == "true");
		workBox.disabled = true;
		workCell.appendChild(workBox);
		workRow.appendChild(workCell);

		// delete button with id attached
		workButton = baseINPUT.cloneNode(false);
		workButton.type = 'button';
		workButton.id = "delete" + holID;
		workButton.pilHolID = holID;
		workButton.value = "Delete";
		workButton.onclick = removeHoliday;

		workCell = baseTD.cloneNode(false);
		workCell.appendChild(workButton);
		workRow.appendChild(workCell);

		detailBody.appendChild(workRow);
	}

	addClass(detailTable, "pretty");

	detailSection.appendChild(detailTable);
}

function update() {
	pilQueueCommand({
		action: "update",
		calID: dojo.byId('calSelect').value,
		calName: dojo.byId('calName').value,
		yearEntry: dojo.byId('yearEntry').value
	});

	pilPushQueue("./PtoCalendar", addNewCalendar);
}

function addNewCalendar(type, response, event) {
	var calendarSelect = dojo.byId("calSelect");

	var xmlDoc = response.xhr.responseXML;

	var calendar = xmlDoc.getElementsByTagName("calendar");

	var calID = getNodeAttribute(calendar, "id");
	var calName = getNodeValue(calendar, "calendarName");
	var opt;

	if (calendarSelect.value == 0) {
		opt = baseOPTION.cloneNode(false);
		opt.value = calID;
		opt.text = calName;

		calendarSelect.appendChild(opt);
		calendarSelect.value = calID;
	} else {
		loadCalSelect();
	}

	var yearSelect = dojo.byId('yearSelect');
	var yearsXML = xmlDoc.getElementsByTagName("years");

	if (yearSelect.value == 0) {
		var year = getChildrenByTagName(yearsXML[0], "year");
		var yearID = getNodeAttribute(year[0], "id");
		var yearInt = getNodeValue(year[0], "calendarYear");

		opt = baseOPTION.cloneNode(false);
		opt.value = yearID;
		opt.text = yearInt;

		yearSelect.appendChild(opt);
		yearSelect.value = yearID;
	} else {
		loadYears();
	}

	loadCalendar();
}

function addHoliday() {
	var dateString = dojo.byId('holidayDate').value;
	var hDateOffset = new Date(dateString);
	var hDate = new Date(hDateOffset.getTime() + hDateOffset.getTimezoneOffset()*60000);
	
	if (hDate.getDay() == 6 || hDate.getDay() == 0) {
		alert("Holiday cannot fall on a weekend.");
		return false;
	}

	pilQueueCommand({
		action: "addHoliday",
		calID: dojo.byId('calSelect').value,
		year: dojo.byId('yearSelect').value,
		name: dojo.byId('holidayName').value,
		date: dateString,
		summerHours: dojo.byId('summerHours').checked
	});

	pilPushQueue("./PtoCalendar", backFromAdd);
}

function backFromAdd(type, response, event) {
	var retVal = response.xhr.responseText;

	if (retVal == "fail") {
		alert("Failed to add holiday.");
	} else {
		alert("Add successful!");

		loadCalendar();
	}
}

function removeHoliday(event) {
	var holID = event.target.pilHolID;

	pilQueueCommand({
		action: "removeHoliday",
		holID: holID,
		year: dojo.byId('yearEntry').value
	});

	pilPushQueue("./PtoCalendar", loadCalendar);
}

function checkIt(evt) {
	evt = (evt) ? evt : window.event;

	var charCode = (evt.which) ? evt.which : evt.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		status = "This field accepts numbers only.";

		return false;
	}

	status = "";

	return true;
}

function pauseInit() {
	window.setTimeout(init, 2000);
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	// reload page when the calendar select box value changes
	dojo.connect(dojo.byId('calSelect'), 'onchange', 'loadYears');
	dojo.connect(dojo.byId('yearSelect'), 'onchange', 'loadCalendar');

	// connect buttons to functions
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');

	dojo.connect(dojo.byId('addHolidayButton'), 'onclick', 'addHoliday');

	loadCalSelect();
}

dojo.addOnLoad(init);