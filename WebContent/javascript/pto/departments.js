// departments.js

function loadPage() {
	clearEntry();

	loadDeptSelect();
	loadEmpSelect();
}

function clearEntry() {
	$('#includeInactive').prop('checked', false);
	$('#deptName').val('');
	$('#deptShortName').val('');
	$('#deptHeadSelect').val('0');
	$('#sendReport').prop('checked', false);
}

function loadEmpSelect() {
	pilQueueCommand({
		action: "supervisorSelectList"
	});

	pilPushQueue("./PtoEmployee", fillEmpSelect);
}

function fillEmpSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var deptHeadSelect = dojo.byId("deptHeadSelect");
	var selectedHead = deptHeadSelect.value;

	if (selectedHead == "") {
		selectedHead = "0";
	}

	pilLoadSelectBox(deptHeadSelect, "employee", "id", "fullName", xmlDoc, true, "None Selected");

	deptHeadSelect.value = selectedHead;
}

function loadDepartment() {
	pilQueueCommand({
		action: "sendXML",
		deptID: dojo.byId('deptSelect').value
	});

	pilPushQueue("./PtoDepartment", displayDept);
}

function displayDept(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();

	var department = xmlDoc.getElementsByTagName("department");

	dojo.byId('deptName').value = getNodeValue(department, "departmentName");
	dojo.byId('deptShortName').value = getNodeValue(department, "shortName");
	dojo.byId('deptHeadSelect').value = getNodeValue(department, "deptHeadID");
	dojo.byId('sendReport').checked = (getNodeValue(department, "dailyReport") == "true");
}

function update() {
	pilQueueCommand({
		action: "update",
		deptID: dojo.byId('deptSelect').value,
		deptName: dojo.byId('deptName').value,
		shortName: dojo.byId('deptShortName').value,
		deptHeadID: dojo.byId('deptHeadSelect').value,
		sendReport: dojo.byId('sendReport').checked
	});

	pilPushQueue("./PtoDepartment", addNewDepartment);
}

function addNewDepartment(type, response, event) {
	var departmentSelect = dojo.byId("deptSelect");

	if (departmentSelect.value == 0) {
		var xmlDoc = response.xhr.responseXML;

		var department = xmlDoc.getElementsByTagName("department");

		var deptID = getNodeAttribute(department, "id");
		var deptName = getNodeValue(department, "departmentName");

		var opt = baseOPTION.cloneNode(false);
		opt.value = deptID;
		opt.text = deptName + "(" + deptID + ")";

		departmentSelect.appendChild(opt);
		departmentSelect.value = deptID;
	} else {
		loadDeptSelect();
	}

	displayDepartment(type, response, event);
}

function remove() {
	pilQueueCommand({
		action: "remove",
		deptID: dojo.byId('deptSelect').value
	});

	pilPushQueue("./PtoDepartment", backFromRemove);
}

function backFromRemove(type, response, event) {
	var answer = response.xhr.responseText;

	if (answer == "done") {
		dojo.byId('deptSelect').value = "0";
		clearEntry();
		loadDeptSelect();
	} else {
		alert("Remove failed!");
	}
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	// reload page when the dept delect box value changes
	dojo.connect(dojo.byId('deptSelect'), 'onchange', 'loadDepartment');

	// override default form submission
	// dojo.connect(dojo.byId('deptForm'), 'onsubmit', 'returnFalse');

	// connect buttons to functions
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
	dojo.connect(dojo.byId('deleteButton'), 'onclick', 'remove');

	$('#includeInactive').change(toggleInactiveDepartments);

	loadPage();
}

dojo.addOnLoad(init);