// locations.js
function loadPage() {
	clearEntry();
	loadLocSelect();
	loadCalSelect();
}

function clearEntry() {
	dojo.byId('locName').value = "";
	dojo.byId('locAdd1').value = "";
	dojo.byId('locAdd2').value = "";
	dojo.byId('locAdd3').value = "";
	dojo.byId('locCountry').value = "";
	dojo.byId('locCity').value = "";
	dojo.byId('locState').value = "";
	dojo.byId('locZip').value = "";
	dojo.byId('locPhone').value = "";
	dojo.byId('calSelect').value = "0";
}

function loadLocSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoLocation", fillLocSelect);
}

function fillLocSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var locSelect = dojo.byId("locSelect");
	var selectedLoc = locSelect.value;

	if (selectedLoc == "") {
		selectedLoc = "0";
	}

	pilLoadSelectBox(locSelect, "location", "id", "name", xmlDoc, false, "Add New");

	locSelect.value = selectedLoc;
}

function loadCalSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoCalendar", fillCalSelect);
}

function fillCalSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var calendarSelect = dojo.byId("calSelect");
	var selectedCalendar = calendarSelect.value;

	if (selectedCalendar == "") {
		selectedCalendar = "0";
	}

	pilLoadSelectBox(calendarSelect, "calendar", "id", "calendarName", xmlDoc, false, "None Selected");

	calendarSelect.value = selectedCalendar;
}

function loadLocation() {
	pilQueueCommand({
		action: "sendXML",
		locID: dojo.byId('locSelect').value
	});

	pilPushQueue("./PtoLocation", displayLocation);
}

function displayLocation(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();

	var location = xmlDoc.getElementsByTagName("location");
	var calendar = xmlDoc.getElementsByTagName("calendar");

	dojo.byId('locName').value = getNodeValue(location, "name");
	dojo.byId('locAdd1').value = getNodeValue(location, "address1");
	dojo.byId('locAdd2').value = getNodeValue(location, "address2");
	dojo.byId('locAdd3').value = getNodeValue(location, "address3");
	dojo.byId('locCountry').value = getNodeValue(location, "country");
	dojo.byId('locCity').value = getNodeValue(location, "city");
	dojo.byId('locState').value = getNodeValue(location, "state");
	dojo.byId('locZip').value = getNodeValue(location, "postalCode");
	dojo.byId('locPhone').value = getNodeValue(location, "phone");

	dojo.byId('calSelect').value = getNodeAttribute(calendar, "id");
}

function update() {
	pilQueueCommand({
		action: "update",
		locID: dojo.byId('locSelect').value,
		name: dojo.byId('locName').value,
		address1: dojo.byId('locAdd1').value,
		address2: dojo.byId('locAdd2').value,
		address3: dojo.byId('locAdd3').value,
		city: dojo.byId('locCity').value,
		state: dojo.byId('locState').value,
		zip: dojo.byId('locZip').value,
		country: dojo.byId('locCountry').value,
		phone: dojo.byId('locPhone').value,
		calID: dojo.byId('calSelect').value
	});

	pilPushQueue("./PtoLocation", addNewLocation);
}

function addNewLocation(type, response, event) {
	var locationSelect = dojo.byId("locSelect");

	var xmlDoc = response.xhr.responseXML;

	var location = xmlDoc.getElementsByTagName("location");

	var locID = getNodeAttribute(location, "id");
	var locName = getNodeValue(location, "name");

	if (locationSelect.value == 0) {
		var opt = baseOPTION.cloneNode(false);
		opt.value = locID;
		opt.text = locName + "(" + locID + ")";

		locationSelect.appendChild(opt);
		locationSelect.value = locID;
	} else {
		loadLocSelect();
	}

	displayLocation(type, response, event);
}

function remove() {
	pilQueueCommand({
		action: "remove",
		locID: dojo.byId('locSelect').value
	});

	pilPushQueue("./PtoLocation", backFromRemove);
}

function backFromRemove(type, response, event) {
	var answer = response.xhr.responseText;

	if (answer == "done") {
		dojo.byId('locSelect').value = "0";
		clearEntry();
		loadLocSelect();
	} else {
		alert("Remove failed!");
	}
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	// reload page when the dept delect box value changes
	dojo.connect(dojo.byId('locSelect'), 'onchange', 'loadLocation');

	// override default form submission
	dojo.connect(dojo.byId('empForm'), 'onsubmit', 'returnFalse');
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
	dojo.connect(dojo.byId('deleteButton'), 'onclick', 'remove');

	// override default form submission
	// dojo.connect(dojo.byId('locForm'), 'onsubmit', 'returnFalse');

	loadPage();
}

dojo.addOnLoad(init);