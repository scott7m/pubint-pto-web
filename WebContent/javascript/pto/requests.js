// requests.js

var ptoNotRequested = 0;
var ptoPendingStatus = 1;
var ptoApprovedStatus = 2;

function isNotRequested(status) {
	return status === ptoNotRequested;
}

function isPending(status) {
	return status == ptoPendingStatus;
}

function isApproved(status) {
	return status == ptoApprovedStatus;
}

function setupScreen() {
	clearEntry();

	loadRequestTypeSelect();
}

function loadRequestTypeSelect() {
	pilQueueCommand({
		action: "selectList",
		requestID: dojo.byId('requestID').value
	});

	pilPushQueue("./PtoRequestType", fillRequestTypeSelect);
}

function fillRequestTypeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var requestSelect = dojo.byId("requestSelect");

	pilLoadSelectBox(requestSelect, "requestType", "id", "description", xmlDoc, false, '');

	loadEmpSelect();
}

function loadEmpSelect(asReceptionist) {
	pilQueueCommand({
		action: "selectList",
		asReceptionist: (asReceptionist == true ? true : false)
	});

	pilPushQueue("./PtoEmployee", fillEmpSelect);
}

function fillEmpSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var empSelect = dojo.byId("employeeID");

	pilLoadSelectBox(empSelect, "employee", "id", "fullName", xmlDoc, false);

	finishSetupScreen();
}

function finishSetupScreen() {
	if (dojo.byId('requestID').value != '') {
		if (isHR()) {
			dojo.byId('sendButton').value = "Update Request";
		} else {
			$("#sendButton").hide();
		}

		loadSpecificRequest();
	} else {
		loadUserEmployee();
	}
}

function loadSpecificRequest() {
	pilQueueCommand({
		action: "sendXML",
		requestID: dojo.byId('requestID').value
	});

	pilPushQueue("./PtoRequest", fillSpecificRequest);
}

function fillSpecificRequest(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var request = xmlDoc.getElementsByTagName("request");
	dojo.byId('requestID').value = getNodeAttribute(request, "id");

	var employee = xmlDoc.getElementsByTagName("employee");
	var employeeID = getNodeAttribute(employee, "id") * 1;

	dojo.byId('employeeID').value = employeeID;

	var startDate = getNodeValue(request, 'startDate');
	var endDate = getNodeValue(request, 'endDate');

	dijit.byId('startDate').set('value', startDate);
	dijit.byId('endDate').set('value', endDate);

	resetTimeBoxes();

	var startTime = getNodeValue(request, "startTime");
	var endTime = getNodeValue(request, "endTime");

	var parameters = {
		value: "T" + startTime
	};

	createTime(dojo.byId('startTime'), parameters, null);

	parameters = {
		value: "T" + endTime
	};

	createTime(dojo.byId('endTime'), parameters, null);

	var requestType = xmlDoc.getElementsByTagName("requestType");
	dojo.byId('requestSelect').value = getNodeAttribute(requestType, "id");

	var daysTaken = getNodeValue(request, "daysTaken");

	if (daysTaken == null || daysTaken == '') {
		calculateNumDays();
	} else {
		dojo.byId('numDays').value = daysTaken;
		window.pilBlockCalculate = true;
	}

	dojo.byId('requestComment').value = getNodeValue(request, "comment");

	pilKillChildren(dojo.byId('requestStatus'));

	var requestStatus = getNodeValue(request, "requestStatus");

	dojo.byId('requestStatus').appendChild(document.createTextNode(requestStatus));
	dojo.byId('requestStatus').pilStatus = requestStatus;

	if (requestStatus != "Not Requested") {
		dojo.byId('employeeID').disabled = true;
	}

	var accessLvl = getNodeValue(request, "accessLevel");
	var statusID = getNodeAttribute(request, "statusID");

	specifyAccess(accessLvl, statusID);

	loadEmployeeHistory(employeeID);
	setTypePrompt();

	pilKillChildren(dojo.byId('requestStatusDetail'));

	var actionBy = getChildrenByTagName(request[0], "actionBy");
	var cancelBy = getChildrenByTagName(request[0], "canceledBy");

	var statusDetail = createParagraph("Action taken: " + getNodeValue(request, "action"), "Action by: "
			+ getNodeValue(actionBy, "fullName"), "Action date: " + getNodeValue(request, "actionDate"),
			"Canceled by: " + getNodeValue(cancelBy, "fullName"), "Canceled date: "
					+ getNodeValue(request, "canceledDate"));

	dojo.byId('requestStatusDetail').appendChild(statusDetail);
	dojo.byId('statusID').value = statusID;

	$('#printButton').show();
}

function specifyAccess(accessLevel, statusID) {
	var today = new Date();
	today.setHours(0, 0, 0, 0);

	var startDate = dijit.byId('startDate').get('value');
	var startTime = dijit.byId('startTime').get('value');

	var checkDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0, 0);

	if ( (accessLevel == "false") || ( (accessLevel != "HR") && ( ( !isPending(statusID)) || (checkDate < today)))) {
		// disable all fields - read only!
		$("#sendButton").hide();

		dijit.byId('startDate').set("readOnly", true);
		dijit.byId('startTime').set("readOnly", true);
		dojo.byId('requestSelect').disabled = true;
		dijit.byId('endDate').set("readOnly", true);
		dijit.byId('endTime').set("readOnly", true);
		dojo.byId('requestComment').readOnly = true;
	}

	if (accessLevel == "HR") {
		// unlock HR only access
		dojo.byId('numDays').disabled = false;
	}

	// show cancel button for pending and approved
	// (if adequate privileges...)
	if (isPending(statusID) || isApproved(statusID)) {
		if ( (accessLevel == 'HR') || (checkDate > today)) {
			// H/R anytime...
			// anyone (self or super) in the future...
			$('#cancelButton').show();
		} else if ( (accessLevel === 'super') && (checkDate.getTime() === today.getTime())) {
			// super can do today too...
			$('#cancelButton').show();
		}
	}

	if (isPending(statusID)) {
		if ( (accessLevel == 'super') || (accessLevel == 'HR')) {
			$('#approveButton').show();

			if (checkDate > today) {
				$('#rejectButton').show();
			}
		}
	}
}

function setTypePrompt() {
	if (dojo.byId('requestSelect').value == "0") {
		dojo.byId('reqInstructions').innerHTML = "<b>Select a request type.</b>";
	} else {
		dojo.byId('reqInstructions').innerHTML = '';
	}
}

function loadUserEmployee() {
	pilQueueCommand({
		action: "sendXML",
		employeeID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoEmployee", fillUserEmployee);
}

function resetTimeBoxes() {
	var startTimeContainer = dojo.byId('startTimeContainer');
	var endTimeContainer = dojo.byId('endTimeContainer');

	dijit.registry.remove('startTime');
	dijit.registry.remove('endTime');

	pilKillChildren(startTimeContainer);
	pilKillChildren(endTimeContainer);

	var startTime = baseINPUT.cloneNode(false);

	startTime.id = 'startTime';
	startTime.name = 'startTime';

	var endTime = baseINPUT.cloneNode(false);

	endTime.id = 'endTime';
	endTime.name = 'endTime';

	var startLabel = baseLABEL.cloneNode(false);
	startLabel.appendChild(document.createTextNode("Starting Time"));
	startLabel.htmlFor = "startTime";

	var endLabel = baseLABEL.cloneNode(false);
	endLabel.appendChild(document.createTextNode("Ending Time"));
	endLabel.htmlFor = "endTime";

	startTimeContainer.appendChild(startLabel);
	endTimeContainer.appendChild(endLabel);

	startTimeContainer.appendChild(startTime);
	endTimeContainer.appendChild(endTime);
}

function fillUserEmployee(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	// Make hidden fields for pertinent employee info for easy access later? Yes
	// I think so.
	var employee = xmlDoc.getElementsByTagName("employee");

	var empID = getNodeAttribute(employee, "id");

	dojo.byId('employeeID').value = empID;

	var startTime = getNodeValue(employee, "startTime");
	var endTime = getNodeValue(employee, "endTime");

	var startSplit = startTime.split(":", 2);
	var endSplit = endTime.split(":", 2);

	var employee_start = new Date(1970, 1, 1, startSplit[0], startSplit[1], 0);
	var employee_end = new Date(1970, 1, 1, endSplit[0], endSplit[1], 0);

	var timeConstraints = {
		min: employee_start,
		max: employee_end
	};

	var parameters = {
		value: "T" + startTime
	};

	resetTimeBoxes();

	createTime(dojo.byId('startTime'), parameters, timeConstraints);
	createTime(dojo.byId('endTime'), parameters, timeConstraints);

	dojo.connect(dijit.byId('startTime'), 'onChange', 'startTimeChanged');
	dojo.connect(dijit.byId('endTime'), 'onChange', 'endTimeChanged');

	// Set the dates and times to read-only until the user sets a request type.
	dijit.byId('startDate').set("readOnly", true);
	dijit.byId('endDate').set("readOnly", true);
	dijit.byId('startTime').set("readOnly", true);
	dijit.byId('endTime').set("readOnly", true);

	dojo.byId('requestComment').readOnly = true;

	loadEmployeeHistory(empID);

	fillDefaultStartDate();
}

function createTime(item, parameters, constraints) {
	var fullParameterList = {
		lang: 'en-us',
		required: 'true'
	};

	if (parameters != null) {
		fullParameterList = dojo.mixin(fullParameterList, parameters);
	}

	if (constraints != null) {
		fullParameterList.constraints = dojo.mixin({
			formatLength: 'short',
			timePattern: 'h:mm a',
			clickableIncrement: 'T00:15:00',
			visibleIncrement: 'T00:30:00',
			visibleRange: 'T08:00:00'
		}, constraints);
	} else {
		fullParameterList.constraints = {
			formatLength: 'short',
			timePattern: 'h:mm a',
			clickableIncrement: 'T00:15:00',
			visibleIncrement: 'T00:30:00',
			visibleRange: 'T08:00:00'
		};
	}

	var makeTime = null;

	try {
		makeTime = new dijit.form.TimeTextBox(fullParameterList, item);

		if (item.disabled === true) {
			makeTime.disabled = true;
		}
	} catch (Err) {
		console.log("Error creating time field " + Err);
	}

	return makeTime;
}

function fillDefaultStartDate() {
	var today = new Date();
	var dow = today.getDay();

	if (dow == 6) {
		today.setDate(today.getDate() + 2);
	} else if (dow == 0) {
		today.setDate(today.getDate() + 1);
	}

	dijit.byId('startDate').set('value', fixDate(today));

	validateStartDate();
}

function clearEntry() {
	dojo.byId('requestSelect').value = "0";
	dojo.byId('startDate').value = '';
	dojo.byId('endDate').value = '';
	dojo.byId('requestComment').value = '';
	dojo.byId('numDays').value = '';

	pilKillChildren(dojo.byId('requestStatus'));
	pilKillChildren(dojo.byId('requestStatusDetail'));

	dojo.byId('requestStatus').appendChild(document.createTextNode("Not Requested"));
}

function appendMessage(message, nextLine) {
	if (message !== '') {
		message += '\n';
	}

	message += nextLine;

	return message;
}

function isValidRequest() {
	var rSel = dojo.byId('requestSelect');

	var message = '';

	if (rSel.value == "0") {
		message = appendMessage(message, 'You must select a request type!');
	} else if (dojo.byId('startDate').value == '') {
		message = appendMessage(message, 'You must enter a beginning date!');
	} else if (dojo.byId('startTime').value == '') {
		message = appendMessage(message, 'You must enter a start time!');
	} else if (dojo.byId('endDate').value == '') {
		message = appendMessage(message, 'You must enter an ending date!');
	} else if (dojo.byId('endTime').value == '') {
		message = appendMessage(message, 'You must enter an ending time!');
	} else if ( (rSel[rSel.selectedIndex].pilRequireComment == "true") && (dojo.byId('requestComment').value == '')) {
		message = appendMessage(message, 'You must enter a comment for this request type!');
	}

	if (message != '') {
		alert(message);

		return false;
	}

	return true;
}

function sendRequest() {
	if ( !isValidRequest()) {
		return;
	}

	disableButtons();

	pilQueueCommand({
		action: "sendRequest",
		requestID: dojo.byId('requestID').value,
		requestTypeID: dojo.byId('requestSelect').value,
		beginDate: dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
			selector: 'date'
		}),
		beginTime: dojo.byId('startTime').value,
		endDate: dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
			selector: 'date'
		}),
		endTime: dojo.byId('endTime').value,
		requestComment: dojo.byId('requestComment').value,
		employeeID: dojo.byId('employeeID').value,
		daysTaken: dojo.byId('numDays').value,
		comment: dojo.byId('requestComment').value
	});

	pilPushQueue("./PtoRequest", reportResults);
}

function reportResults(type, response, event) {
	var servletReply = response.xhr.responseText;

	if (servletReply == "done") {
		alert("Request sent.");

		clearEntry();
	} else if (servletReply == "fail") {
		alert("Request failed.");
	} else {
		alert(servletReply);
	}

	enableButtons();
}

function disableButtons() {
	dojo.byId('closeButton').disabled = true;
	dojo.byId('sendButton').disabled = true;
}

function enableButtons() {
	dojo.byId('closeButton').disabled = false;
	dojo.byId('sendButton').disabled = false;
}

function startDateChanged() {
	// jdm: if still initializing...don't perform this function
	if (window.pilInitialLoad) {
		if (window.pilCalculating) {
			window.pilCalculating = false;

			return;
		} else {
			window.pilCalculating = true;
		}
	}
	
	if (dojo.byId('requestStatus').textContent == "Not Requested") {
		validateStartDate();
	}
	
	calculateNumDays();
	calculate("startDate");
}

function endDateChanged() {
	// jdm: if still initializing...don't perform this function
	if (window.pilInitialLoad) {
		if (window.pilCalculating) {
			window.pilCalculating = false;

			return;
		} else {
			window.pilCalculating = true;
		}
	}
	
	if (dojo.byId('requestStatus').textContent == "Not Requested") {
		validateEndDate();
	}
	
	calculateNumDays();
	calculate('endDate');
}

function startTimeChanged() {
	calculateNumDays();
}

function endTimeChanged() {
	calculateNumDays();
}

function requestTypeChanged() {
	if (dojo.byId('requestSelect').value == 0) {
		dijit.byId('startDate').set("readOnly", true);
		dijit.byId('endDate').set("readOnly", true);
		dijit.byId('startTime').set("readOnly", true);
		dijit.byId('endTime').set("readOnly", true);

		dojo.byId('requestComment').readOnly = true;

		// jdm: block date validations
		window.pilInitialLoad = true;
	} else {
		dijit.byId('startDate').set("readOnly", false);
		dijit.byId('endDate').set("readOnly", false);
		dijit.byId('startTime').set("readOnly", false);
		dijit.byId('endTime').set("readOnly", false);

		dojo.byId('requestComment').readOnly = false;

		// jdm: block date validations
		window.pilInitialLoad = false;

	}

	calculateEndDate();

	calculate("requestType");

	setTypePrompt();
}

function calculate(eventType) {
	var request = dojo.byId('requestSelect');

	var startDate;
	var endDate;

	try {
		startDate = dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
			selector: 'date'
		});
	} catch (error) {

	}

	try {
		endDate = dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
			selector: 'date'
		});
	} catch (error) {

	}

	if ( (startDate === undefined) || (endDate == undefined)) {
		return;
	}

	pilQueueCommand({
		action: "calculateDates",
		requestType: request.value,
		startDate: startDate,
		endDate: endDate,
		startTime: dojo.byId('startTime').value,
		endTime: dojo.byId('endTime').value,
		employeeID: dojo.byId('employeeID').value,
		eventType: eventType
	});

	pilPushQueue("./PtoRequest", fillDates);
}

function fillDates(type, response, event) {
	// jdm: only perform during the initial page load
	if ( !window.pilInitialLoad) {
		return;
	}

	var xmlDoc = response.xhr.responseXML;

	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg == '') {
		dijit.byId('startDate').set('value', getNodeValue(dateReply, "startDate"));
		dijit.byId('endDate').set('value', getNodeValue(dateReply, 'endDate'));
		dijit.byId('endTime').set('value', "T" + getNodeValue(dateReply, "endTime"));
		
		if (! window.pilBlockCalculate) {
			dojo.byId('numDays').value = getNodeValue(dateReply, "numDays");
		}
	} else {
		alert(errorMsg);
	}
}

function calculateEndDate() {
	var startDate = dijit.byId('startDate').get('value');
	var endDate = dijit.byId('endDate').get('value');

	var startTime = dijit.byId('startTime').get('value');
	var endTime = dijit.byId('endTime').get('value');

	startDate.addTime(startTime);
	endDate.addTime(endTime);

	var request = dojo.byId('requestSelect');

	if ( (endDate == '') || (endDate <= startDate)) {
		pilQueueCommand({
			action: "calculateEndDate",
			requestType: request.value,
			startDate: dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
				selector: 'date'
			}),
			employeeID: dojo.byId('employeeID').value
		});

		pilPushQueue("./PtoRequest", fillEndDate);
	}
}

function fillEndDate(type, response, event) {
	var xmlDoc = response.xhr.responseXML;
	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg == '') {
		dijit.byId('endDate').set('value', getNodeValue(dateReply, 'endDate'));

		calculateNumDays();
	} else {
		alert(errorMsg);
	}
}

function calculateStartDate() {
	var startDate = dijit.byId('startDate').get('value');
	var endDate = dijit.byId('endDate').get('value');

	if (endDate <= startDate) {
		pilQueueCommand({
			action: "calculateStartDate",
			endDate: dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
				selector: 'date'
			}),
			employeeID: dojo.byId('employeeID').value
		});

		pilPushQueue("./PtoRequest", fillStartDate);
	}
}

function fillStartDate(type, response, event) {
	var xmlDoc = response.xhr.responseXML;
	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg == '') {
		dijit.byId('startDate').set('value', getNodeValue(dateReply, "startDate"));
	} else {
		alert(errorMsg);
	}
}

function calculateEndTime() {
	var startTime = dojo.byId('startTime').value;
	var endTime = dojo.byId('endTime').value;

	var request = dojo.byId('requestSelect');

	if ( (endTime == '') || (endTime <= startTime)) {
		pilQueueCommand({
			action: "calculateEndTime",
			requestType: request.value,
			startTime: dojo.byId('startTime').value,
			employeeID: dojo.byId('employeeID').value
		});

		pilPushQueue("./PtoRequest", fillEndTime);
	}
}

function fillEndTime(type, response, event) {
	var xmlDoc = response.xhr.responseXML;
	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg == '') {
		dijit.byId('endTime').set('value', "T" + getNodeValue(dateReply, "endTime"));
	} else {
		alert(errorMsg);
	}

}

function calculateNumDays() {
	if (window.pilBlockCalculate) {
		return false;
	}
	
	var startDate = dijit.byId('startDate').get('value');
	var endDate = dijit.byId('endDate').get('value');

	var request = dojo.byId('requestSelect');

	if (endDate >= startDate) {
		pilQueueCommand({
			action: "calculateNumDays",
			startDate: dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
				selector: 'date'
			}),
			endDate: dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
				selector: 'date'
			}),
			startTime: dojo.byId('startTime').value,
			endTime: dojo.byId('endTime').value,
			employeeID: dojo.byId('employeeID').value,
			requestType: request.value
		});

		pilPushQueue("./PtoRequest", fillNumDays);
	} else {
		// alert("The end date must be after the start date!");
	}
}

function fillNumDays(type, response, event) {
	dojo.byId('numDays').value = response.xhr.responseText;
}

function validateStartDate() {
	pilQueueCommand({
		action: "validateDate",
		theDate: dojo.date.stamp.toISOString(dijit.byId('startDate').getValue(), {
			selector: 'date'
		}),
		empID: dojo.byId('employeeID').value
	});

	if (isPending)
	pilPushQueue("./PtoRequest", startDateValidation);
}

function startDateValidation(type, response, event) {
	var xmlDoc = response.xhr.responseXML;
	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg != '') {
		alert("Starting" + errorMsg);

		dijit.byId('startDate').set('value', getNodeValue(dateReply, "validDate"));
	} else {
		calculateEndDate();
	}
}

function validateEndDate() {
	pilQueueCommand({
		action: "validateDate",
		theDate: dojo.date.stamp.toISOString(dijit.byId('endDate').getValue(), {
			selector: 'date'
		}),
		empID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoRequest", endDateValidation);
}

function endDateValidation(type, response, event) {
	var xmlDoc = response.xhr.responseXML;
	var dateReply = xmlDoc.getElementsByTagName("dateReply");

	var errorMsg = getNodeValue(dateReply, "errorMsg");

	if (errorMsg != '') {
		alert("Returning" + errorMsg);

		dijit.byId('endDate').set('value', getNodeValue(dateReply, "validDate"));
	}
}

function approveRequest(event) {
	$('.actionButton').each(function() {
		this.disabled = true;
	});

	pilQueueCommand({
		action: "processRequest",
		requestID: dojo.byId('requestID').value,
		processType: 'approve'
	});

	pilPushQueue("./PtoRequest", actionReport);
}

function rejectRequest(event) {
	$('.actionButton').each(function() {
		this.disabled = true;
	});

	pilQueueCommand({
		action: "processRequest",
		requestID: dojo.byId('requestID').value,
		processType: 'reject'
	});

	pilPushQueue("./PtoRequest", actionReport);
}

function cancelRequest(event) {
	$('.actionButton').each(function() {
		this.disabled = true;
	});

	pilQueueCommand({
		action: "processRequest",
		requestID: dojo.byId('requestID').value,
		processType: 'cancel'
	});

	pilPushQueue("./PtoRequest", actionReport);
}

function processRequest(event) {
	$('.actionButton').each(function() {
		this.disabled = true;
	});

	var buttonPressed = event.target;
	var procType = buttonPressed.pilProcessType;

	pilQueueCommand({
		action: "processRequest",
		requestID: dojo.byId('requestID').value,
		processType: procType
	});

	pilPushQueue("./PtoRequest", actionReport);
}

function actionReport(type, response, event) {
	var servletResponse = response.xhr.responseText;

	if (servletResponse == "failed") {
		alert("The request failed to process.");
	} else {
		alert(servletResponse);

		loadSpecificRequest();
	}

	$('.actionButton').each(function() {
		this.disabled = false;
	});
}

function escalate() {
	escalateMenu();

	// escalate should fully populate the employee drop down (if reception)
	if ( (dojo.byId('requestStatus').textContent == "Not Requested")) {
		$('#employeeID')[0].disabled = false;

		if (isReception()) {
			loadEmpSelect(true);
		}
	}

	if (isHR()) {
		dojo.byId('numDays').disabled = false;

		$("#sendButton").show();
		$("#requestStatusDetail").show();
	}
}

function printRequest() {
	if ($("#requestID").val() == '') {
		alert("You must select a request to print!");

		return false;
	}

	var link = "./PtoRequestPrint?requestID=" + $("#requestID").val();

	window.open(link, 'requestPDF');
}

function init() {
	ptoSetup();

	// connect buttons to functions
	dojo.connect(dojo.byId('sendButton'), 'onclick', 'sendRequest');

	dojo.connect(dojo.byId('requestSelect'), 'onchange', 'requestTypeChanged');
	dojo.connect(dojo.byId('employeeID'), 'onchange', 'loadUserEmployee');

	dojo.connect(dojo.byId('approveButton'), 'onclick', 'approveRequest');
	dojo.connect(dojo.byId('rejectButton'), 'onclick', 'rejectRequest');
	dojo.connect(dojo.byId('cancelButton'), 'onclick', 'cancelRequest');
	dojo.connect(dojo.byId('printButton'), 'onclick', 'printRequest');

	// jdm: connect the onChange handlers to the starting and ending dates
	dojo.connect(dijit.byId('startDate'), 'onChange', 'startDateChanged');
	dojo.connect(dijit.byId('endDate'), 'onChange', 'endDateChanged');

	// jdm: disable the onChange handlers
	window.pilInitialLoad = true;

	setupScreen();
}

dojo.addOnLoad(init);
