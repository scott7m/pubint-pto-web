// requestTypes.js

function loadRequestSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoRequestType", fillRequestSelect);
}

function fillRequestSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var requestSelect = dojo.byId("requestSelect");
	var selectedRequest = requestSelect.value;

	if (selectedRequest == "") {
		selectedRequest = "0";
	}

	pilLoadSelectBox(requestSelect, "requestType", "id", "description", xmlDoc, true, "Add New");

	requestSelect.value = selectedRequest;
}

function loadTimeSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoTimeType", fillTimeSelect);
}

function fillTimeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var timeSelect = dojo.byId("timeSelect");
	var selectedTime = timeSelect.value;

	if (selectedTime == "") {
		selectedTime = "0";
	}

	pilLoadSelectBox(timeSelect, "timeType", "id", "selectDesc", xmlDoc, true, "None Selected");

	timeSelect.value = selectedTime;
}

function clearEntry() {
	dojo.byId('description').value = "";
	dojo.byId('shortDescription').value = '';
	dojo.byId('timeSelect').value = "0";
	dojo.byId('commentRequired').checked = false;
	dojo.byId('specialRouting').checked = false;
	// dojo.byId('unpaidTime').checked = false;
	dojo.byId('hrOnly').checked = false;
}

function loadRequestType() {
	pilQueueCommand({
		action: "sendXML",
		requestID: dojo.byId('requestSelect').value
	});

	pilPushQueue("./PtoRequestType", displayRequestType);
}

function displayRequestType(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();

	var requestType = xmlDoc.getElementsByTagName("requestType");

	dojo.byId('description').value = getNodeValue(requestType, "description");
	dojo.byId('shortDescription').value = getNodeValue(requestType, "shortDescription");

	var timeType = xmlDoc.getElementsByTagName("timeType");

	var timeTypeID;

	if (timeType.length == "0") {
		timeTypeID = 0;
	} else {
		timeTypeID = getNodeAttribute(timeType, "id");
	}

	dojo.byId('timeSelect').value = timeTypeID;

	dojo.byId('commentRequired').checked = (getNodeValue(requestType, "requireComment") == "true");
	dojo.byId('specialRouting').checked = (getNodeValue(requestType, "specialRouting") == "true");
	// dojo.byId('unpaid').checked = (getNodeValue(requestType, "unpaidTime") ==
	// "true");
	dojo.byId('hrOnly').checked = (getNodeValue(requestType, "hrOnly") == "true");
}

function update() {
	pilQueueCommand({
		action: "update",
		requestID: dojo.byId('requestSelect').value,
		description: dojo.byId('description').value,
		shortDescription: dojo.byId('shortDescription').value,
		timeID: dojo.byId('timeSelect').value,
		commentRequired: dojo.byId('commentRequired').checked,
		specialRouting: dojo.byId('specialRouting').checked,
		// unpaid : dojo.byId('unpaidTime').checked,
		hrOnly: dojo.byId('hrOnly').checked
	});

	pilPushQueue("./PtoRequestType", addNewRequestType);
}

function addNewRequestType(type, response, event) {
	var requestSelect = dojo.byId("requestSelect");

	var xmlDoc = response.xhr.responseXML;

	var requestType = xmlDoc.getElementsByTagName("requestType");

	var requestID = getNodeAttribute(requestType, "id");
	var desc = getNodeValue(requestType, "description");
	var shortDescription = getNodeValue(requestType, 'shortDescription');

	if (requestSelect.value == 0) {
		var opt = baseOPTION.cloneNode(false);

		opt.value = requestID;
		opt.text = desc + "(" + requestID + ")";

		requestSelect.appendChild(opt);
		requestSelect.value = requestID;
	} else {
		loadRequestSelect();
	}

	displayRequestType(type, response, event);
}

function remove() {
	pilQueueCommand({
		action: "remove",
		requestID: dojo.byId('requestSelect').value
	});

	pilPushQueue("./PtoRequestType", backFromRemove);
}

function backFromRemove() {
	clearEntry();

	dojo.byId('requestSelect').value = "0";

	loadRequestSelect();
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	// reload page when the Request Type select box value changes
	dojo.connect(dojo.byId('requestSelect'), 'onchange', 'loadRequestType');

	// connect buttons to functions
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
	dojo.connect(dojo.byId('deleteButton'), 'onclick', 'remove');

	// override default form submission
	// dojo.connect(dojo.byId('rtypeForm'), 'onsubmit', 'returnFalse');

	loadRequestSelect();
	loadTimeSelect();
}

dojo.addOnLoad(init);