/**
 * notifications.js
 */
function loadPage() {
	clearEntry();
	
	loadNotifySelect();
	loadEmpSelect();
	loadDeptSelect();
}

function clearEntry() {
	$("#notifySelect").val(0);
	$("#title").val("");
	$("#template").val("");
	$("detailDisplay").val("");
}

function loadNotifySelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pushQueue("./NotificationServlet", fillNotifySelect);
}

function fillNotifySelect(xmlDoc) {
	var notifySelect = $("#notifySelect");

	loadSelectBox(notifySelect, "template", "id", "title", xmlDoc, false, "Add New");

//	notifySelect.value = notifySelect.childNodes[0].value;
}

function loadEmpSelect(asReceptionist) {
	pilQueueCommand({
		action: "selectList",
		asReceptionist: (asReceptionist == true ? true : false)
	});

	pushQueue("./PtoEmployee", fillEmpSelect);
}

function fillEmpSelect(xmlDoc) {
	var empSelect = $("#employeeID");

	loadSelectBox(empSelect, "employee", "id", "fullName", xmlDoc, false, "None Selected");

//	empSelect.value = empSelect.childNodes[0].value;

//	loadEmployee();
}

function loadNotification() {
	if ($("#notifySelect").val() === "0") {
		clearEntry();
		$("#addDetail").hide();
		return false;
	}
	
	pilQueueCommand({
		action: "sendXML",
		id: $("#notifySelect").val()
	});

	pushQueue("./NotificationServlet", showNotification);
}

function showNotification(xmlDoc) {
	var temp = $(xmlDoc).find("text").text();
	
	$("#title").val($(xmlDoc).find("title").text());
	$("textarea#template").val(temp);
	
	showDetails(xmlDoc);
}

function showDetails(xmlDoc) {
	var detailDisplay = $("#detailDisplay");
	detailDisplay.val("");
	$("#addDetail").show();
	
	detailDisplay.html("<b><u>Recipients</u> (Click to remove)</b><br/>");
	
	$(xmlDoc).find("recipients").each(function() {
		detailDisplay.append(
				"<a href='javascript:void();' onclick='dropRecipient(this);' " + 
				"id='recipient" + $(this).attr("employeeID") + "' pilID='" + 
				$(this).attr("employeeID") + "' class='recipient'>" + 
				$(this).find("employeeName").text() + 
				"</a><br/>");
	});
}

function addRecipient() {
	pilQueueCommand({
		action: "addRecipient",
		templateID: $("#notifySelect").val(),
		deptID: $("#deptSelect").val(),
		empID: $("#employeeID").val()
	});

	pushQueue("./NotificationServlet", addReturn);
}

function addReturn(xmlDoc) {
	if ($(xmlDoc).find("errorMsg").text() != "") {
		alert($(xmlDoc).find("errorMsg").text());
	} else {
		var detailDisplay = $("#detailDisplay");
		var empName = $(xmlDoc).find("empName").text();
		var empID = $(xmlDoc).find("empID").text();
		
		detailDisplay.append(
				"<a href='javascript:void();' onclick='dropRecipient(this);' " + 
				"id='recipient" + empID + "' pilID='" + 
				empID + "' class='recipient'>" + 
				empName + "</a><br/>");
	}
}

function dropRecipient(target) {
	var pilID = $(target).attr("pilID");
	
	pilQueueCommand({
		action: "dropRecipient",
		templateID: $("#notifySelect").val(),
		empID: pilID
	});

	pushQueue("./NotificationServlet", dropReturn);
}

function dropReturn(xmlDoc) {
	if ($(xmlDoc).find("errorMsg").text() != "") {
		alert($(xmlDoc).find("errorMsg").text());
	} else {
		var pilID = $(xmlDoc).find("pilID").text();
		$("#recipient"+pilID).next().remove();
		$("#recipient"+pilID).remove();
	}
}

function deptChange() {
	$("#employeeID").value = $("#employeeID")[0][0].value;
}

function empChange() {
	$("#deptSelect").value = $("#deptSelect")[0][0].value;
}

function init() {
	ptoSetup();
	
	loadPage();
	
	$("#notifySelect").change(loadNotification);
	$("#addRecipientButton").click(addRecipient);
	
//	$("#deptSelect").change(deptChange);
//	$("#employeeID").change(empChange);
}

$(document).ready(init);