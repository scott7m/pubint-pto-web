/**
 * resetPass.js
 * 
 * UI to allow HR and IT to reset people's passwords
 * without direct database access.
 */

function loadPage() {
	loadEmpSelect(true);
}

function loadEmpSelect(asReceptionist) {
	pilQueueCommand({
		action: "selectList",
		asReceptionist: (asReceptionist == true ? true : false)
	});

	if (asReceptionist) {
		pilPushQueue("./PtoEmployee", fillEmpSelectAsReceptionist);
	} else {
		pilPushQueue("./PtoEmployee", fillEmpSelect);
	}
}

function fillEmpSelectAsReceptionist(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var empSelect = dojo.byId("employeeID");

	pilLoadSelectBox(empSelect, "employee", "id", "fullName", xmlDoc, false, "Add New");

	empSelect.value = empSelect.childNodes[0].value;

	loadEmployee();
}

function loadEmployee() {
	clearEntry();

	pilQueueCommand({
		action: "sendXML",
		employeeID: dojo.byId('employeeID').value
	});

	pilPushQueue("./PtoEmployee", displayEmp);
}

function clearEntry() {
	$("#employeePhoto").empty();
	$("#email").empty();
}

function displayEmp(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();

	var employee = xmlDoc.getElementsByTagName("employee")[0];

	var employeeID = getNodeAttribute(employee, "id");

	dojo.byId('firstName').value = getNodeValue(employee, "firstName");
	//dojo.byId('midInitial').value = getNodeValue(employee, "middleInitial");
	dojo.byId('lastName').value = getNodeValue(employee, "lastName");
	dojo.byId('email').value = getNodeValue(employee, "email");

	var path = "/archives/headshots/" + getNodeValue(employee, "fullName") + ".jpg";

	$("#employeePhoto").empty().append($("<img>").attr('src', path));
}

function resetPassword() {
	var ans = confirm("Are you sure you want to reset the password for " 
			+ $("#firstName").val() + " " + $("#lastName").val() + "?");
	
	if (ans) {
		pilQueueCommand({
			action: "resetPassword",
			employeeID: dojo.byId('employeeID').value,
			email: dojo.byId('email').value
		});

		pilPushQueue("./PtoEmployee", displayResults);
	}
}

function displayResults(type, response, event) {
	var returnedValue = response.xhr.responseText;
	
	if (returnedValue === "Success") {
		alert("Password successfully changed.");
	} else {
		alert("There was a problem resetting the password.");
	}
}

function init() {
	ptoSetup();

	// reload page when the employee select box value changes
	dojo.connect(dojo.byId('employeeID'), 'onchange', 'loadEmployee');

	// connect buttons to functions
	dojo.connect(dojo.byId('resetButton'), 'onclick', 'resetPassword');
	
	loadPage();
}

dojo.addOnLoad(init);

