// timeTimes.js

function loadTimeSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoTimeType", fillTimeSelect);
}

function fillTimeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var timeSelect = dojo.byId("timeSelect");
	var selectedTime = timeSelect.value;

	if (selectedTime == "") {
		selectedTime = "0";
	}

	pilLoadSelectBox(timeSelect, "timeType", "id", "selectDesc", xmlDoc, false, "Add New");

	timeSelect.value = selectedTime;
}

function loadTypeSelect() {
	pilQueueCommand({
		action: "typeSelectList"
	});

	pilPushQueue("./PtoTimeType", fillTypeSelect);
}

function fillTypeSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var typeSelect = dojo.byId("typeCategorySelect");
	var selectedType = typeSelect.value;

	if (selectedType == "") {
		selectedType = "1";
	}

	pilLoadSelectBox(typeSelect, "timeTypeCategory", "id", "description", xmlDoc, false);

	typeSelect.value = selectedType;
}

function loadLocSelect() {
	pilQueueCommand({
		action: "selectList"
	});

	pilPushQueue("./PtoLocation", fillLocSelect);
}

function fillLocSelect(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var locSelect = dojo.byId("locSelect");
	var selectedLoc = locSelect.value;

	if (selectedLoc == "") {
		selectedLoc = "0";
	}

	pilLoadSelectBox(locSelect, "location", "id", "name", xmlDoc, false, "Default");

	locSelect.value = selectedLoc;
}

function clearEntry() {
	dojo.byId('typeCategorySelect').value = "1";
	dojo.byId('description').value = "";
	dojo.byId('locSelect').value = "0";
	dojo.byId('yrsBrk1').value = "";
	dojo.byId('yrsBrk1Max').value = "";
	dojo.byId('yrsBrk2').value = "";
	dojo.byId('yrsBrk2Max').value = "";
	dojo.byId('yrsBrk3').value = "";
	dojo.byId('yrsBrk3Max').value = "";
	dojo.byId('maximum').value = "";
	dojo.byId('maximumRollover').value = "";
	dojo.byId('brk1Date').value = "";
	dojo.byId('brk1Qty').value = "";
	dojo.byId('brk2Date').value = "";
	dojo.byId('brk2Qty').value = "";
	dojo.byId('brk3Date').value = "";
	dojo.byId('brk3Qty').value = "";
	dojo.byId('brk4Date').value = "";
	dojo.byId('brk4Qty').value = "";
	dojo.byId('brk5Date').value = "";
	dojo.byId('brk5Qty').value = "";
	dojo.byId('brk6Date').value = "";
	dojo.byId('brk6Qty').value = "";
	dojo.byId('brk7Date').value = "";
	dojo.byId('brk7Qty').value = "";
	dojo.byId('brk8Date').value = "";
	dojo.byId('brk8Qty').value = "";
	dojo.byId('brk9Date').value = "";
	dojo.byId('brk9Qty').value = "";
	dojo.byId('brk10Date').value = "";
	dojo.byId('brk10Qty').value = "";
	dojo.byId('brk11Date').value = "";
	dojo.byId('brk11Qty').value = "";
	dojo.byId('brk12Date').value = "";
	dojo.byId('brk12Qty').value = "";
}

function loadTimeType() {
	pilQueueCommand({
		action: "sendXML",
		timeID: dojo.byId('timeSelect').value
	});

	pilPushQueue("./PtoTimeType", displayTimeType);
}

function displayTimeType(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	clearEntry();

	var timeType = xmlDoc.getElementsByTagName("timeType");

	var typeType = xmlDoc.getElementsByTagName("timeTypeCategory");

	var typeID;

	if (typeType.length == "0") {
		typeID = 1;
	} else {
		typeID = getNodeAttribute(typeType, "id");
	}

	dojo.byId('typeCategorySelect').value = typeID;

	var loc = xmlDoc.getElementsByTagName("office");

	var locID;

	if (loc.length == "0") {
		locID = 0;
	} else {
		locID = getNodeAttribute(loc, "id");
	}

	dojo.byId('locSelect').value = locID;
	dojo.byId('description').value = getNodeValue(timeType, "description");

	dojo.byId('yrsBrk1').value = getNodeValue(timeType, "yearBreak1");
	dojo.byId('yrsBrk1Max').value = getNodeValue(timeType, "year1MaxPTO");

	if (dojo.byId('yrsBrk1').value != "") {
		dojo.byId('yrsBrk2').disabled = false;
		dojo.byId('yrsBrk2Max').disabled = false;
	} else {
		dojo.byId('yrsBrk2').disabled = true;
		dojo.byId('yrsBrk2Max').disabled = true;
	}

	dojo.byId('yrsBrk2').value = getNodeValue(timeType, "yearBreak2");
	dojo.byId('yrsBrk2Max').value = getNodeValue(timeType, "year2MaxPTO");

	if (dojo.byId('yrsBrk2').value != "") {
		dojo.byId('yrsBrk3').disabled = false;
		dojo.byId('yrsBrk3Max').disabled = false;
	} else {
		dojo.byId('yrsBrk3').disabled = true;
		dojo.byId('yrsBrk3Max').disabled = true;
	}

	dojo.byId('yrsBrk3').value = getNodeValue(timeType, "yearBreak3");
	dojo.byId('yrsBrk3Max').value = getNodeValue(timeType, "year3MaxPTO");

	dojo.byId('maximum').value = getNodeValue(timeType, "maxDaysAllowed");
	dojo.byId('maximumRollover').value = getNodeValue(timeType, "maxRollover");

	var work = getNodeValue(timeType, "year1Break1");

	if (work != "") {
		dijit.byId('brk1Date').set("value", work);
	} else {
		dijit.byId('brk1Date').set("value", null);
	}

	dojo.byId('brk1Qty').value = getNodeValue(timeType, "year1Break1Days");

	work = getNodeValue(timeType, "year1Break2");

	if (work != "") {
		dijit.byId('brk2Date').set("value", work);
	} else {
		dijit.byId('brk2Date').set("value", null);
	}

	dojo.byId('brk2Qty').value = getNodeValue(timeType, "year1Break2Days");

	work = getNodeValue(timeType, "year1Break3");

	if (work != "") {
		dijit.byId('brk3Date').set("value", work);
	} else {
		dijit.byId('brk3Date').set("value", null);
	}

	dojo.byId('brk3Qty').value = getNodeValue(timeType, "year1Break3Days");

	work = getNodeValue(timeType, "year1Break4");

	if (work != "") {
		dijit.byId('brk4Date').set("value", work);
	} else {
		dijit.byId('brk4Date').set("value", null);
	}

	dojo.byId('brk4Qty').value = getNodeValue(timeType, "year1Break4Days");

	work = getNodeValue(timeType, "year1Break5");

	if (work != "") {
		dijit.byId('brk5Date').set("value", work);
	} else {
		dijit.byId('brk5Date').set("value", null);
	}

	dojo.byId('brk5Qty').value = getNodeValue(timeType, "year1Break5Days");

	work = getNodeValue(timeType, "year1Break6");

	if (work != "") {
		dijit.byId('brk6Date').set("value", work);
	} else {
		dijit.byId('brk6Date').set("value", null);
	}

	dojo.byId('brk6Qty').value = getNodeValue(timeType, "year1Break6Days");

	work = getNodeValue(timeType, "year1Break7");

	if (work != "") {
		dijit.byId('brk7Date').set("value", work);
	} else {
		dijit.byId('brk7Date').set("value", null);
	}

	dojo.byId('brk7Qty').value = getNodeValue(timeType, "year1Break7Days");

	work = getNodeValue(timeType, "year1Break8");

	if (work != "") {
		dijit.byId('brk8Date').set("value", work);
	} else {
		dijit.byId('brk8Date').set("value", null);
	}

	dojo.byId('brk8Qty').value = getNodeValue(timeType, "year1Break8Days");

	work = getNodeValue(timeType, "year1Break9");

	if (work != "") {
		dijit.byId('brk9Date').set("value", work);
	} else {
		dijit.byId('brk9Date').set("value", null);
	}

	dojo.byId('brk9Qty').value = getNodeValue(timeType, "year1Break9Days");

	work = getNodeValue(timeType, "year1Break10");

	if (work != "") {
		dijit.byId('brk10Date').set("value", work);
	} else {
		dijit.byId('brk10Date').set("value", null);
	}

	dojo.byId('brk10Qty').value = getNodeValue(timeType, "year1Break10Days");

	work = getNodeValue(timeType, "year1Break11");

	if (work != "") {
		dijit.byId('brk11Date').set("value", work);
	} else {
		dijit.byId('brk11Date').set("value", null);
	}

	dojo.byId('brk11Qty').value = getNodeValue(timeType, "year1Break11Days");

	work = getNodeValue(timeType, "year1Break12");

	if (work != "") {
		dijit.byId('brk12Date').set("value", work);
	} else {
		dijit.byId('brk12Date').set("value", null);
	}

	dojo.byId('brk12Qty').value = getNodeValue(timeType, "year1Break12Days");
}

function update() {
	pilQueueCommand({
		action: "update",
		timeID: dojo.byId('timeSelect').value,
		typeTypeID: dojo.byId('typeCategorySelect').value,
		description: dojo.byId('description').value,
		maximum: dojo.byId('maximum').value,
		locID: dojo.byId('locSelect').value,
		yrsBrk1: dojo.byId('yrsBrk1').value,
		yrsBrk1Max: dojo.byId('yrsBrk1Max').value,
		yrsBrk2: dojo.byId('yrsBrk2').value,
		yrsBrk2Max: dojo.byId('yrsBrk2Max').value,
		yrsBrk3: dojo.byId('yrsBrk3').value,
		yrsBrk3Max: dojo.byId('yrsBrk3Max').value,
		maximum: dojo.byId('maximum').value,
		maximumRollover: dojo.byId('maximumRollover').value,
		brk1Date: dojo.byId('brk1Date').value,
		brk1Qty: dojo.byId('brk1Qty').value,
		brk2Date: dojo.byId('brk2Date').value,
		brk2Qty: dojo.byId('brk2Qty').value,
		brk3Date: dojo.byId('brk3Date').value,
		brk3Qty: dojo.byId('brk3Qty').value,
		brk4Date: dojo.byId('brk4Date').value,
		brk4Qty: dojo.byId('brk4Qty').value,
		brk5Date: dojo.byId('brk5Date').value,
		brk5Qty: dojo.byId('brk5Qty').value,
		brk6Date: dojo.byId('brk6Date').value,
		brk6Qty: dojo.byId('brk6Qty').value,
		brk7Date: dojo.byId('brk7Date').value,
		brk7Qty: dojo.byId('brk7Qty').value,
		brk8Date: dojo.byId('brk8Date').value,
		brk8Qty: dojo.byId('brk8Qty').value,
		brk9Date: dojo.byId('brk9Date').value,
		brk9Qty: dojo.byId('brk9Qty').value,
		brk10Date: dojo.byId('brk10Date').value,
		brk10Qty: dojo.byId('brk10Qty').value,
		brk11Date: dojo.byId('brk11Date').value,
		brk11Qty: dojo.byId('brk11Qty').value,
		brk12Date: dojo.byId('brk12Date').value,
		brk12Qty: dojo.byId('brk12Qty').value
	});

	pilPushQueue("./PtoTimeType", addNewTimeType);
}

function addNewTimeType(type, response, event) {
	var timeSelect = dojo.byId("timeSelect");

	var xmlDoc = response.xhr.responseXML;

	var timeType = xmlDoc.getElementsByTagName("timeType");

	var timeID = getNodeAttribute(timeType, "id");
	var desc = getNodeValue(timeType, "description");

	if (timeSelect.value == 0) {
		var opt = baseOPTION.cloneNode(false);
		opt.value = timeID;
		opt.text = desc + "(" + timeID + ")";

		timeSelect.appendChild(opt);
		timeSelect.value = timeID;
	} else {
		loadTimeSelect();
	}

	displayTimeType(type, response, event);
}

function remove() {
	pilQueueCommand({
		action: "remove",
		timeID: dojo.byId('timeSelect').value
	});

	pilPushQueue("./PtoTimeType", backFromRemove);
}

function backFromRemove(type, response, event) {
	clearEntry();

	dojo.byId('timeSelect').value = "0";

	loadTimeSelect();
}

function enableBrks() {
	if (dojo.byId('yrsBrk1').value != "") {
		dojo.byId('yrsBrk2').disabled = false;
		dojo.byId('yrsBrk2Max').disabled = false;
	} else {
		dojo.byId('yrsBrk2').disabled = true;
		dojo.byId('yrsBrk2Max').disabled = true;
	}

	if (dojo.byId('yrsBrk2').value != "") {
		dojo.byId('yrsBrk3').disabled = false;
		dojo.byId('yrsBrk3Max').disabled = false;
	} else {
		dojo.byId('yrsBrk3').disabled = true;
		dojo.byId('yrsBrk3Max').disabled = true;
	}
}

function escalate() {
	escalateMenu();
}

function init() {
	ptoSetup();

	// reload page when the Time Type select box value changes
	dojo.connect(dojo.byId('timeSelect'), 'onchange', 'loadTimeType');

	// connect buttons to functions
	dojo.connect(dojo.byId('updateButton'), 'onclick', 'update');
	dojo.connect(dojo.byId('deleteButton'), 'onclick', 'remove');

	dojo.connect(dojo.byId('yrsBrk1'), 'onchange', 'enableBrks');
	dojo.connect(dojo.byId('yrsBrk2'), 'onchange', 'enableBrks');

	// override default form submission
	// dojo.connect(dojo.byId('ttypeForm'), 'onsubmit', 'returnFalse');

	loadTimeSelect();
	loadTypeSelect();
	loadLocSelect();
}

dojo.addOnLoad(init);