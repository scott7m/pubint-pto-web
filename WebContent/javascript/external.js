/*
   Run through all of the links on a page and set them
   to open as a new page/tab if they have the rel attribute
   set to external
   jdm
*/
function externalLinks() {
	if (!document.getElementsByTagName) return;

	var anchors = document.getElementsByTagName("a");

	for (var i=0; i<anchors.length; i++) {
		var anchor = anchors[i];
		
		if (anchor.getAttribute("href") && anchor.getAttribute("rel") == "external") {
			anchor.target = "_blank";
		}
	}
}
