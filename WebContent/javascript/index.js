/**
 * JavaScript library for menu page (index.jsp)
 */

function escalate() {
	escalateMenu();
}

function sendDaily() {
	pilQueueCommand({
		action: "sendDailyAttendance"
	});

	pilPushQueue("./PtoEmployee", bitBucket);
}

function processDepartmental() {
	pilQueueCommand({
		action: "departmentalAttendance"
	});

	pilPushQueue("./PtoRequest", bitBucket);
}

function populateHistory() {
	var today = new Date();

	pilQueueCommand({
		action: "populateHistory",
		date: today
	});

	pilPushQueue("./PtoEmployee", bitBucket);
}

function rollover2015() {
	pilQueueCommand({
		action: "rollover2015PTO"
	});

	pilPushQueue("./PtoEmployee", bitBucket);
}

function init() {
	ptoSetup();

	// connect buttons to functions
	dojo.connect(dojo.byId('employeeListButton'), 'onclick', 'loadEmployeeList');
	dojo.connect(dojo.byId('rollover2015'), 'onclick', 'rollover2015');

	try {
		dojo.connect(dojo.byId('populateHistory'), 'onclick', 'populateHistory');
		dojo.connect(dojo.byId('sendDaily'), 'onclick', 'sendDaily');
	} catch (exception) {

	}

	try {
		dojo.connect(dojo.byId('departmental'), 'onclick', 'processDepartmental');
	} catch (exception) {

	}

	getPending();
}

dojo.addOnLoad(init);
