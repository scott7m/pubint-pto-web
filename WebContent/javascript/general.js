/*
		This is a general library of frequently used functions
		JDM: 04/12/2006
 */

dojo.require("dojo.parser");
dojo.require("dijit.dijit");
dojo.require("dijit.form.DateTextBox");
dojo.require("dijit.form.TimeTextBox");
dojo.require("dijit.form._FormWidget");
dojo.require("dojo.io.iframe");

dojo.require("dojo.behavior");

dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.Tree");
dojo.require("dojo.data.ItemFileReadStore");

dojo.require("dijit.Tooltip");

dojo.require('dojo.date.locale');

var undefined;

window.pilCommandQueue = [ ];

// create basic form elements for cloning
var baseA = document.createElement("a");
var baseDL = document.createElement("dl");
var baseDT = document.createElement("dt");
var baseDD = document.createElement("dd");
var baseDIV = document.createElement("div");
var baseUL = document.createElement("ul");
var baseOL = document.createElement("ol");
var baseLI = document.createElement("li");
var baseFORM = document.createElement("form");
var baseSELECT = document.createElement("select");
var baseINPUT = document.createElement("input");
var baseOPTION = document.createElement("option");
var baseTABLE = document.createElement("table");
var baseTHEAD = document.createElement("thead");
var baseTBODY = document.createElement("tbody");
var baseTH = document.createElement("th");
var baseTR = document.createElement("tr");
var baseTD = document.createElement("td");
var baseSPAN = document.createElement("span");
var baseBR = document.createElement("br");
var baseTEXTAREA = document.createElement("textarea");
var baseIMG = document.createElement('img');
var baseNAV = document.createElement('nav');
var baseLABEL = document.createElement('label');

function createH1(text) {
	var heading = document.createElement("h1");

	heading.appendChild(document.createTextNode(text));

	return heading;
}

function createH2(text) {
	var heading = document.createElement("h2");

	heading.appendChild(document.createTextNode(text));

	return heading;
}

function createH3(text) {
	var heading = document.createElement("h3");

	heading.appendChild(document.createTextNode(text));

	return heading;
}

function createH4(text) {
	var heading = document.createElement("h4");

	heading.appendChild(document.createTextNode(text));

	return heading;
}

function createH5(text) {
	var heading = document.createElement("h5");

	heading.appendChild(document.createTextNode(text));

	return heading;
}

function createParagraph() {
	var paragraph = document.createElement('p');

	var line = 0;

	for (var i = 0; i < arguments.length; i++) {
		if (arguments[i].trim() == '') {
			continue;
		}

		if (line > 0) {
			paragraph.appendChild(baseBR.cloneNode(false));
		}

		paragraph.appendChild(document.createTextNode(arguments[i]));

		line++;
	}

	return paragraph;
}

// pull the value of a node from a DOM (xml) object
function getNodeValue(node, tag) {
	var returnValue = undefined;

	if (tag !== ".") {
		var foundAt = -1;

		try {
			for (var i = 0; i < node.childNodes.length; i++) {
				if (node.childNodes[i].nodeName === tag) {
					foundAt = i;

					break;
				}
			}
		} catch (err1) {
		}

		if (foundAt === -1) {
			try {
				if ( (node.length !== undefined) && (node.length > 0)) {
					node = node[0];

					for (var j = 0; j < node.childNodes.length; j++) {
						if (node.childNodes[j].nodeName === tag) {
							foundAt = j;

							break;
						}
					}
				}
			} catch (err2) {
			}
		}

		if (foundAt >= 0) {
			var set = false;

			try {
				returnValue = node.childNodes[foundAt].nodeValue;

				if ( (returnValue !== "") && (returnValue !== null) && (returnValue !== undefined)) {
					set = true;
				}
			} catch (err3) {
			}

			if ( !set) {
				try {
					returnValue = node.childNodes[foundAt].textContent;

					if ( (returnValue !== "") && (returnValue !== null) && (returnValue !== undefined)) {
						set = true;
					}
				} catch (err4) {
				}
			}

			if ( !set) {
				try {
					if (node.childNodes[foundAt].childNodes.length > 0) {
						returnValue = node.childNodes[foundAt].childNodes[0].textContent;
					}

					if ( (returnValue !== "") && (returnValue !== null) && (returnValue !== undefined)) {
						set = true;
					}
				} catch (err5) {
				}
			}
		}
	} else {
		returnValue = node.textContent;
	}

	if (returnValue === undefined) {
		returnValue = "";
	}

	returnValue = returnValue.replace(/&amp;/gi, "&");

	return returnValue;
}

// pull an attribute value from a DOM (xml) object
function getNodeAttribute(obj, tag) {
	var returnValue = undefined;
	var set = false;

	try {
		returnValue = obj.attributes.getNamedItem(tag).value;
		set = true;
	} catch (err) {
	}

	if ( !set) {
		try {
			returnValue = obj[0].attributes.getNamedItem(tag).value;
			set = true;
		} catch (err1) {
		}
	}

	if (returnValue === undefined) {
		returnValue = "";
	}

	returnValue = returnValue.replace(/&amp;/gi, "&");

	return returnValue;
}

function getChildrenByTagName(obj, tag) {
	var list = [ ];
	var child;

	for (var i = 0; i < obj.childNodes.length; i++) {
		child = obj.childNodes[i];

		if (child.tagName === tag) {
			list[list.length] = child;
		}
	}

	return list;
}

function setNodeAttribute(obj, tag, value) {
	var attributeList = null;
	var set = false;

	try {
		attributeList = obj.attributes;

		set = true;
	} catch (err) {
	}

	if ( !set) {
		try {
			attributeList = obj[0].attributes;

			set = true;
		} catch (err2) {
		}
	}

	if (set) {
		var newAttribute = document.createAttribute(tag);

		newAttribute.value = value;
		attributeList.setNamedItem(newAttribute);
	}
}

function addClass(node, newClass) {
	if (node !== null) {
		dojo.addClass(node, newClass);
	}

	return true;
}

function removeClass(node, newClass) {
	if (node !== null) {
		dojo.removeClass(node, newClass);
	}

	return true;
}

function pushClass(node, newClass) {
	addClass(node, newClass);

	for (var i = 0; i < node.childNodes.length; i++) {
		pushClass(node.childNodes[i], newClass);
	}

	return true;
}

function popClass(node, newClass) {
	removeClass(node, newClass);

	for (var i = 0; i < node.childNodes.length; i++) {
		popClass(node.childNodes[i], newClass);
	}

	return true;
}

function clearLoadedFlag(node) {
	var inputNodes = node.getElementsByTagName("SELECT");

	if (inputNodes.length > 0) {
		for (var i = 0; i < inputNodes.length; i++) {
			var inputNode = inputNodes[i];

			if (inputNode.pilLoaded !== null) {
				inputNode.pilLoaded = null;
			}
		}
	}

	return true;
}

function createAnchor(anchorNode, label, url, external) {
	if (external === undefined) {
		external = false;
	}

	if (external) {
		anchorNode.target = "_blank";
	}

	anchorNode.href = url;
	anchorNode.appendChild(document.createTextNode(label));

	return true;
}

function pilKillChildren(node) {
	if (node !== null) {
		while (node.hasChildNodes()) {
			node.removeChild(node.firstChild);
		}
	}

	return true;
}

function loadSelectBox(selectBox, nodeType, nodeID, nodeDesc, xmlDoc, showIDs, zeroDesc) {
	if (showIDs === undefined) {
		showIDs = false;
	}

	var holdSelected = selectBox.val();

	selectBox.empty();

	if (zeroDesc !== undefined) {
		selectBox.append($("<option value='0'>").text(zeroDesc));
	}

	$(xmlDoc).find(nodeType).each(function() {
		var id = $(this).attr(nodeID);

		if ( ! ( (id === '0') && (zeroDesc !== undefined))) {
			var desc = unEscapeString($(this).find(nodeDesc).text());

			if (showIDs) {
				desc += " (" + id + ")";
			}

			selectBox.append($("<option value='" + id + "'>").text(desc));
		}

	});

	selectBox.value = holdSelected;
}

function pilLoadSelectBox(selectBox, nodeType, nodeID, nodeDesc, xmlDoc, showIDs, zeroDesc) {
	if (showIDs === undefined) {
		showIDs = false;
	}

	var holdSelected = selectBox.value;

	pilKillChildren(selectBox);

	try {
		var data = xmlDoc.getElementsByTagName(nodeType);

		var workOption = baseOPTION.cloneNode(false);

		if (zeroDesc !== undefined) {
			workOption = baseOPTION.cloneNode(false);
			workOption.value = 0;
			workOption.text = zeroDesc;

			selectBox.appendChild(workOption);
		}

		for (var i = 0; i < data.length; i++) {
			var id = getNodeAttribute(data[i], nodeID);

			if (holdSelected == "") {
				// if nothing was set coming in...Set to first id
				// holdSelected = id;
			}

			if ( ! ( (id === '0') && (zeroDesc !== undefined))) {
				var desc = unEscapeString(getNodeValue(data[i], nodeDesc));

				if (showIDs) {
					desc += " (" + id + ")";
				}

				workOption = baseOPTION.cloneNode(false);
				workOption.value = id;
				workOption.text = desc;

				selectBox.appendChild(workOption);
			}
		}

		selectBox.value = holdSelected;
	} catch (error) {
		;
	}
}

function configureUpload(url, callBack, formNode) {
	dojo.io.bind({
		url: url,
		handler: callBack,
		mimetype: "text/plain",
		formNode: formNode
	});
}

function urlEncode(string) {
	var newString = "" + string; // force whatever is passed in to be a
	// string

	while (newString.indexOf(" ") !== -1) {
		newString = newString.replace(" ", "+");
	}

	return escape(newString);
}

function pilGetElementsByClass(chkClass, node) {
	var result = dojo.query("." + chkClass, node);

	return result;
}

function setupCloseButton() {
	var buttonNodes = pilGetElementsByClass("closeButton", document);

	if (buttonNodes.length > 0) {
		for (var i = 0; i < buttonNodes.length; i++) {
			var buttonNode = buttonNodes[i];

			dojo.connect(buttonNode, "onclick", function(evt) {
				window.close();
				window.closeWindow();
			});
		}
	}

	return true;
}

function setupDirty(subDocument) {
	var selectNodes = subDocument.getElementsByTagName("select");
	var inputNodes = subDocument.getElementsByTagName("input");
	var textAreaNodes = subDocument.getElementsByTagName("textarea");

	var i;

	if (selectNodes.length > 0) {
		for (i = 0; i < selectNodes.length; i++) {
			var selectNode = selectNodes[i];

			if (selectNode.dirtyConfigured !== true) {
				if (selectNode.disableDirty === null) {
					dojo.connect(selectNode, "onchange", "dirtySelect");
				}

				selectNode.dirtyConfigured = true;
			}
		}
	}

	if (inputNodes.length > 0) {
		for (i = 0; i < inputNodes.length; i++) {
			var inputNode = inputNodes[i];

			if (inputNode.dirtyConfigured !== true) {
				if (inputNode.disableDirty === null) {
					if (inputNode.type === "text") {
						dojo.disconnect(inputNode, "onchange", "dirtyInput");

						dojo.connect(inputNode, "onchange", "dirtyInput");
					}
				}

				inputNode.dirtyConfigured = true;
			}
		}
	}

	if (textAreaNodes.length > 0) {
		for (i = 0; i < textAreaNodes.length; i++) {
			var textAreaNode = textAreaNodes[i];

			if (textAreaNode.dirtyConfigured !== true) {
				if (textAreaNode.disableDirty === null) {
					dojo.disconnect(textAreaNode, "onchange", "dirtyInput");

					dojo.connect(textAreaNode, "onchange", "dirtyInput");
				}

				textAreaNode.dirtyConfigured = true;
			}
		}
	}
}

function makeDirty(subDocument) {
	var selectNodes = subDocument.getElementsByTagName("select");
	var inputNodes = subDocument.getElementsByTagName("input");
	var textAreaNodes = subDocument.getElementsByTagName("textarea");

	if (selectNodes.length > 0) {
		for (var i = 0; i < selectNodes.length; i++) {
			var selectNode = selectNodes[i];

			if (selectNode.disableDirty === null) {
				addClass(selectNode, "dirty");
			}
		}
	}

	if (inputNodes.length > 0) {
		for (var i = 0; i < inputNodes.length; i++) {
			var inputNode = inputNodes[i];

			if (inputNode.disableDirty === null) {
				if (inputNode.type === "text") {
					addClass(inputNode, "dirty");
				}
			}
		}
	}

	if (textAreaNodes.length > 0) {
		for (var i = 0; i < textAreaNodes.length; i++) {
			var textAreaNode = textAreaNodes[i];

			if (textAreaNode.disableDirty === null) {
				addClass(textAreaNode, "dirty");
			}
		}
	}

	return true;
}

function checkDirty(checkNode) {
	var isDirty = false;

	var dirtyHeader = pilGetElementsByClass("dirty", checkNode);

	isDirty = (dirtyHeader.length > 0);

	return isDirty;
}

function dirtySelect(event) {
	var work = event.target;

	addClass(work, "dirty");

	if (work.pilParent !== undefined) {
		addClass(work.pilParent, "dirty");
	}
}

function dirtyInput(event) {
	var work = event.target;

	if (work.tagName === "INPUT") {
		addClass(work, "dirty");

		if (work.pilParent !== undefined) {
			addClass(work.pilParent, "dirty");
		}
	} else if (work.tagName === "TEXTAREA") {
		addClass(work, "dirty");

		if (work.pilParent !== undefined) {
			addClass(work.pilParent, "dirty");
		}
	}
}

function validateDate(event) {
	var rawDate = event.target.value;
	var date = rawDate.replace('/', '-');

	var explodedDate = date.split('-');

	var year = 0;
	var month = 0;
	var day = 0;

	if (explodedDate[0] > 1000) {
		year = explodedDate[0];
		month = explodedDate[1];
		day = explodedDate[2];
	} else {
		if (explodedDate[2] < 100) {
			year = explodedDate[2] + 2000;
		} else {
			year = explodedDate[2];
		}

		month = explodedDate[0];
		day = explodedDate[1];
	}

	var check = new Date(month + "/" + day + "/" + year);

	if (check.getDate() !== (day * 1)) {
		alert("Invalid Date");
		event.target.focus();
		return (false);
	} else if (check.getMonth() !== (month - 1)) {
		alert("Invalid Date");
		event.target.focus();
		return (false);
	} else if (check.getFullYear() !== (year * 1)) {
		alert("Invalid Date");
		event.target.focus();
		return (false);
	}

	return (true);
}

function returnFalse() {
	return false;
}

function isNull(val) {
	return (val === null);
}

function pilSwapNode(node1, node2) {
	var placeHolder1 = document.createTextNode("place holder 1");
	var placeHolder2 = document.createTextNode("place holder 2");

	var parentNode1 = node1.parentNode;
	var parentNode2 = node2.parentNode;

	parentNode1.insertBefore(placeHolder1, node1);
	parentNode2.insertBefore(placeHolder2, node2);

	parentNode1.replaceChild(node2, placeHolder1);
	parentNode2.replaceChild(node1, placeHolder2);

	return true;
}

function pilKillNode(node1) {
	if (node1 !== null) {
		pilKillChildren(node1);

		if (node1.parentNode !== undefined) {
			node1.parentNode.removeChild(node1);
		}
	}

	return true;
}

function showError(response) {
	var xmlDoc = response.xhr.responseXML;
	var isError = false;

	try {
		var error = getNodeAttribute(xmlDoc, "isError");

		if (error != "") {
			var errorReason = getNodeAttribute(xmlDoc, "errorReason");
			var errorMessage = getNodeAttribute(xmlDoc, "errorMessage");

			alert(errorReason + "\n\n" + errorMessage);

			isError = true;
		}
		;
	} catch (error) {
		;
	}

	return isError;
}

function bitBucket(type, response, event) {
	showError(response);

	return true;
}

function pilEncapsulate(command) {
	var commandXML = "<command ";
	var nodes = "";

	for (i in command) {
		commandXML += i + "='" + escapeString(command[i]) + "' ";
		nodes += "<" + i + ">" + escapeString(command[i]) + "</" + i + ">";
	}

	commandXML += ">" + nodes + "</command>";

	return commandXML;
}

function escapeString(inputData) {
	var outputData = inputData;

	if ( typeof inputData == 'string') {
		if (inputData != null && inputData != "") {
			outputData = inputData.replace(/&/gi, "&amp;");
			outputData = outputData.replace(/</gi, "&lt;");
			outputData = outputData.replace(/>/gi, "&gt;");
			outputData = outputData.replaceAll("\"", "&quot;");
			outputData = outputData.replaceAll("\\", "~1~");
			outputData = outputData.replaceAll("%", "~2~");
			outputData = outputData.replaceAll("\'", "~3~");
			outputData = outputData.replaceAll("\n", "~4~");
		}
	}

	return outputData;
}

function unEscapeString(inputData) {
	var outputData = inputData;

	if ( typeof inputData == 'string') {
		if (inputData != null && inputData != "") {
			outputData = inputData.replaceAll("&amp;", "&");
			outputData = outputData.replaceAll("&lt;", "<");
			outputData = outputData.replaceAll("&gt;", ">");
			outputData = outputData.replaceAll("&quot;", "\"");
			outputData = outputData.replaceAll("~1~", "\\");
			outputData = outputData.replaceAll("~2~", "%");
			outputData = outputData.replaceAll("~3~", "\'");
			outputData = outputData.replaceAll("~4~", "\n");
		}
	}

	return outputData;
}

function flushQueue() {
	window.pilCommandQueue = [ ];
	window.pilBusy = false;

	return true;
}

function pilQueueCommand(command) {
	var commandXML = pilEncapsulate(command);

	if (window.pilCommandQueue === undefined) {
		window.pilCommandQueue = [ ];
	}

	window.pilCommandQueue[window.pilCommandQueue.length] = commandXML;
	window.pilBusy = true;

	return true;
}

// function added to begin moving away from the old pilXXXX dojo framework
function queueCommand(command) {
	return pilQueueCommand(command);
}

function pilPushSpecificQueue(url, onLoad, queue, onError) {
	if (onError == undefined) {
		onError = "bitBucket";
	}

	var xmlDoc = "";

	xmlDoc += "<?xml version='1.0' encoding='utf-8' ?>";
	xmlDoc += "<commands>";

	for (var i = 0; i < queue.length; i++) {
		if (queue[i] !== undefined) {
			xmlDoc += queue[i];
		}
	}

	xmlDoc += "</commands>";

	var parser = new DOMParser();

	var xmlDocument = parser.parseFromString(xmlDoc, "text/xml");

	dojo.rawXhrPost({
		url: url,
		postData: xmlDocument,
		load: onLoad,
		headers: {
			"Content-Type": "application/xml"
		},
		handleAs: "xml"
	});

	return true;
}

function pushSpecificQueue(url, onLoad, queue, onError) {
	if (onError == undefined) {
		onError = "bitBucket";
	}

	var xmlDoc = "";

	xmlDoc += "<?xml version='1.0' encoding='utf-8' ?>";
	xmlDoc += "<commands>";

	for (var i = 0; i < queue.length; i++) {
		if (queue[i] !== undefined) {
			xmlDoc += queue[i];
		}
	}

	xmlDoc += "</commands>";

	$.ajax({
		type: 'POST',
		url: url,
		data: xmlDoc,
		contentType: 'text/xml',
		dataType: 'xml',
		cache: false,
		error: showError,
		success: onLoad
	});

	return true;
}

function pushQueue(url, onLoad) {
	pushSpecificQueue(url, onLoad, window.pilCommandQueue);

	flushQueue();

	return true;
}

function pilPushQueue(url, onLoad) {
	pilPushSpecificQueue(url, onLoad, window.pilCommandQueue);

	flushQueue();

	return true;
}

function uploadFile(event) {
	var target = event.target;
	var formID = target.parentNode;
	var url = formID.action;

	var parameters = {
		url: url,
		load: function(data) {
			// basically do nothing
		},
		error: function(data) {
			// basically do nothing
		},
		form: formID
	};

	dojo.io.iframe.send(parameters);
}

function copyDate(sentDate) {
	var workDate;

	if ( ! (sentDate instanceof Date)) {
		var tryFromString = strToDate(sentDate);

		if (tryFromString instanceof Date) {
			workDate = tryFromString;
		} else {
			workDate = new Date();
		}
	} else {
		workDate = new Date(sentDate.getFullYear(), sentDate.getMonth(), sentDate.getDate());
	}

	return workDate;
}

function offsetDate(sentDate, offset) {
	if (offset === undefined) {
		offset = 0;
	}

	var workDate = copyDate(sentDate);

	workDate.setDate(workDate.getDate() + offset);

	return workDate;
}

function fixDate(sentDate, offset) {
	if (offset === undefined) {
		offset = 0;
	}

	var workDate = offsetDate(sentDate, offset);

	var d = workDate.getDate();
	var day = (d < 10) ? '0' + d : d;
	var m = workDate.getMonth() + 1;
	var month = (m < 10) ? '0' + m : m;
	var strDate = workDate.getFullYear() + "-" + month + "-" + day;

	return strDate;
}

function strToDate(strDate) {
	if (strDate.trim() == '') {
		return null;
	}

	var date = strDate.replace('/', '-');

	var explodedDate = date.split('-');

	var year = 0;
	var month = 0;
	var day = 0;

	if (explodedDate[0] > 1000) {
		year = explodedDate[0];
		month = explodedDate[1];
		day = explodedDate[2];
	} else {
		if (explodedDate[2] < 100) {
			year = explodedDate[2] + 2000;
		} else {
			year = explodedDate[2];
		}

		month = explodedDate[0];
		day = explodedDate[1];
	}

	// make sure that you shift the month back one!
	var check = new Date(year, month - 1, day);

	return check;
}

function completeSetup() {
	// externalLinks();

	setupCloseButton();
}

function cursorWait() {
	document.body.style.cursor = 'progress';

	return true;
}

function cursorReset() {
	try {
		document.body.style.cursor = 'default';
	} catch (err) {

	}

	return true;
}

function reformatMas90ProjectID(mas90ID) {
	var mas90ProjectID = mas90ID;

	mas90ProjectID = mas90ProjectID.replace("-", "");
	mas90ProjectID = "000000000000" + mas90ProjectID.trim();

	var ln = mas90ProjectID.length;

	mas90ProjectID = mas90ProjectID.substring(ln - 8);

	var projID = mas90ProjectID.substring(0, 6);
	var projVer = mas90ProjectID.substring(6);

	mas90ProjectID = projID + "-" + projVer;

	return mas90ProjectID;
}

function fillDestination() {
	var selectBox = dojo.byId("destination");

	workOption = baseOPTION.cloneNode(false);
	workOption.value = "JRS";
	workOption.text = "JRS";

	selectBox.appendChild(workOption);

	/*
	 * workOption = baseOPTION.cloneNode(false); workOption.value = "Yantian";
	 * workOption.text = "FOB Yantian";
	 * 
	 * selectBox.appendChild(workOption);
	 * 
	 * workOption = baseOPTION.cloneNode(false); workOption.value = "Hong Kong";
	 * workOption.text = "FOB Hong Kong";
	 * 
	 * selectBox.appendChild(workOption);
	 * 
	 * workOption = baseOPTION.cloneNode(false); workOption.value = "FCA";
	 * workOption.text = "FCA";
	 * 
	 * selectBox.appendChild(workOption);
	 */

	workOption = baseOPTION.cloneNode(false);
	workOption.value = "Customer";
	workOption.text = "Customer ACOR";

	selectBox.appendChild(workOption);

	/*
	 * workOption = baseOPTION.cloneNode(false); workOption.value = "Special";
	 * workOption.text = "Special Address";
	 * 
	 * selectBox.appendChild(workOption);
	 */

	return true;
}

function createDate(item, parameters, constraints) {
	var fullParameterList = {
		lang: 'en-us',
		required: 'true',
		style: 'width: 100px;'
	};

	if (parameters != null) {
		fullParameterList = dojo.mixin(fullParameterList, parameters);
	}

	if (constraints != null) {
		fullParameterList.constraints = dojo.mixin({
			datePattern: 'yyyy-MM-dd'
		}, constraints);
	} else {
		fullParameterList.constraints = {
			datePattern: 'yyyy-MM-dd'
		};
	}

	var makeDate = null;

	try {
		makeDate = new dijit.form.DateTextBox(fullParameterList, item);

		if (item.disabled === true) {
			makeDate.disabled = true;
		}
	} catch (Err) {

	}

	return makeDate;
}

function createDowDate(item, parameters, constraints) {
	var fullParameterList = {
		lang: 'en-us',
		required: 'true',
		style: 'width: 150px;'
	};

	if (parameters != null) {
		fullParameterList = dojo.mixin(fullParameterList, parameters);
	}

	if (constraints != null) {
		fullParameterList.constraints = dojo.mixin({
			datePattern: 'EEE, MMM dd, yyyy'
		}, constraints);
	} else {
		fullParameterList.constraints = {
			datePattern: 'EEE, MMM dd, yyyy'
		};
	}

	var makeDate = null;

	try {
		makeDate = new dijit.form.DateTextBox(fullParameterList, item);
	} catch (Err) {

	}

	return makeDate;
}

function createMMDate(item, parameters, constraints) {
	var fullParameterList = {
		lang: 'en-us',
		required: 'true',
		style: 'width: 150px;'
	};

	if (parameters != null) {
		fullParameterList = dojo.mixin(fullParameterList, parameters);
	}

	if (constraints != null) {
		fullParameterList.constraints = dojo.mixin({
			datePattern: 'MMMM'
		}, constraints);
	} else {
		fullParameterList.constraints = {
			datePattern: 'MMMM'
		};
	}

	var makeDate = null;

	try {
		makeDate = new dijit.form.DateTextBox(fullParameterList, item); // dijit.form.MonthTextBox??
	} catch (Err) {

	}

	return makeDate;
}

function ptoSetup() {
	cursorWait();

	$('.hidden').hide();

	setupCloseButton();

	// userLoggedIn();

	dummy = cursorReset();

	dojo.behavior.add({
		'.isDate': function(item) {
			try {
				createDate(item);
			} catch (Err) {

			}
		}
	});

	dojo.behavior.add({
		'.isDowDate': function(item) {
			try {
				createDowDate(item);
			} catch (Err) {

			}
		}
	});

	dojo.behavior.add({
		'.isMMDate': function(item) {
			try {
				createMMDate(item);
			} catch (Err) {

			}
		}
	});

	dojo.behavior.apply();

	inheritEscalation();

	$(document).foundation();
}

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, "");
};

String.prototype.ltrim = function() {
	return this.replace(/^\s+/, "");
};

String.prototype.rtrim = function() {
	return this.replace(/\s+$/, "");
};

String.prototype.replaceAll = function(find, replace) {
	var text = this;
	var index = text.indexOf(find);

	while (index != -1) {
		text = text.replace(find, replace);
		index = text.indexOf(find);
	}

	return text;
};

Date.prototype.addTime = function(timeDate) {
	this.setHours(timeDate.getHours());
	this.setMinutes(timeDate.getMinutes());

	return this;
};

function closeWindow(event) {
	window.close();

	return true;
}

function userLoggedIn(event) {
	if (window.pilBusy == true) {
		window.setTimeout(userLoggedIn, 60000);
	} else {
		pilQueueCommand({
			action: "userLoggedIn"
		});

		pilPushQueue("./PtoEmployee", userLoggedReturn);

		window.setTimeout(userLoggedIn, 300000);
	}

	return true;
}

function userLoggedReturn(type, response, event) {
	var returnString = response.xhr.responseText;

	if (returnString != "true") {
		window.location = "/";
	}
}

function adminAuth(obj) {
	pilQueueCommand({
		action: "authUser"
	});

	pilPushQueue("./PtoEmployee", loadAdmin);
}

function loadAdmin(type, response, event) {
	var dept = response.xhr.responseText;

	if (dept == "HR" || dept == "IT") {
		$('li.adminItem').show('slow');
	}
}

function getPending(obj) {
	pilQueueCommand({
		action: "getPendingRequests"
	});

	pilPushQueue("./PtoRequest", loadPending);
}

function loadPending(type, response, event) {
	// Fill in requestHistory div with current year's PENDING requests.

	var xmlDoc = response.xhr.responseXML;

	var requestList = xmlDoc.getElementsByTagName("request");

	if (requestList.length == 0) {
		return false;
	}

	var detailDiv = dojo.byId('pendingHistory');

	pilKillChildren(detailDiv);

	var table = baseTABLE.cloneNode(false);
	detailDiv.appendChild(table);

	var row = baseTR.cloneNode(false);
	var cell = baseTH.cloneNode(false);
	var work = document.createTextNode("PTO needing approval:");
	cell.appendChild(work);
	row.appendChild(cell);
	table.appendChild(row);

	var detailTable = baseTABLE.cloneNode(false);
	detailDiv.appendChild(detailTable);

	// var oneTo3Table = baseTABLE.cloneNode(false);
	// detailDiv.appendChild(oneTo3Table);

	var headerRow = baseTR.cloneNode(false);

	var headCell = baseTH.cloneNode(false);
	var headText = document.createTextNode("Name");

	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Start Date");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Ending");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Type of Time Away");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Status");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Days");
	headCell.align = "left";
	headCell.appendChild(headText);
	headerRow.appendChild(headCell);

	detailTable.appendChild(headerRow);

	// var oneTo3Header = headerRow.cloneNode(true);
	// oneTo3Table.appendChild(oneTo3Header);

	var requests = xmlDoc.getElementsByTagName("request");

	var workCell;
	var workText;
	var workRow;

	for (var i = 0; i < requests.length; i++) {
		var employee = requests[i].getElementsByTagName("employee");

		workRow = baseTR.cloneNode(false);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(employee[0], "fullName"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "startDate"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "endDate"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		var requestType = getChildrenByTagName(requests[i], "requestType");

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requestType, "description"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		var workA = baseA.cloneNode(false);
		var requestStatus = getNodeValue(requests[i], "requestStatus");
		workA.href = "./pto/requests.jsp?requestID=" + getNodeAttribute(requests[i], "id");
		workA.innerHTML = requestStatus;
		workA.target = "_blank";
		workCell.appendChild(workA);

		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workText = document.createTextNode(getNodeValue(requests[i], "daysTaken"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		// if (getNodeAttribute(requests[i], "oneTo3") == "true") {
		// oneTo3Table.appendChild(workRow);
		// } else {
		detailTable.appendChild(workRow);
		// }
	}
}

function loadEmployeeList() {
	if (dojo.byId('employeeList').innerHTML == "") {
		pilQueueCommand({
			action: "activeEmployeeList"
		});

		pilPushQueue("./PtoEmployee", displayEmployeeList);

		dojo.byId('employeeListButton').value = "Hide Employees";
	} else {
		pilKillChildren(dojo.byId('employeeList'));

		dojo.byId('employeeListButton').value = "Load Employees";
	}
}

function displayEmployeeList(type, response, event) {
	var xmlDoc = response.xhr.responseXML;

	var employeeList = xmlDoc.getElementsByTagName("employees");

	if (employeeList[0].childNodes.length == 0) {
		return false;
	}

	var detailDiv = dojo.byId('employeeList');

	pilKillChildren(detailDiv);

	var table = baseTABLE.cloneNode(false);

	detailDiv.appendChild(table);

	var row = baseTR.cloneNode(false);
	var cell = baseTH.cloneNode(false);
	var work = document.createTextNode("PIL Employees:");

	cell.appendChild(work);
	cell.align = "left";
	row.appendChild(cell);
	table.appendChild(row);

	var detailTable = baseTABLE.cloneNode(false);
	detailDiv.appendChild(detailTable);

	var headerRow = baseTR.cloneNode(false);

	var headCell = baseTH.cloneNode(false);

	var headText = document.createTextNode("First Name");

	headCell.align = "left";
	headCell.appendChild(headText);

	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Last Name");
	headCell.align = "left";
	headCell.appendChild(headText);

	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Company");
	headCell.align = "left";
	headCell.appendChild(headText);

	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Location");
	headCell.align = "left";
	headCell.appendChild(headText);

	headerRow.appendChild(headCell);

	headCell = baseTH.cloneNode(false);
	headText = document.createTextNode("Extension");
	headCell.align = "left";
	headCell.appendChild(headText);

	headerRow.appendChild(headCell);

	detailTable.appendChild(headerRow);

	var employees = xmlDoc.getElementsByTagName("employee");

	var workCell;
	var workText;
	var workRow;

	var currentDept = "";

	for (var i = 0; i < employees.length; i++) {
		var location = employees[i].getElementsByTagName("location");

		// var dept = employees[i].getElementsByTagName("department");

		var deptName = getNodeValue(employees[i], "departmentName");

		if (currentDept != deptName) {
			workRow = baseTR.cloneNode(false);

			workCell = baseTD.cloneNode(false);
			workCell.colspan = "5";
			workCell.innerHTML = "<b><u>" + deptName + "</u></b>";
			workRow.appendChild(workCell);

			currentDept = deptName;
			detailTable.appendChild(workRow);
		}

		workRow = baseTR.cloneNode(false);

		workCell = baseTD.cloneNode(false);
		workCell.align = "left";
		var workA = baseA.cloneNode(false);
		workA.href = "./ViewEmployee?empID=" + getNodeAttribute(employees[i], "id");
		workA.innerHTML = getNodeValue(employees[i], "firstName");
		workA.target = "_blank";
		workCell.appendChild(workA);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workCell.align = "left";
		workText = document.createTextNode(getNodeValue(employees[i], "lastName"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workCell.align = "left";
		workText = document.createTextNode("Publications International");
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workCell.align = "left";
		workText = document.createTextNode(getNodeValue(location[0], "name"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		workCell = baseTD.cloneNode(false);
		workCell.align = "left";
		workText = document.createTextNode(getNodeValue(employees[i], "extension"));
		workCell.appendChild(workText);
		workRow.appendChild(workCell);

		detailTable.appendChild(workRow);
	}
}

function toggleElement(id) {
	var el = document.getElementById(id);
	if (el.getAttribute('class') == 'hide') {
		el.setAttribute('class', 'show');
	} else {
		el.setAttribute('class', 'hide');
	}
}

function generateReport() {
	window.open("./DailyAttendanceReport?reportDate="
			+ dojo.date.stamp.toISOString(dijit.byId('reportDate').getValue(), {
				selector: 'date'
			}), 'dailyatt');
}

function inheritEscalation() {
	var escalationTest = $("#escalatedPrivileges");

	if (escalationTest.length > 0) {
		escalate();

		return false;
	}

	pilQueueCommand({
		action: "checkEscalation"
	});

	pilPushQueue("./PtoEmployee", setEscalation);
}

function checkEscalation() {
	var escalationTest = $("#escalatedPrivileges");

	if (escalationTest.length > 0) {
		escalate();

		return false;
	}

	var escalationPassword = prompt("Re-enter your password for privilege escalation:", "");

	pilQueueCommand({
		action: "checkEscalation",
		password: escalationPassword
	});

	pilPushQueue("./PtoEmployee", setEscalation);
}

function setEscalation(type, response, event) {
	$("#escalatedPrivileges").remove();

	var xmlDoc = response.xhr.responseXML;

	var privileges = xmlDoc.getElementsByTagName('privileges');

	var allowEscalation = getNodeValue(privileges, "escalation");

	if (allowEscalation == "allowed") {
		var holdEscalation = baseINPUT.cloneNode(false);

		holdEscalation.id = 'escalatedPrivileges';
		holdEscalation.hr = getNodeValue(privileges, 'hr') * 1;
		holdEscalation.adminMenu = getNodeValue(privileges, 'admin') * 1;
		holdEscalation.reception = getNodeValue(privileges, 'reception') * 1;

		var body = dojo.byId('body');

		body.appendChild(holdEscalation);

		$("#escalatedPrivileges").hide();

		escalate();
	}
}

function isHR() {
	try {
		var escalation = dojo.byId('escalatedPrivileges');

		return (escalation.hr == 1);
	} catch (exception) {
		return false;
	}
}

function isAdmin() {
	try {
		var escalation = dojo.byId('escalatedPrivileges');

		return (escalation.adminMenu == 1);
	} catch (exception) {
		return false;
	}
}

function isReception() {
	try {
		var escalation = dojo.byId('escalatedPrivileges');

		return (escalation.reception == 1);
	} catch (exception) {
		return false;
	}
}

function escalateMenu() {
	if (isAdmin()) {
		$('.administration').show();
	}
}

// generalized routines

// Department dropdown routines

function toggleInactiveDepartments() {
	if ($('#includeInactive').prop('checked')) {
		$('.inactiveDept').show();
	} else {
		$('.inactiveDept').hide();
	}
}

// load department select box taking into
// account include inactive
function loadDeptSelect() { // using jQuery
	pilQueueCommand({
		action: "selectList"
	});

	pushQueue("./PtoDepartment", fillDeptSelect);
}

function fillDeptSelect(xmlDoc) { // using jQuery
	$('#deptSelect').empty().append('<option>').val(0).text("Add New");

	$(xmlDoc).find('department').each(function(index) {
		var optionClass = ($(this).find('active').text() == 'true' ? 'activeDept' : 'inactiveDept');
		var id = $(this).attr('id')
		var desc = $(this).find('departmentName').text();

		var optionString = "<option class='" + optionClass + "' value='" + id + "'>" + desc + "</option>";

		$('#deptSelect').append(optionString);
	});

	$('.inactiveDept').hide();

	$('#deptSelect').val(0);
}

function fillXFerDeptSelect(xmlDoc) { // using jQuery
	$('#xferDeptSelect').empty().append('<option>').val(0).text("Add New");

	$(xmlDoc).find('department').each(function(index) {
		var optionClass = ($(this).find('active').text() == 'true' ? 'activeDept' : 'inactiveDept');
		var id = $(this).attr('id')
		var desc = $(this).find('departmentName').text();

		var optionString = "<option class='" + optionClass + "' value='" + id + "'>" + desc + "</option>";

		$('#xferDeptSelect').append(optionString);
	});

	$('.inactiveDept').hide();

	$('#xferDeptSelect').val(0);
}