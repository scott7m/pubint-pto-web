alter table request add column (`canceledBy` bigint(20) DEFAULT NULL, `canceledDate` datetime DEFAULT NULL, `employeeStartTime` time DEFAULT NULL, `employeeEndTime` time DEFAULT NULL);

