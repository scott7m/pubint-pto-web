<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="robots" content="noindex">
<title>Insert title here</title>
</head>
<body>
    <div class="off-canvas-wrap">
      <div class="inner-wrap">
        <aside class="left-off-canvas-menu">

  <ul class="off-canvas-list">
    <li><label class="first">Foundation</label></li>
    <li><a href="index.html">Home</a></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label class="first">Learn</label></li>
    <li><a href="learn/features.html">Features</a></li>
    <li><a href="learn/faq.html">FAQ</a></li>
    <li><a href="learn/case-washington-post.html">Case Studies</a></li>
    <li><a href="learn/website-examples.html">Website Examples</a></li>
    <li><a href="learn/video-started-with-foundation.html">Videos</a></li>
    <li><a href="learn/training.html">Training</a></li>
    <li><a href="learn/about.html">About</a></li>  
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>Develop</label></li>
    <li><a href="templates.html">Add-ons</a></li>
    <li><a href="docs">Docs</a></li>
    <li><a href="develop/download.html">Download</a></li>
    <li><a href="develop/contribute.html">Contribute</a></li>
    <li><a href="develop/tools.html">Tools</a></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>Support</label></li>
    <li><a href="support/support.html">Support Channels</a></li>
    <li><a href="/forum">Foundation Forum</a></li>
    <li><a href="support/images-and-badges.html">Images &amp; Badges</a></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>Business</label></li>
    <li><a href="business/services.html" class="">Business</a></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>Docs</label></li>
    <li><a href="docs" class="">Docs</a></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>Getting Started</label></li>
    <li class="gs"><p><a href="docs" class="button">Getting Started</a></p></li>
  </ul>

  <hr>

  <ul class="off-canvas-list">
    <li><label>More ZURB Goodness</label></li>
    <li><a href="http://zurb.com/studios/our-work" class="">Foundation Training</a></li>
    <li><a href="http://zurb.com/studios/our-work" class="">Our Work</a></li>
    <li><a href="http://zurb.com/apps" class="">Design Apps</a></li>
  </ul>

  <hr>
  <div class="zurb-links">
    <ul class="top">
      <li class="logo"><a href="http://zurb.com"></a></li>
      <li><a href="http://zurb.com/about">About</a></li>
      <li><a href="http://zurb.com/blog">Blog</a></li>
      <li><a href="http://zurb.com/contact">Contact</a></li>
    </ul>
    <ul class="pillars">
      <li>
        <a href="http://www.zurb.com/studios" class="footer-link-block services">
          <span class="title">Studios</span>
          <span>Helping startups win since '98.</span>
        </a>
      </li>
      <li>
        <a href="http://foundation.zurb.com/" class="footer-link-block foundation">
          <span class="title">Foundation</span>
          <span>World's most advanced responsive framework.</span>
        </a>
      </li>
      <li>
        <a href="http://zurb.com/apps" class="footer-link-block apps">
          <span class="title">Design Apps</span>
          <span>Tools to rapidly prototype and iterate.</span>
        </a>
      </li>
      <li>
        <a href="http://zurb.com/university" class="footer-link-block expo">
          <span class="title">University</span>
          <span>Online training for smarter product design.</span>
        </a>
      </li> 
    </ul>
</div>
</aside>

         <title>Foundation: The Most Advanced Responsive Front-end Framework from ZURB</title><meta name="description" content="Foundation from ZURB is the most advanced responsive front-end framework in the world." />
 

<nav class="tab-bar show-for-small">
  <a class="left-off-canvas-toggle menu-icon ">
    <span>Foundation</span>
  </a>  
</nav>


<nav class="top-bar hide-for-small" data-topbar>
  <ul class="title-area">
    <li class="name">
      <h1><a href="index.html">Foundation</a></h1>
    </li>
  </ul>

  <section class="top-bar-section">
    <ul class="right">
      <li class="divider"></li>
      <li class="has-dropdown">
        <a href="learn/features.html" class="">Learn</a>
        <ul class="dropdown">
          <li><a href="learn/features.html">Features</a></li>
          <li><a href="learn/faq.html">FAQ</a></li>
          <li><a href="learn/case-washington-post.html">Case Studies</a></li>
          <li><a href="learn/website-examples.html">Website Examples</a></li>
          <li><a href="learn/video-started-with-foundation.html">Videos</a></li>
          <li><a href="learn/training.html">Training</a></li>
          <li><a href="learn/about.html">About</a></li>
        </ul>
      </li>
      <li class="divider"></li>
      <li class="has-dropdown">
        <a href="templates.html" class="">Develop</a>
        <ul class="dropdown">
          <li><a href="templates.html">Add-ons</a></li>
          <li><a href="/docs">Docs</a></li>
          <li><a href="develop/download.html">Download</a></li>
          <li><a href="develop/contribute.html">Contribute</a></li>
          <li><a href="develop/tools.html">Tools</a></li>
        </ul>
      </li>
      <li class="divider"></li>
      <li class="has-dropdown">
        <a href="support/support.html" class="">Support</a>
        <ul class="dropdown">
          <li><a href="support/support.html">Support Channels</a></li>
          <li><a href="/forum">Foundation Forum</a></li>
          <li><a href="support/images-and-badges.html">Images &amp; Badges</a></li>
        </ul>
      </li>
      <li class="divider"></li>
      <li class="has-dropdown">
        <a href="business/services.html" class="">Business</a>
      </li>
      <li class="divider"></li>
      <li>
        <a href="/docs" class="">Docs</a>
      </li>
      <li class="divider"></li>
      <li class="has-form">
        <a href="/docs" class="small button">Getting Started</a>
    </ul>
  </section>
</nav>
</body>
</html>