<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="pto" uri="PTOTags" %>
<%--Set the page name --%>
<%
String systemName ="PIL PTO Tracking";
String pageName = "Unused PTO Report";
%>
<%--These are used for calculating the base URI --%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="uri" value="${req.requestURI}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="base" value="${fn:substring(url, 0, fn:length(url) - fn:length(req.requestURI))}${req.contextPath}/" />
<%
	// put any parameter grabs here
%>
<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><%=systemName %> - <%=pageName %></title>
        <meta name="description" content="">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<base href="${base}" />
		<link href="./styles/screen.css" title="base" rel="stylesheet" type="text/css" />
		<style type="text/css">@import "/dojo/dijit/themes/tundra/tundra.css";</style>

		<script type="text/javascript" src="/dojo/dojo/dojo.js" djConfig="parseOnLoad: true, locale: 'en-us'"></script>
		<script src="./bower_components/jquery/dist/jquery.min.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/modernizr/modernizr.js" language="JavaScript" type="text/javascript"></script>
		<script src="./bower_components/foundation/js/foundation.js" language="JavaScript" type="text/javascript"></script>
                <script src="./bower_components/foundation/js/foundation/foundation.topbar.js" language="JavaScript" type="text/javascript"></script>
		<script src="./javascript/general.js" language="JavaScript" type="text/javascript"></script>
		
		<!-- JSP Specific javascripts -->
		<script src="./javascript/reports/unusedPto.js" language="JavaScript" type="text/javascript"></script>
	</head>
	
	<body class="tundra">
<pto:pageMenu />

		<div id="body" class="asRow">
			<div class="leftColumn">
				<form id="unusedPtoForm">
					<div class="row">
						<div class="small-12 columns dijitCell">
							<label>As Of Date</label>
							<input type="text" id="reportDate" class="isDowDate" />
						</div>
					</div>
					<div class="row">
						<div class="small-12 columns dijitCell">
							<label>Include Terminated Employees?</label>
							<input type="checkbox" id="includeTerm" />
						</div>
					</div>
				</form>
				<div class="asRow">
					<input type="button" name="generateButton" id="generateButton" value="Generate Report" />
					<input type="button" name="closeButton" id="closeButton" value="Close Window" cssClass="closeButton" class="closeButton"/>
				</div>
			</div>
		</div>
		<div id="menu" class="menu"></div>
	</body>
</html>
